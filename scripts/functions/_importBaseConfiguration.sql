DROP FUNCTION IF EXISTS `_importBaseConfiguration`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `_importBaseConfiguration`() RETURNS int(11)
    DETERMINISTIC
BEGIN

DECLARE result INT default 0;
DECLARE debug VARCHAR(200);

/*
 * Division, Site and Facility
*/
update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblDivision
set DivisionName = 'Oregon Metro'
where DivisionID = 1;
update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblSite
set SiteName = 'Oregon Metro', DivisionID = 1
where SiteID = 1;
update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFacility
set FacilityName = 'Brainard Cemetery', SiteID = 1, MapID = '', TiledMapURI = '', CurrentMapHash = '', ExtFacilityIdentifier = '11111111-1111-1111-1111-111111111101'
where FacilityID = 1;


insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFacility
(
    FacilityID,
    FacilityName,
    SiteID,
    ExtFacilityIdentifier
) values 
    (2, 'Columbia Pioneer Cemetery', 1, '11111111-1111-1111-1111-111111111102'),
    (3, 'Douglass Cemetery', 1, '11111111-1111-1111-1111-111111111103'),
    (4, 'Escobar Cemetery', 1, '11111111-1111-1111-1111-111111111104'),
    (5, 'Grand Army of the Republic Cemetery', 1, '11111111-1111-1111-1111-111111111105'),
    (6, 'Gresham Pioneer Cemetery', 1, '11111111-1111-1111-1111-111111111106'),
    (7, 'Jones Cemetery', 1, '11111111-1111-1111-1111-111111111107'),
    (8, 'Lone Fir Pioneer Cemetery', 1, '11111111-1111-1111-1111-111111111108'),
    (9, 'Mt. View Corbett Cemetery', 1, '11111111-1111-1111-1111-111111111109'),
    (10, 'Mt. View Stark Cemetery', 1, '11111111-1111-1111-1111-111111111110'),
    (11, 'Multnomah Park Cemetery', 1, '11111111-1111-1111-1111-111111111111'),
    (12, 'Pleasant Home Cemetery', 1, '11111111-1111-1111-1111-111111111112'),
    (13, 'Powell Grove Cemetery', 1, '11111111-1111-1111-1111-111111111113'),
    (14, 'White Birch Cemetery', 1, '11111111-1111-1111-1111-111111111114');

  
RETURN result;

END;
//
DELIMITER ;
