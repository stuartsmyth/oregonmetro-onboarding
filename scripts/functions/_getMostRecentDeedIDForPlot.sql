DROP FUNCTION IF EXISTS `_getMostRecentDeedIDForPlot`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `_getMostRecentDeedIDForPlot`(in_plotid int) RETURNS int
    DETERMINISTIC
BEGIN

DECLARE deed_id int;

select d.DeedID into deed_id
from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblDeed d
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlotPurchase pp on d.DeedID = pp.DeedID
where pp.PlotID = in_plotid
order by d.PlotPurchaseDate desc
limit 1;

RETURN deed_id;
END;
//
DELIMITER ;
