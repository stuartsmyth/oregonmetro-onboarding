DROP FUNCTION IF EXISTS `_importDeedTables`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `_importDeedTables`() RETURNS int(11)
    DETERMINISTIC
BEGIN

DECLARE deedCount INT default 0;
DECLARE debug VARCHAR(200);
 
 /*Add cemeteries and general businesses as contacts for deeds*/
    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact (
        `Title`,
        `Forename`,
        `MiddleName`,
        `Surname`,
        `Fullname`,
        `ExtContactIdentifier`,
        `ExtContactIdentifierName`,
        `CustomerNumber`
    )
    select
        null as Title,
        b.name as Forename,
        null as MiddleName,
        null as Surname,
        b.name as FullName,
        b.ID as ExtContactIdentifier,
        'ID' as ExtContactIdentifierName,
        null as CustomerNumber
        from businesses b
       where b.class_type = 'TCemetery'
        ;

    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact (
        `Title`,
        `Forename`,
        `MiddleName`,
        `Surname`,
        `Fullname`,
        `ExtContactIdentifier`,
        `ExtContactIdentifierName`,
        `CustomerNumber`
    )
    select
        null as Title,
        b.name as Forename,
        null as MiddleName,
        null as Surname,
        b.name as FullName,
        b.ID as ExtContactIdentifier,
        'ID' as ExtContactIdentifierName,
        null as CustomerNumber
        from businesses b
       where b.class_type = 'TGeneralTypeBusiness'
        ;

insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblDeed(
  DeedID,
  FacilityID,
  SiteID,
  CreatedForBurialID,
  DeedNumber,
  NumberOfPlots,
  NumberOfIntermentRights,
  NumberOfInurnmentRights,
  ActiveOwner,
  DeedStatus,
  LeaseStartDate,
  LeaseExpiryDate,
  LeaseLengthYears,
  PerpetualLease,
  PlotsList,
  PlotIdsList,
  Notes,
  IsProblematic,
  Deleted,
  CreatedBy,
  DateCreated,
  LastUpdatedBy,
  LastUpdatedDateTime,
  PlotPurchaseDate,
  Cost,
  IsLinkedInContract,
  ExtDeedIdentifier,
  ExtTransferDeedIdentifier
)
select 
	null as DeedId,
    CASE
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111101' then 1
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111102' then 2
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111103' then 3
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111104' then 4
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111105' then 5
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111106' then 6
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111107' then 7
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111108' then 8
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111109' then 9
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111110' then 10
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111111' then 11
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111112' then 12
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111113' then 13
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111114' then 14
    ELSE 1
    END as FacilityID,
  1 as SiteID,
  NUll as CreatedForBurialID,
  t.reference_number as DeedNumber,
  1 as NumberOfPlots,
  1 as NumberOfIntermentRights,
  1 as NumberOfInurnmentRights,
  1 as ActiveOwner,
  'Live' DeedStatus,
  null as LeaseStartDate,
  null as LeaseExpiryDate,
  null as LeaseLengthYears,
  1 as PerpetualLease,
  group_concat(ifnull(p.Section,''),'/',ifnull(p.Row,''),'/',ifnull(p.PlotNumber,'')) as PlotsList,
  group_concat(p.PlotID) as PlotIdsList,
  null as Notes,
  0 as IsProblematic,
  0 as Deleted,
  1 as CreatedBy,
  NOW() as DateCreated,
  null as LastUpdatedBy,
  null as LastUpdatedDateTime,
  FROM_DAYS(t.date_start- 1721060) as PlotPurchaseDate,
  null as Cost,
  0 as IsLinkedInContract,
  t.id as ExtDeedIdentifier,
  ts.Transferred_To_ID as ExtTransferDeedIdentifier
  from transfer t
  join transfer_2_site ts on ts.transfer_id = t.id
  join sites s on s.id = ts.site_id
  join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblplot p on p.ImportPlotIdentifier = s.id
  /*where ts.Transferred_To_ID = '00000000-0000-0000-0000-000000000000'*/
  group by t.id;




/*Transferred Deeds

insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblDeed(
  DeedID,
  FacilityID,
  SiteID,
  CreatedForBurialID,
  DeedNumber,
  NumberOfPlots,
  NumberOfIntermentRights,
  NumberOfInurnmentRights,
  ActiveOwner,
  DeedStatus,
  LeaseStartDate,
  LeaseExpiryDate,
  LeaseLengthYears,
  PerpetualLease,
  PlotsList,
  PlotIdsList,
  Notes,
  IsProblematic,
  Deleted,
  CreatedBy,
  DateCreated,
  LastUpdatedBy,
  LastUpdatedDateTime,
  PlotPurchaseDate,
  Cost,
  IsLinkedInContract,
  ExtDeedIdentifier,
  ExtTransferDeedIdentifier
)
select 
	null as DeedId,
    CASE
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111101' then 1
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111102' then 2
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111103' then 3
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111104' then 4
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111105' then 5
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111106' then 6
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111107' then 7
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111108' then 8
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111109' then 9
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111110' then 10
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111111' then 11
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111112' then 12
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111113' then 13
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111114' then 14
    ELSE 1
    END as FacilityID,
  1 as SiteID,
  NUll as CreatedForBurialID,
  t.reference_number as DeedNumber,
  1 as NumberOfPlots,
  1 as NumberOfIntermentRights,
  1 as NumberOfInurnmentRights,
  0 as ActiveOwner,
  'Cancelled' DeedStatus,
  null as LeaseStartDate,
  null as LeaseExpiryDate,
  null as LeaseLengthYears,
  1 as PerpetualLease,
  group_concat(ifnull(p.Section,''),'/',ifnull(p.Row,''),'/',ifnull(p.PlotNumber,'')) as PlotsList,
  group_concat(p.PlotID) as PlotIdsList,
  null as Notes,
  0 as IsProblematic,
  0 as Deleted,
  1 as CreatedBy,
  NOW() as DateCreated,
  null as LastUpdatedBy,
  null as LastUpdatedDateTime,
  null as PlotPurchaseDate,
  null as Cost,
  0 as IsLinkedInContract,
  t.id as ExtDeedIdentifier,
  ts.Transferred_To_ID as ExtTransferDeedIdentifier
  from transfer t
  join transfer_2_site ts on ts.transfer_id = t.id
  join sites s on s.id = ts.site_id
  join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblplot p on p.ImportPlotIdentifier = s.id
  where ts.Transferred_To_ID <> '00000000-0000-0000-0000-000000000000'
  group by t.id, ts.Transferred_To_ID, ts.site_id;*/

Update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblDeed d
set d.ParentDeedID = d.DeedID;

/*update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tbldeed d
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tbldeed d2 on d.ExtTransferDeedIdentifier = d2.ExtDeedIdentifier
set d.ParentDeedID = d2.deedid
where d.ExtTransferDeedIdentifier <> '00000000-0000-0000-0000-000000000000';*/

Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblDeedOwner
(DeedOwnerID,
DeedID,
ContactID,
Precedence)
select
null as DeedOwnerID,
d.DeedID as DeedID,
c.ContactID as ContactID,
1 as Precedence
from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblcontact c
join persons p on p.id = c.ExtContactIdentifier
join transfer_2_RH t2 on t2.rh_id = p.id
join transfer t on t.id = t2.transfer_id
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tbldeed d on d.ExtDeedIdentifier = t.id
left join businesses b on b.id = c.ExtContactIdentifier;

Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblDeedOwner
(DeedOwnerID,
DeedID,
ContactID,
Precedence)
select
null as DeedOwnerID,
d.DeedID as DeedID,
c.ContactID as ContactID,
1 as Precedence
from oregonmetro_plotbox.tblcontact c
join businesses b on b.id = c.ExtContactIdentifier
join transfer_2_RH t2 on t2.rh_id = b.id
join transfer t on t.id = t2.transfer_id
join oregonmetro_plotbox.tbldeed d on d.ExtDeedIdentifier = t.id;

Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlotPurchase
	(PlotPurchaseID,
	DeedID,
	PlotID,
	Disabled,
	DisabledReason)
select
	null as PlotPurchaseID,
  d.DeedID as DeedID,
  p.PlotID as PlotID,
  0 as Disabled,
  null as DisabledReason
  from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblDeed d
  join transfer_2_site t2 on d.ExtDeedIdentifier = t2.transfer_id
  join sites s on s.id = t2.site_id
  join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblplot p on s.id = p.ImportPlotIdentifier
  where t2.Transferred_To_ID = '00000000-0000-0000-0000-000000000000'
  group by t2.site_id, t2.transferred_to_id;

/*Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlotPurchase
	(PlotPurchaseID,
	DeedID,
	PlotID,
	Disabled,
	DisabledReason)
select
	null as PlotPurchaseID,
  d.DeedID as DeedID,
  p.PlotID as PlotID,
  0 as Disabled,
  null as DisabledReason
  from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblDeed d
  join transfer_2_site t2 on d.ExtDeedIdentifier = t2.transfer_id
  join sites s on s.id = t2.site_id
  join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblplot p on s.id = p.ImportPlotIdentifier
  where d.`ExtTransferDeedIdentifier` <> '00000000-0000-0000-0000-000000000000'
  group by t2.site_id, t2.transferred_to_id;

Update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot p
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlotPurchase pp on p.PlotID = pp.PlotID
set p.CurrentDeedID = pp.DeedID,
p.Purchased = 1;
*/
/*update oregonmetro_plotbox.tblDeed d
join transfer_2_site t2 on t2.Transferred_To_ID = d.ExtDeedIdentifier
join oregonmetro_plotbox.tbldeed d2 on d2.ExtTransferDeedIdentifier = d.ExtDeedIdentifier
set d.ParentDeedID = d2.DeedID
where t2.Transferred_To_ID <> '00000000-0000-0000-0000-000000000000';
*/

delete from oregonmetro_plotbox.tblplotpurchase where plotpurchaseid in (select pp1.plotpurchaseid from(select pp.plotpurchaseid from oregonmetro_plotbox.tblplotpurchase pp
join oregonmetro_plotbox.tbldeed d on d.deedid = pp.deedid
join oregonmetro_source.Transfer_2_Site t on d.extdeedidentifier = t.transfer_id
join oregonmetro_plotbox.tblplot p on p.ImportPlotIdentifier = t.site_id
where d.exttransferdeedidentifier not like '00000000-0000-0000-0000-000000000000'
group by pp.plotpurchaseid
) pp1);

Update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot p
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlotPurchase pp on p.PlotID = pp.PlotID
set p.CurrentDeedID = pp.DeedID,
p.Purchased = 1;

update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tbldeed d
left join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblplotpurchase pp on (d.deedid = pp.deedid)
set d.activeowner = 0, d.deedstatus = 'Cancelled'
where pp.deedid is null;

Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlotPurchase
	(PlotPurchaseID,
	DeedID,
	PlotID,
	Disabled,
	DisabledReason)
select
	null as PlotPurchaseID,
  d.DeedID as DeedID,
  p.PlotID as PlotID,
  0 as Disabled,
  null as DisabledReason
  from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblDeed d
  join transfer_2_site t2 on d.ExtDeedIdentifier = t2.transfer_id
  join sites s on s.id = t2.site_id
  join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblplot p on s.id = p.ImportPlotIdentifier
  where d.`ExtTransferDeedIdentifier` <> '00000000-0000-0000-0000-000000000000'
  and d.deedstatus = 'Cancelled'
  group by t2.site_id, t2.transferred_to_id;

update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblDeed d
set d.PlotPurchaseDate = '0000-00-00' where d.PlotPurchaseDate = '9999-12-31';

select count(*) into deedCount from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblDeed;


RETURN deedCount;

END;
//
DELIMITER ;
