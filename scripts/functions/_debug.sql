DROP FUNCTION IF EXISTS `_debug`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `_debug`(msg varchar(255)) RETURNS int(11)
    DETERMINISTIC
BEGIN

insert into proc_debug(time_stamp, message) values(now(), msg);
RETURN 1;

END;
//
DELIMITER ;