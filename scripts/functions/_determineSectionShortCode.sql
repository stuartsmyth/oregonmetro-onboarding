DROP FUNCTION IF EXISTS `_determineSectionShortCode`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `_determineSectionShortCode`(	in_section_name varchar(50) ) 
	RETURNS VARCHAR(50) CHARSET utf8
    DETERMINISTIC
BEGIN

DECLARE section_short_name VARCHAR(10);

SET section_short_name = LEFT(in_section_name, 10);

RETURN section_short_name;

END;
//
DELIMITER ;