DROP FUNCTION IF EXISTS `_splitString`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `_splitString`(str VARCHAR(255), delim VARCHAR(12), pos INT) RETURNS varchar(255) CHARSET utf8
    DETERMINISTIC
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(str, delim, pos),
       LENGTH(SUBSTRING_INDEX(str, delim, pos -1)) + 1),
       delim, '');
//
DELIMITER ;