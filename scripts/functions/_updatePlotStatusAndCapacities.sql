DROP FUNCTION IF EXISTS `_updatePlotStatusAndCapacities`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `_updatePlotStatusAndCapacities`() RETURNS int(11)
    DETERMINISTIC
BEGIN

/* Capacity will have been set earlier so don't default it here 
update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot p set p.TotalCapacity = if(p.ImportSearchablePlotReference like '%d%/%d%', 2,1);*/
/*update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot p set p.TotalIntermentCapacity = if(p.ImportSearchablePlotReference like '%d%/%d%', 2,1);*/
update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot p set p.IntermentOccupancy = (select count(*) from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy g where g.PlotID = p.PlotID);
update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot p set p.TotalIntermentCapacity = p.IntermentOccupancy where p.IntermentOccupancy > p.TotalIntermentCapacity;
update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot p set p.RemainingIntermentCapacity = p.TotalIntermentCapacity - p.IntermentOccupancy;
update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot p set p.Purchased = 1 where p.IntermentOccupancy > 0;

update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot set PlotStatus = 'Full' where IntermentOccupancy >= TotalIntermentCapacity and PlotStatus not like '%On Hold%';
update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot set PlotStatus = 'Empty' where Purchased = 1 and IntermentOccupancy = 0;
update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot set PlotStatus = 'Partial' where IntermentOccupancy > 0 and IntermentOccupancy < TotalIntermentCapacity and PlotStatus not like '%On Hold%';
update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot p set p.PlotStatus = 'Available' where p.Purchased = 0 and ifnull((select count(*) from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy g where g.PlotID = p.PlotID), 0) = 0 and PlotStatus not like '%On Hold%';







update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFacility set IsForcedJsRegenRequired = 1;


return 0;

END;
//
DELIMITER ;
