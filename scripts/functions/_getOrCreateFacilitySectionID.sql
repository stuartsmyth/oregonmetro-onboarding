DROP FUNCTION IF EXISTS `_getOrCreateFacilitySectionID`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `_getOrCreateFacilitySectionID`(in_facilityid INT, in_section varchar(50)) RETURNS INT
    DETERMINISTIC
BEGIN

DECLARE facility_section_id int default 0;
DECLARE existing_section_id int default 0;

select ifnull(fs.FacilitySectionID, 0) into existing_section_id 
from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFacilitySection fs
where fs.FacilityID = in_facilityid and fs.SectionName = in_section;

if existing_section_id > 0 then
	SET facility_section_id = existing_section_id;
else 
	insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFacilitySection(FacilityID, SectionName, ShortCode)
	values (in_facilityid, in_section, _determineSectionShortCode(in_section));
	SET facility_section_id = LAST_INSERT_ID();
end if;

RETURN facility_section_id;

END;
//
DELIMITER ;