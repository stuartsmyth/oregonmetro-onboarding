DROP FUNCTION IF EXISTS `_importRiskAssessmentTables`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `_importRiskAssessmentTables`() RETURNS int(11)
    DETERMINISTIC
BEGIN

DECLARE assessment_count INT default 0;

RETURN assessment_count;

END;
//
DELIMITER ;
