DROP FUNCTION IF EXISTS `_importMemorialTables`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `_importMemorialTables`() RETURNS int(11)
    DETERMINISTIC
BEGIN


DECLARE memorial_count INT default 0;
DECLARE debug VARCHAR(200);

    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFee (
        `FeeName`,
        `FeeType`,
        `VariablePrice`,
        `CatalogItemCode`
    )
    select
        m.Name as FeeName,
        'Memorial Garden Memorials' as FeeType,
        1 as VariablePrice,
        m.Code as CatalogItemCode
        from MEMTYPE m;

    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFee (
        `FeeName`,
        `FeeType`,
        `VariablePrice`,
        `CatalogItemCode`
    )
    values
    ('Plaque', 'Memorial Garden Memorials', 1, 'PLQ');

    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFacilitySection (
        `FacilityID`,
        `SectionName`,
        `ShortCode`
    )
    Select
        1 as FacilityID,
        m.Locn1 as SectionName,
        m.Locn1 as SectionShortCode
        from MMMREG m
        group by m.Locn1;

    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot (
        `FacilityId`,
        `FacilitySectionID`,
        `Section`,
        `SectionShortCode`,
        `Row`,
        `PlotNumber`
    )
    select
        1 as FacilityId,
        fs.FacilitySectionID as FacilitySectionID,
        m.Locn1 as Section,
        m.Locn1 as SectionShortCode,
        m.Locn2 as Row,
        m.Locn3 as PlotNumber
        from MMMREG m
        join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFacilitySection fs on fs.SectionName like m.Locn1
        group by m.Locn1, m.Locn2, m.Locn3;


    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorial (
        `MemorialType`,
        `FacilityID`,
        `SiteID`,
        `OwnerContactID`,
        `MemorialDeedNumber`,
        `MemorialPurchaseDate`,
        `LeaseStartDate`,
        `LeaseExpiryDate`,
        `LeaseLengthYears`,
        `PlotID`,
        `ExtMemIdentifier`,
        `Notes`
    )
    select
        'Garden Memorials' as MemorialType,
        1 as FacilityId,
        1 as SiteID,
        c.ContactId as OwnerContactID,
        concat(m.MemType, '-', m.AgrNo) as MemorialDeedNumber,
        str_to_Date(m.Comments, '%Y-%m-%d') as MemorialPurchaseDate,
        str_to_Date(m.OrdDate, '%Y-%m-%d') as LeaseStartDate,
        str_to_Date(m.LeaseStart, '%Y-%m-%d') as LeaseExpireyDate,
        m.LeaseEnd as LeaseLengthYears,
        p.PlotID as PlotID,
        m.Seq as ExtMemIdentifier,
        null as Notes
        from MMMREG m
        left join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact c on c.ExtContactIdentifier = concat('AP-M', ' ', m.Seq)
        join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot p on ifnull(m.Locn1,'') = ifnull(p.Section,'') and ifnull(m.Locn2,'') = ifnull(p.Row,'') and ifnull(m.Locn3,'') = ifnull(p.PlotNumber,'')
        group by m.seq;

    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItem (
        `MemorialId`,
        `FeeID`,
        `FeeType`,
        `Description`,
        `CreatedDate`,
        `ExtMitemIdentifier`
    )
    select
        pm.MemorialId as MemorialID,
        f.FeeID as FeeID,
        f.FeeType as FeeType,
        f.FeeName,
        pm.MemorialPurchaseDate as CreatedDate,
        m.Seq as ExtMitemIdentifier
        from MMMREG m
        join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorial pm on pm.ExtMemIdentifier = m.Seq
        join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFee f on f.CatalogItemCode = m.MemType;

    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItemDetail (
        `MemorialItemID`,
        `InscriptionText`,
        `Precedence`,
        `ManufactureDate`,
        `ReceivedDate`,
        `InstallationDate`,
        `NotificationDate`,
        `CreatedDate`
    )
    select
    mi.MemorialItemID as MemorialItemID,
    p.Inscr1 as InscriptionText,
    p.PlaqNo as Precedence,
    str_to_Date(p.ManufODate, '%Y-%m-%d') as ManufactureDate,
    str_to_Date(p.PlaqRecdDate, '%Y-%m-%d') as ReceivedDate,
    str_to_Date(p.PlaqFixDate, '%Y-%m-%d') as InstallationDate,
    str_to_Date(p.NotifDate, '%Y-%m-%d') as NotificationDate,
    null as CreatedDate
    from MPlaqReg p 
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItem mi on mi.ExtMitemIdentifier = p.MemlSeq;

Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorial (
        `MemorialType`,
        `FacilityID`,
        `SiteID`,
        `OwnerContactID`,
        `MemorialDeedNumber`,
        `MemorialPurchaseDate`,
        `LeaseStartDate`,
        `LeaseExpiryDate`,
        `LeaseLengthYears`,
        `PlotID`,
        `ExtMemIdentifier`,
        `Notes`
    )
    select
    'Books/Scrolls' as MemorialType,
    1 as FacilityID,
    1 as SiteID,
    c.ContactID as OwnerContactID,
    concat('BOR-',bi.BorNo) as MemorialDeedNumber,
    str_to_Date(bi.OrderDate, '%Y-%m-%d') as MemorialPurchaseDate,
    str_to_Date(bi.OrderDate, '%Y-%m-%d') as LeaseStartDate,
    null as LeaseExpireyDate,
    0 as LeaseLengthYears,
    null as PlotId,
    concat('B-',bi.BorSeq) as ExtMemIdentifier,
    concat('Item type: ', t.Name) as Notes
    from MBRITEMS bi
    join MBORREG b on bi.borseq = b.seq
    left join doncaster_plotbox.tblContact c on c.CustomerNumber = b.appseq
    join MBRITTYP t on t.Code = bi.ItemType;

    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItem (
        `MemorialId`,
        `FeeID`,
        `FeeType`,
        `Amount`,
        `Description`,
        `CreatedDate`,
        `ExtMitemIdentifier`
    )
    select
    m.MemorialID as MemorialId,
    f.FeeID as FeeID,
    f.FeeType as feetype,
    mb.price as amount,
    mt.Name as Description,
    m.MemorialPurchaseDate as CreatedDate,
    m.ExtMemIdentifier as ExtMitemIdentifier
    from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorial m
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFee f on f.CatalogItemCode like 'BK OF REM'
    join MBRITEMS mb on concat('B-',mb.BorSeq) = m.ExtMemIdentifier
    join MBRITTYP mt on mt.Code like mb.ItemType
    where m.ExtMemIdentifier like 'B-%';

    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItemDetail (
        `MemorialItemID`,
        `InscriptionText`,
        `Precedence`,
        `ManufactureDate`,
        `ReceivedDate`,
        `InstallationDate`,
        `NotificationDate`,
        `CreatedDate`
    )
    select
    mi.MemorialItemID as MemorialItemID,
    br.Ins1 as InscriptionText,
    1 as Precedence,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ManufactureDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ReceivedDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as InstallationDate,
    null as NotificationDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as CreatedDate
    from MBORREG br
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItem mi on mi.ExtMitemIdentifier = concat('B-',br.Seq);

    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItemDetail (
        `MemorialItemID`,
        `InscriptionText`,
        `Precedence`,
        `ManufactureDate`,
        `ReceivedDate`,
        `InstallationDate`,
        `NotificationDate`,
        `CreatedDate`
    )
    select
    mi.MemorialItemID as MemorialItemID,
    br.Ins2 as InscriptionText,
    2 as Precedence,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ManufactureDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ReceivedDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as InstallationDate,
    null as NotificationDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as CreatedDate
    from MBORREG br
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItem mi on mi.ExtMitemIdentifier = concat('B-',br.Seq)
    where br.Ins2 is not null;

     Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItemDetail (
        `MemorialItemID`,
        `InscriptionText`,
        `Precedence`,
        `ManufactureDate`,
        `ReceivedDate`,
        `InstallationDate`,
        `NotificationDate`,
        `CreatedDate`
    )
    select
    mi.MemorialItemID as MemorialItemID,
    br.Ins3 as InscriptionText,
    3 as Precedence,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ManufactureDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ReceivedDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as InstallationDate,
    null as NotificationDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as CreatedDate
    from MBORREG br
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItem mi on mi.ExtMitemIdentifier = concat('B-',br.Seq)
    where br.Ins3 is not null;


 Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItemDetail (
        `MemorialItemID`,
        `InscriptionText`,
        `Precedence`,
        `ManufactureDate`,
        `ReceivedDate`,
        `InstallationDate`,
        `NotificationDate`,
        `CreatedDate`
    )
    select
    mi.MemorialItemID as MemorialItemID,
    br.Ins4 as InscriptionText,
    4 as Precedence,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ManufactureDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ReceivedDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as InstallationDate,
    null as NotificationDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as CreatedDate
    from MBORREG br
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItem mi on mi.ExtMitemIdentifier = concat('B-',br.Seq)
    where br.Ins4 is not null;

 Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItemDetail (
        `MemorialItemID`,
        `InscriptionText`,
        `Precedence`,
        `ManufactureDate`,
        `ReceivedDate`,
        `InstallationDate`,
        `NotificationDate`,
        `CreatedDate`
    )
    select
    mi.MemorialItemID as MemorialItemID,
    br.Ins5 as InscriptionText,
    5 as Precedence,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ManufactureDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ReceivedDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as InstallationDate,
    null as NotificationDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as CreatedDate
    from MBORREG br
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItem mi on mi.ExtMitemIdentifier = concat('B-',br.Seq)
    where br.Ins5 is not null;

 Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItemDetail (
        `MemorialItemID`,
        `InscriptionText`,
        `Precedence`,
        `ManufactureDate`,
        `ReceivedDate`,
        `InstallationDate`,
        `NotificationDate`,
        `CreatedDate`
    )
    select
    mi.MemorialItemID as MemorialItemID,
    br.Ins6 as InscriptionText,
    6 as Precedence,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ManufactureDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ReceivedDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as InstallationDate,
    null as NotificationDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as CreatedDate
    from MBORREG br
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItem mi on mi.ExtMitemIdentifier = concat('B-',br.Seq)
    where br.Ins6 is not null;

 Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItemDetail (
        `MemorialItemID`,
        `InscriptionText`,
        `Precedence`,
        `ManufactureDate`,
        `ReceivedDate`,
        `InstallationDate`,
        `NotificationDate`,
        `CreatedDate`
    )
    select
    mi.MemorialItemID as MemorialItemID,
    br.Ins7 as InscriptionText,
    7 as Precedence,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ManufactureDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ReceivedDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as InstallationDate,
    null as NotificationDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as CreatedDate
    from MBORREG br
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItem mi on mi.ExtMitemIdentifier = concat('B-',br.Seq)
    where br.Ins7 is not null;

 Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItemDetail (
        `MemorialItemID`,
        `InscriptionText`,
        `Precedence`,
        `ManufactureDate`,
        `ReceivedDate`,
        `InstallationDate`,
        `NotificationDate`,
        `CreatedDate`
    )
    select
    mi.MemorialItemID as MemorialItemID,
    br.Ins8 as InscriptionText,
    8 as Precedence,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ManufactureDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as ReceivedDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as InstallationDate,
    null as NotificationDate,
    str_to_Date(br.EntryDate, '%Y-%m-%d') as CreatedDate
    from MBORREG br
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItem mi on mi.ExtMitemIdentifier = concat('B-',br.Seq)
    where br.Ins8 is not null;

  /*Memorial Genealogy*/
        Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact (
        `Title`,
        `Forename`,
        `MiddleName`,
        `Surname`,
        `FullName`,
        `AddressLine1`,
        `Town`,
        `County`,
        `Country`,
        `Deceased`,
        `ExtContactIdentifierName`,
        `ExtContactIdentifier`
    )
    select
    si.Title as Title,
    si.FName as Forename,
    null as MiddleName,
    si.SNAME as Surname,
    concat(si.FName, ' ', si.SNAME) as Fullname,
    null as AddressLine1,
    null as Town,
    null as County,
    null as Country,
    1 as Deceased,
    null as ExtContactIdentifierName,
    concat('M- ', si.Seq) as ExtContactIdentifier
    from SINDEX si
    where si.Type = 'M';

 Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy (
        `FacilityID`,
        `ContactID`,
        `GenealogyForename`,
        `GenealogySurname`,
        `DeceasedDate`,
        `FuneralType`
    )
        select
    1 as FacilityID,
    c.ContactID as ContactID,
    c.Forename as GenealogyForename,
    c.Surname as GenealogySurname,
    str_to_date(si.DeathDate, '%Y-%m-%d') as DeceasedDate,
    'Cremation' as FuneralType
    from  SINDEX si 
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact c on c.ExtContactIdentifier = concat('M- ', si.Seq);

    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialGenealogy (
        `MemorialId`,
        `GenealogyID`
    )
    select
        m.MemorialID,
        g.GenealogyID
    from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorial m
    join SINDEX si on si.SourceSeq = m.ExtMemIdentifier
    /*join SINDEX si2 on si.RegNo = si2.RegNo and si.Seq <> si2.Seq*/
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact c on c.ExtContactIdentifier = concat('M- ', si.Seq)
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy g on g.ContactID = c.ContactID;

   /* Insert into lcc_plotbox.tblMemorialGenealogy (
        `MemorialId`,
        `GenealogyID`
    )
    select
        m.MemorialID,
        g.GenealogyID
    from lcc_plotbox.tblMemorial m
    join SINDEX si on si.SourceSeq = m.ExtMemIdentifier
    join SINDEX si2 on si.RegNo = si2.RegNo and si.Seq <> si2.Seq
    join lcc_plotbox.tblContact c on c.ExtContactIdentifier = si2.Seq
    join lcc_plotbox.tblGenealogy g on g.ContactID = c.ContactID
    where si.RegNo not like 0;*/

        Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact (
        `Title`,
        `Forename`,
        `MiddleName`,
        `Surname`,
        `FullName`,
        `AddressLine1`,
        `Town`,
        `County`,
        `Country`,
        `Deceased`,
        `ExtContactIdentifierName`,
        `ExtContactIdentifier`
    )
    select
    si.Title as Title,
    si.FName as Forename,
    null as MiddleName,
    si.SNAME as Surname,
    concat(si.FName, ' ', si.SNAME) as Fullname,
    null as AddressLine1,
    null as Town,
    null as County,
    null as Country,
    1 as Deceased,
    null as ExtContactIdentifierName,
    concat('R- ', si.Seq) as ExtContactIdentifier
    from SINDEX si
    where si.Type = 'R';

 Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy (
        `FacilityID`,
        `ContactID`,
        `GenealogyForename`,
        `GenealogySurname`,
        `DeceasedDate`,
        `FuneralType`
    )
        select
    1 as FacilityID,
    c.ContactID as ContactID,
    c.Forename as GenealogyForename,
    c.Surname as GenealogySurname,
    str_to_date(si.DeathDate, '%Y-%m-%d') as DeceasedDate,
    'Cremation' as FuneralType
    from  SINDEX si 
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact c on c.ExtContactIdentifier = concat('R- ', si.Seq);
    
    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialGenealogy (
        `MemorialId`,
        `GenealogyID`
    )
    select
        m.MemorialID,
        g.GenealogyID
    from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorial m
    join SINDEX si on concat('B-',si.SourceSeq) = m.ExtMemIdentifier
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact c on c.ExtContactIdentifier =concat('R- ', si.Seq)
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy g on g.ContactID = c.ContactID;

    /*Headstome memorials*/

    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact (
        `Title`,
        `Forename`,
        `MiddleName`,
        `Surname`,
        `FullName`,
        `AddressLine1`,
        `Town`,
        `County`,
        `Country`,
        `Deceased`,
        `ExtContactIdentifierName`,
        `ExtContactIdentifier`,
        `CustomerNumber`
    )
    select
    si.Title as Title,
    si.FName as Forename,
    null as MiddleName,
    si.SNAME as Surname,
    concat(si.FName, ' ', si.SNAME) as Fullname,
    null as AddressLine1,
    null as Town,
    null as County,
    null as Country,
    1 as Deceased,
    'H' as ExtContactIdentifierName,
    concat('H - ', si.Seq) as ExtContactIdentifier,
    si.SourceSeq as CustomerNumber
    from SINDEX si
    where si.Type = 'H';

    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy (
        `PlotID`,
        `FacilityID`,
        `ContactID`,
        `GenealogyForename`,
        `GenealogySurname`,
        `DeceasedDate`,
        `BurialDate`
    )
     select
    null as PlotID,
        CASE
    WHEN si.cemcremcode = 'ARKSYCEM' then 2
    WHEN si.cemcremcode = 'ARKSOCEM' then 3
    WHEN si.cemcremcode = 'ASKRNCEM' then 4
    WHEN si.cemcremcode = 'CAMPSCEM' then 5
    WHEN si.cemcremcode = 'CONISCEM' then 6
    WHEN si.cemcremcode = 'DENABCEM' then 7
    WHEN si.cemcremcode = 'DONCOCEM' then 8
    WHEN si.cemcremcode = 'MEXBRCEM' then 9
    WHEN si.cemcremcode = 'REDHSCEM' then 10
    WHEN si.cemcremcode = 'ROSEHCEM' then 11
    WHEN si.cemcremcode = 'RSP' then 11
    WHEN si.cemcremcode = 'ROSSGCEM' then 12
    WHEN si.cemcremcode = 'WARMSCEM' then 13
    WHEN si.cemcremcode = 'WINDBCEM' then 14
    ELSE 1
    END as FacilityID,
    c.ContactID as ContactID,
    c.Forename as GenealogyForename,
    c.Surname as GenealogySurname,
    str_to_date(si.DeathDate, '%Y-%m-%d') as DeceasedDate,
    'Burial' as FuneralType
    from  SINDEX si 
    join doncaster_plotbox.tblContact c on c.ExtContactIdentifier = concat('H - ', si.Seq)
    join GRAVMEML g on g.Seq = si.SourceSeq
    join doncaster_plotbox.tblPlot p on p.importPlotIdentifier = g.grvseq
    ;


     Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorial (
        `MemorialType`,
        `FacilityID`,
        `SiteID`,
        `OwnerContactID`,
        `MemorialDeedNumber`,
        `MemorialPurchaseDate`,
        `LeaseStartDate`,
        `LeaseExpiryDate`,
        `LeaseLengthYears`,
        `PlotID`,
        `ExtMemIdentifier`,
        `Notes`
    )
               select
        'Garden Memorials' as MemorialType,
        p.FacilityId as FacilityID,
        1 as SiteID,
        null as OwnerContactID,
        concat(g.MemType, '-', ifnull(p.section,''), '/', ifnull(p.Row, ''), '/', ifnull(p.PlotNumber,'')) as MemorialDeedNumber,
        null as MemorialPurchaseDate,
        null as LeaseStartDate,
        null as LeaseExpireyDate,
       null as LeaseLengthYears,
        p.PlotID as PlotID,
        g.Seq as ExtMemIdentifier,
        concat('Comments: ', ifnull(g.Comments, ''), '\n',
        'Height: ', ifnull(g.height, ''), '\n',
        'Width: ', ifnull(g.width, ''), '\n',
        'Depth: ', ifnull(g.depth, ''), '\n',
        'Base Height: ', ifnull(g.baseheight, ''), '\n',
        'Base Width: ', ifnull(g.basewidth, ''), '\n',
        'Base Depth: ', ifnull(g.basedepth, ''), '\n',
        'Permit', '\n',
        'Mason: ', ifnull(f.funeraldirectorname,''), '\n',
        'Permit Number: ', ifnull(p.permitno,''), '\n',
        'Approved Date: ', ifnull(p.ApprovedDate, ''), '\n',
        'Paid Date: ', ifnull(p.PaidDate, ''), '\n',
        'Fee: ', ifnull(p.fee,''), '\n',
        'Receipt Number: ', ifnull(p.RecptNo,''), '\n',
        'Comments: ', ifnull(p.Comments,''), '\n',
        'Received: ', ifnull(p.Received,'')) as Notes
        from GRAVMEML g
        join doncaster_plotbox.tblPlot p on g.GrvSeq = p.ImportPlotIdentifier
        left join BGRVPERM p on p.memlseq = g.seq
        left join doncaster_plotbox.tblContact fun on fun.ExtContactIdentifier = p.Mason and fun.ExtContactIdentifierName = 'FUNDIR'
    left join doncaster_plotbox.tblFuneralDirector f on f.ContactID = fun.ContactID
        group by g.seq;

    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItem (
        `MemorialId`,
        `FeeID`,
        `FeeType`,
        `Description`,
        `CreatedDate`,
        `ExtMitemIdentifier`
    )
    select
        pm.MemorialId as MemorialID,
        f.FeeID as FeeID,
        f.FeeType as FeeType,
        f.FeeName,
        pm.MemorialPurchaseDate as CreatedDate,
        m.Seq as ExtMitemIdentifier
        from GRAVMEML m
        join DONCASTER_PLOTBOX.tblMemorial pm on pm.ExtMemIdentifier = m.Seq
        join DONCASTER_PLOTBOX.tblFee f on f.CatalogItemCode = m.MemType;

   Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialItemDetail (
        `MemorialItemID`,
        `InscriptionText`,
        `Precedence`,
        `ManufactureDate`,
        `ReceivedDate`,
        `InstallationDate`,
        `NotificationDate`,
        `CreatedDate`
    )
  select
    mi.MemorialItemID as MemorialItemID,
    p.Inscript as InscriptionText,
    1 as Precedence,
    null as ManufactureDate,
    null as ReceivedDate,
    null as InstallationDate,
    null as NotificationDate,
    null as CreatedDate
    from GRAVMEML p 
    join doncaster_plotbox.tblMemorialItem mi on mi.ExtMitemIdentifier = p.Seq
    where p.inscript is not null;

 Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorialGenealogy (
        `MemorialId`,
        `GenealogyID`
    )
    select
        m.MemorialID,
        g.GenealogyID
    from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblMemorial m
    join SINDEX si on si.SourceSeq = m.ExtMemIdentifier
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact c on c.ExtContactIdentifier =concat('H - ', si.Seq)
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy g on g.ContactID = c.ContactID;

RETURN memorial_count;

END;
//
DELIMITER ;
