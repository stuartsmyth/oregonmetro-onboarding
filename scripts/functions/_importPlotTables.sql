DROP FUNCTION IF EXISTS `_importPlotTables`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `_importPlotTables`() RETURNS int(11)
    DETERMINISTIC
BEGIN


DECLARE plot_count INT default 0;
DECLARE debug VARCHAR(200);

   
   Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFacilitySection(
	`FacilitySectionID`,
	`FacilityID`,
	`SectionName`,
	`ShortCode`,
	`IsDeleted`)
select
    null as FacilitySectionID,
    CASE
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111101' then 1
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111102' then 2
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111103' then 3
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111104' then 4
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111105' then 5
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111106' then 6
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111107' then 7
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111108' then 8
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111109' then 9
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111110' then 10
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111111' then 11
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111112' then 12
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111113' then 13
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111114' then 14
    ELSE 1
    END as FacilityID,
  _splitstring(s.full_name,'-',1) as SectionName,
  _splitstring(s.full_name,'-',1) as SectionShortCode,
  0 as IsDeleted
  from sites s
  group by _splitstring(s.full_name,'-',1), s.cemetery_id;

insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot(
  FacilityID,
  Section,
  FacilitySectionID,
  Row,
  PlotNumber,
  SectionShortCode,
  IntermentOccupancy,
  TotalInurnmentCapacity,
  RemainingIntermentCapacity,
  RemainingInurnmentCapacity,
  ImportSearchablePlotReference,
  ImportPlotIdentifier,
  PlotStatus,
  PlotCost,
  PlotType,
  Notes)
select 
   CASE
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111101' then 1
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111102' then 2
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111103' then 3
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111104' then 4
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111105' then 5
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111106' then 6
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111107' then 7
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111108' then 8
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111109' then 9
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111110' then 10
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111111' then 11
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111112' then 12
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111113' then 13
    WHEN s.Cemetery_ID = '11111111-1111-1111-1111-111111111114' then 14
    ELSE 1
    END as FacilityID,
   _splitstring(s.full_name,'-',1) as Section,
  null as FacilitySectionID,
   _splitstring(s.full_name,'-',2) as Row,
   concat(
      (select _splitString(full_name,'-',3)),
     CASE WHEN (select _splitString(full_name,'-',4)) <> ''
          THEN concat('-',(select _splitString(full_name,'-',4)),
          if((select _splitString(full_name,'-',5)) <>'',concat('-',(select _splitString(full_name,'-',5))),'' ),
          if((select _splitString(full_name,'-',6)) <>'',concat('-',(select _splitString(full_name,'-',6))),'' ))
          ELSE ''
      END
      )  as PlotNumber,
  null as SectionShortCode,
  s.TraditionalInterment_Capacity as IntermentOccupancy,
  1 as RemainingIntermentCapacity,
  s.CremationInterment_Capacity as TotalInurnmentCapacity,
  1 as RemainingInurnmentCapacity,
  s.full_name as ImportSearchablePlotReference,
  s.ID as ImportPlotIdentifier,
  'Available' as PlotStatus,
  null as PlotCost,
  d.specification as PlotType,
  concat('Memo: ', ifnull(s.memo1,''), '\n',
  'Name Description: ', ifnull(s.name_description,''), '\n',
  'Site Length: ', ifnull(s.site_length,''),'\n',
  'Site Width: ', ifnull(s.site_width,''),'\n',
  'Site Depth: ', ifnull(s.site_depth,''),'\n',
  'Memorialization: ', ifnull(s.Memorialization,''),'\n',
  '\n',
  'Marker: ', ifnull(m.name,''),'\n',
  ifnull(d2.specification,''),'\n',
  ifnull(m.memo,'')) as Notes
  from sites s
  join descriptors d on s.kind_ID = d.ID
  left join markers m on m.site_id = s.ID
  left join descriptors d2 on d2.id = m.kind_id;

Update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPlot p
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFacilitySection f on f.FacilityID = p.FacilityID
and f.SectionName = p.Section
set p.FacilitySectionID = f.FacilitySectionID,
p.SectionShortCode = f.ShortCode;

RETURN plot_count;

END;
//
DELIMITER ;
