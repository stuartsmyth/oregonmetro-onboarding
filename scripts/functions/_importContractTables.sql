DROP FUNCTION IF EXISTS `_importContractTables`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `_importContractTables`() RETURNS int(11)
    DETERMINISTIC
BEGIN


DECLARE plot_count INT default 0;
DECLARE debug VARCHAR(200);

    INSERT INTO PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContract 
    (`ContractID`,
     `InvoiceID`, 
     `DeedID`,
    `DivisionID`, 
    `SiteID`, 
    `FacilityID`, 
    `FacilitySectionID`, 
    `ContractNumber`, 
    `Status`, 
    `Requirement`, 
    `DownPayment`, 
    `AnnualPercentageRate`, 
    `TotalCashPrice`, 
    `InstallmentAmount`, 
    `NumberOfInstallments`, 
    `RemainingBalance`, 
    `RemainingInstallments`, 
    `InstallmentAutoPay`, 
    `TotalSalePrice`, 
    `CreatedDate`, 
    `NextExpectedPaymentDate`, 
    `FirstPaymentDate`, 
    `PurchaseDate`, 
    `CompletionDate`, 
    `DocuSignID`, 
    `CommissionTotal`, 
    `Notes`, 
    `Deleted`, 
    `Source`, 
    `ExtContractIdentifierName`, 
    `ExtContractIdentifier`, 
    `CreatedBy`, 
    `DeedRequired`, 
    `ContractCancelReason`, 
    `CancellationDate`, 
    `CancelledBy`, 
    `InstalmentCalculator`, 
    `DownPaymentDueDate`, 
    `Historic`)
    SELECT
        null as ContractID,
        null as InvoiceID,
        null as DeedID,
        1 as DivisionId,
        1 as SiteID,
        CASE
    WHEN s.Business_ID = '11111111-1111-1111-1111-111111111101' then 1
    WHEN s.Business_ID = '11111111-1111-1111-1111-111111111102' then 2
    WHEN s.Business_ID = '11111111-1111-1111-1111-111111111103' then 3
    WHEN s.Business_ID = '11111111-1111-1111-1111-111111111104' then 4
    WHEN s.Business_ID = '11111111-1111-1111-1111-111111111105' then 5
    WHEN s.Business_ID = '11111111-1111-1111-1111-111111111106' then 6
    WHEN s.Business_ID = '11111111-1111-1111-1111-111111111107' then 7
    WHEN s.Business_ID = '11111111-1111-1111-1111-111111111108' then 8
    WHEN s.Business_ID = '11111111-1111-1111-1111-111111111109' then 9
    WHEN s.Business_ID = '11111111-1111-1111-1111-111111111110' then 10
    WHEN s.Business_ID = '11111111-1111-1111-1111-111111111111' then 11
    WHEN s.Business_ID = '11111111-1111-1111-1111-111111111112' then 12
    WHEN s.Business_ID = '11111111-1111-1111-1111-111111111113' then 13
    WHEN s.Business_ID = '11111111-1111-1111-1111-111111111114' then 14
    ELSE 1
    END as FacilityID,
        null as FacilitySectionID,
        s.reference_Number as ContractNumber,
       'Approved' as Status,
        'Pre-Need' as Requirement,
        null as DownPayment,
        null as AnnualPercentageRate,
        t.dollar_amount as TotalCashPrice,
        null as InstallmentAmount,
        0 as NumberOfInstallments,
        s.sales_balance as RemainingBalance,
        null as RemainingInstallments,
        0 as InstallmentAutoPay,
       t.dollar_amount as TotalSalePrice,
        s.date_created as CreatedDate,
        null as NextExpectedPaymentDate,
        t.date_processed as FirstPaymentDate,
        s.date_created as PurchaseDate,
        null as CompletionDate,
        null as DocuSignID,
        null as CommissionTotal,
        s.memo as Notes,
        0 as Deleted,
        'Import' as Source,
        'ID' as ExtContractIdentifierName,
        s.id as ExtContractIdentifier,
        null as CreatedBy,
        0 as DeedRequired,
        null as ContractCancelReason,
        null as CancellationDate,
        null as CancelledBy,
        'Installment Amount' as InstallmentCalculator,
        null as DownPaymentDueDate,
        0 as Historic
        from sale s 
        join Transactions t on t.sale_id = s.id
        where t.product_id = '00000000-0000-0000-0000-000000000000'
        and t.sequence = 1;

    Insert INTO PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContractOwner (
        `ContractID`,
        `ContactID`,
        `Precedence`
    )
    select 
        pc.ContractID as ContractID,
        pcon.ContactID  as ContactID,
        1 as Precedence
        from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContract pc 
        join sale s on s.id = pc.ExtContractIdentifier
        join sale_2_customer s2 on s2.sale_id = s.id
        join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact pcon on pcon.ExtContactIdentifier = s2.customer_id;


  INSERT INTO PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFee(
            `FeeID`,
            `FacilityID`,
            `FacilitySectionID`,
            `FeeName`,
            `FeeType`,
            `FeeAmount`,
            `TaxRateID`,
            `IsDeleted`,
            `NominalCode`,
            `Quantity`,
            `CommissionPreNeed`,
            `CommissionAtNeed`,
            `CommissionAtFill`,
            `VariablePrice`,
            `NominalCodeID`,
            `ConversionNominalCodeID`,
            `FeeAN`,
            `FeePN`,
            `NeedCategory`,
            `CostPrice`,
            `CostPriceNominalCodeID`,
            `IsHistoric`,
            `PreNeedCostNominalCodeID`,
            `InventoryNominalCodeID`,
            `PreNeedInventoryNominalCodeID`,
            `SystemOnly`,
            `InternalDescription`,
            `ExtProductTypeIdentifier`,
            `ExtProductIdentifier`
        )
        SELECT
            null as FeeID,
             CASE
    WHEN pt.Business_ID = '11111111-1111-1111-1111-111111111101' then 1
    WHEN pt.Business_ID = '11111111-1111-1111-1111-111111111102' then 2
    WHEN pt.Business_ID = '11111111-1111-1111-1111-111111111103' then 3
    WHEN pt.Business_ID = '11111111-1111-1111-1111-111111111104' then 4
    WHEN pt.Business_ID = '11111111-1111-1111-1111-111111111105' then 5
    WHEN pt.Business_ID = '11111111-1111-1111-1111-111111111106' then 6
    WHEN pt.Business_ID = '11111111-1111-1111-1111-111111111107' then 7
    WHEN pt.Business_ID = '11111111-1111-1111-1111-111111111108' then 8
    WHEN pt.Business_ID = '11111111-1111-1111-1111-111111111109' then 9
    WHEN pt.Business_ID = '11111111-1111-1111-1111-111111111110' then 10
    WHEN pt.Business_ID = '11111111-1111-1111-1111-111111111111' then 11
    WHEN pt.Business_ID = '11111111-1111-1111-1111-111111111112' then 12
    WHEN pt.Business_ID = '11111111-1111-1111-1111-111111111113' then 13
    WHEN pt.Business_ID = '11111111-1111-1111-1111-111111111114' then 14
    ELSE 1
    END as FacilityID,
            null as FacilitySectionID,
            concat(pt.name, '-', p.description)  as FeeName,
            'Contract Service' as FeeType,
            p.list_price as FeeAmount,
            0 as TaxRateID,
            if(p.date_deleted = '9999-12-31 23:59:59.997000', 0, 1) as isDeleted,
            null as NominalCode,
            null as Quantity,
            0 as CommissionPreNeed,
            0 as CommissionAtNeed,
            0 as CommissionAtFill,
            0 as VariablePrice,
            null as NominalCodeID,
            null as ConversionNominalCodeID,
            0 as FeeAN,
            1 as FeePN,
            'Pre-Need' as NeedCategory,
            p.list_price as CostPrice,
            null as CostPriceNominalCodeID,
            0 as IsHistoric,
            null as PreNeedCostNominalCodeID,
            null as InventoryNominalCodeID,
            null as PreNeedInventoryNominalCodeID,
            0 as SystemOnly,
           concat('Product Type Description: ', ifnull(pt.description,''),'\n',
           'Detail Description: ', ifnull(p.detail_description,'')) as InternalDescription,
           pt.ID as ExtProductTypeIdentifier,
           p.ID as ExtProductIdentifier
            FROM product_types pt
            join products p on pt.id = p.product_type_id;

Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContractItems(
    `ContractItemID`,
    `ContractID`,
    `FeeID`,
    `Description`,
    `CreatedDate`,
    `FulfilledDate`,
    `FulfilledBy`,
    `Amount`,
    `TaxRate`,
    `Tax`,
    `Quantity`,
    `ReferenceFeeID`,
    `ItemStatus`,
    `ConversionDate`,
    `QuantityFulfilled`,
    `CreatedBy`,
    `DateCreated`,
    `LastUpdatedBy`,
    `LastUpdatedDateTime`,
    `CreditContractID`,
    `FeeType`,
    `UnitDiscount`
    )
    select
    null as contractItemID,
    c.contractID as ContractID,
    f.FeeID as FeeID,
    f.FeeName as Description,
    t.date_created as CreatedDate,
    null as FulfilledDate,
    null as FulfilledBy,
    t.Quantity * t.dollar_amount as Amount,
    null as TaxRate,
    null as Tax,
    t.Quantity as Quantity,
    null as ReferenceFeeID,
    'Pre-Need' as ItemStatus,
    null as ConversionDate,
    null as QuantityFulfilled,
    null as CreatedBy,
    NOW() as DateCreated,
    null as LastUpdatedBy,
    NOW() as LastUpdatedDateTime,
    null as CreditContractID,
    f.FeeType as FeeType,
    null as UnitDiscount
    from Transactions t 
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContract c on c.ExtContractIdentifier = t.sale_id
    join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFee f on f.ExtProductIdentifier = t.product_id;

    INSERT INTO PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblInvoice 
    (`InvoiceID`, 
    `Status`,
    `BurialID`, 
    `DeedID`, 
    `MemorialID`, 
    `CremationID`, 
    `FuneralDirectorID`, 
    `MemorialMasonID`, 
    `SiteID`, 
    `FacilityID`, 
    `InvoiceRefID`, 
    `PayerContactID`, 
    `InvoiceRefClass`, 
    `IsContract`, 
    `ContactSource`, 
    `Amount`, 
    `TaxAmount`, 
    `AmountOutstanding`, 
    `CreatedDate`, 
    `ExhumationID`, 
    `Deleted`, 
    `TransactionID`, 
    `CreatedBy`, 
    `DateCreated`, 
    `LastUpdatedBy`, 
    `LastUpdatedDateTime`)
    Select 
        null as InvoiceId,
        'Paid' as Status,
        null as BurialID,
        null as DeedID,
        null as MemorialID,
        null as CremationID,
        null as FuneralDirectorID,
        null as MemorialMasonID,
        1 as SiteID,
        c.facilityid as FacilityID,
        null as InvoiceRefID, 
        co.ContactId as PayerContactID,
        c.ContractNumber as InvoiceRefClass,
        1 as IsContract,
        'Contract Owner' as ContractSource,
        c.TotalSalePrice as Amount,
        null as TaxAmount,
        c.RemainingBalance as AmountOutstanding,
        c.CreatedDate as CreatedDate,
        null as ExhumationID,
        0 as Deleted,
        null as TransactionID,
        null as CreatedBy,
        NOW() as DateCreated,
        null as LastUpdatedBy,
        NOW() as LastUpdatedDateTime
        from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContract c
        join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContractOwner co on co.ContractId = c.ContractID;


        Update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContract C
        join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblInvoice i on i.invoiceID = c.ContractID
        set c.InvoiceId = i.InvoiceID;

        INSERT INTO PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblInvoiceItem 
        (`InvoiceItemID`, 
        `InvoiceID`, 
        `InvoicedModelID`, 
        `FeeID`, 
        `InvoicedModelType`, 
        `Description`, 
        `BurialID`, 
        `CremationID`, 
        `FuneralDirectorID`, 
        `DeedID`, 
        `MemorialMasonID`, 
        `MemorialID`, 
        `ContactID`, 
        `ContactSource`, 
        `EventType`, 
        `Amount`,
        `CreatedDate`)
        select
        null as InvoiceItemID,
        i.invoiceid as invoiceID,
        null as invoicedModelID,
        ci.feeid as FeeID,
        'Everafter\PlotBoxModel\Contract\ContractFee' as InvoicedModelType,
        ci.Description as Description,
        null as BurialID,
        null as CreamtionID,
        null as FuneralDirectorID,
        null as DeedID,
        null as MemorialMasonID,
        null as MemorialID,
        i.PayerContactID as contactid,
        'Contract Owner' as ContactSource,
        null as EventType,
        ci.amount as amount,
        ci.createddate as createddate
        from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblInvoice i 
        join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContract c on i.invoiceID = c.ContractID
        join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContractItems ci on ci.contractID = c.contractID;

INSERT INTO PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPayment (
    `PaymentID`, 
    `Amount`, 
    `AmountOutstanding`, 
    `Reference`, 
    `Notes`, 
    `Type`,
    `ContractPaymentType`, 
    `ContractOneOffPaymentStyle`, 
    `ContractID`, 
    `PaymentDate`, 
    `FuneralDirectorID`, 
    `MemorialMasonID`, 
    `InvoiceID`, 
    `StripeChargeID`, 
    `Reconciled`, 
    `ContactID`, 
    `IsVoid`, 
    `LinkedPaymentID`, 
    `TransactionID`, 
    `CreatedBy`, 
    `DateCreated`, 
    `LastUpdatedBy`, 
    `LastUpdatedDateTime`, 
    `PaymentBatchID`)

 select
    null as PaymentID,
    t.dollar_amount as Amount,
    null as AmountOutstanding,
    t.reference_Number as Reference,
    concat('External Memo: ', ifnull(t.external_memo,''),'\n',
    'Payment Method: ', ifnull(d.specification,'')) as Notes,
    CASE
 WHEN d.specification = 'Amex' THEN 'Card'
 WHEN d.specification = 'Cash' THEN 'Cash'
 WHEN d.specification = 'Check' THEN 'Cheque'
 WHEN d.specification = 'Mastercard' THEN 'Card'
 WHEN d.specification = 'No Payment' THEN ''
 WHEN d.specification = 'Other' THEN ''
 WHEN d.specification = 'Visa' THEN 'Card' END
 as `Type`,
   'One Time' as ContractPaymentType,
    'N/A' as ContractOneOffPaymentStyle,
    c.contractID as ContractID,
   str_to_date(t.date_processed, '%Y-%m-%d') as PaymentDate,
    null as FuneralDirectorID,
    null as MemorialMasonID,
    c.invoiceid as InvoiceID,
    null as StripeChargeID,
    1 as Reconciled,
    co.contactID as ContactID,
    0 as IsVoid,
    null as LinkedPaymentID,
    null as TransactionID,
    null as CreatedBy,
    NOW() as DateCreated,
    null as LastUpdatedBy,
    NOW() as LastUpdatedDateTime,
    null as PaymentBatchID
    from Transactions t      
      JOIN oregonmetro_plotbox.tblContract c on t.sale_id = c.ExtContractIdentifier
      join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContractOwner co on co.ContractId = c.ContractID
      left join descriptors d on t.payment_type_id = d.id
      where t.product_id = '00000000-0000-0000-0000-000000000000';

      INSERT INTO PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblTransaction 
      (`TransactionID`, 
      `TransactionModelID`, 
      `TransactionType`, 
      `ContactID`, 
      `Amount`, 
      `AmountOutstanding`, 
      `CreatedDate`)
    select
    null as TransactionID,
    i.invoiceid as TransactionModelID,
    'Invoice' as TransactionType,
    null as contactID,
    i.amount as Amount,
    0.00 as AmountOutstanding,
    i.createddate as createddate
    from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblInvoice i;

      INSERT INTO PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblTransaction 
      (`TransactionID`, 
      `TransactionModelID`, 
      `TransactionType`, 
      `ContactID`, 
      `Amount`, 
      `AmountOutstanding`, 
      `CreatedDate`)
    select
    null as TransactionID,
    p.paymentid as TransactionModelID,
    'Payment' as TransactionType,
    null as contactID,
    p.amount as Amount,
    0.00 as AmountOutstanding,
    p.datecreated as createddate
    from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblpayment p;

update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPayment p
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblTransaction t on p.PaymentID = t.TransactionModelID
set p.TransactionID = t.TransactionID
where t.TransactionType = 'Payment';

update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblInvoice i
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblTransaction t on i.invoiceID = t.TransactionModelID
set i.TransactionID = t.TransactionID
where t.TransactionType = 'Invoice';

/*
 * Create the transaction allocation amount info
 */
insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblTransactionAllocation(
  TransactionAllocationID,
  InvoiceTransactionID,
  PaymentTransactionID,
  AllocationAmount)
select
  null as TransactionAllocationID,
  i.TransactionID as InvoiceTransactionID,
  p.TransactionID as PaymentTransactionID,
  p.Amount as AllocationAmount
from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPayment p
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblInvoice i on p.InvoiceID = i.InvoiceID;

INSERT INTO PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPaymentSchedule 
(`PaymentScheduleID`, 
`ContractID`, 
`TotalCashPrice`, 
`DownPayment`, 
`DownPaymentDueDate`, 
`AnnualPercentageRate`, 
`InstalmentCalculator`, 
`NumberOfInstallments`, 
`InstallmentAmount`, 
`TotalSalePrice`, 
`FirstPaymentDate`, 
`InstallmentAutoPay`, 
`RemainingInstallments`, 
`TotalPayments`, 
`DateCreated`)
select
null as PaymentScheduleID,
c.contractID as contractID,
c.TotalCashPrice as TotalCashPrice,
c.DownPayment as DownPayment,
c.DownPaymentDueDate as DownPaymentDueDate,
0.00 as AnnualPercentageRate,
'NumberOfInstallments' as InstalmentCalculator,
1 as NumberOfInstallments,
c.TotalCashPrice as InstallmentAmount,
c.TotalSalePrice as TotalSalePrice,
c.FirstPaymentDate as FirstPaymentDate,
0 as InstallmentAutoPay,
0 as RemainingInstallments,
1 as TotalPayments,
c.datecreated as datecreated
from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContract c;

INSERT INTO PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPaymentScheduleDetail 
(`PaymentScheduleDetailID`, 
`PaymentScheduleID`, 
`PaymentType`,
 `DueDate`,  
 `AmountDue`,
 `AmountRemaining`, 
 `CompletedDate`, 
 `Notes`)
select
null as PaymentScheduleDetailID,
ps.PaymentScheduleID,
'Regular' as PaymentType,
ps.FirstPaymentDate as DueDate,
ps.TotalCashPrice as AmountDue,
0.00 as AmountRemaining,
ps.FirstPaymentDate as CompletedDate,
null as Notes
from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPaymentSchedule ps;

INSERT INTO PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPaymentScheduleDetailPayment 
(`PaymentScheduleDetailPaymentID`, 
`PaymentScheduleDetailID`, 
`PaymentID`, 
`Amount`, 
`Deleted`)
select
null as PaymentScheduleDetailPaymentID,
ps.PaymentScheduleDetailID as PaymentScheduleDetailID,
p.paymentid as paymentid,
p.amount as amount,
0 as Deleted
from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPaymentScheduleDetail ps
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPaymentSchedule ps1 on ps.PaymentScheduleID = ps1.PaymentScheduleID
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblcontract c on ps1.contractid = c.contractid
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblpayment p on p.contractid = c.contractid;

update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContract c
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblPaymentSchedule p on p.contractid = c.contractid
set c.paymentscheduleid = p.paymentscheduleid;

/*Remove plots that are just the section name and row*/
delete p.* from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblplot p
join oregonmetro_source.sites s on s.id = p.importplotidentifier
where s.id in
(select parent_id from oregonmetro_source.sites)
and p.plotstatus not like 'Full'
;



RETURN plot_count;

END;
//
DELIMITER ;
