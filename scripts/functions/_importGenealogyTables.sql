DROP FUNCTION IF EXISTS `_importGenealogyTables`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `_importGenealogyTables`() RETURNS int(11)
    DETERMINISTIC
BEGIN

DECLARE genealogy_Count INT default 0;
DECLARE debug VARCHAR(200);

 /*Funeral Directors*/

         Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact (
        `FullName`,
        `AddressLine1`,
        `Deleted`,
        `ExtContactIdentifier`,
        `ExtContactIdentifierName`,
        `Notes`
    )
        select
        f.Name as Name,
        null as AddressLine1,
        1 as Deleted,
        f.ID as ExtContactIdentifier,
        'FUNDIR' as ExtContactIdentifierName,
        concat('Memo: ', ifnull(f.memo,''),'\n',
        'Website: ', ifnull(f.website,'')) as Notes
        from businesses f
       where f.Class_type = 'TFuneralHome';

        /*Businesses*/

         Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact (
        `FullName`,
        `AddressLine1`,
        `Deleted`,
        `ExtContactIdentifier`,
        `ExtContactIdentifierName`,
        `Notes`
    )
        select
        f.Name as Name,
        null as AddressLine1,
        1 as Deleted,
        f.ID as ExtContactIdentifier,
        'Businesses' as ExtContactIdentifierName,
        concat('Memo: ', ifnull(f.memo,''),'\n',
        'Website: ', ifnull(f.website,'')) as Notes
        from businesses f
       where f.Class_type = 'TBusiness';

        Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFuneralDirector (
            `ContactID`,
            `FuneralDirectorName`,
            `AddressLine1`,
            `Active`
        )
            select
            c.ContactID as ContactID,
            c.FullName as FuneralDirectorName,
            c.AddressLine1 as AddressLine1,
            0 as Active
            from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact c
            where c.ExtContactIdentifierName like 'FUNDIR';


/*All contacts from persons table*/
    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact (
        `Title`,
        `Forename`,
        `MiddleName`,
        `Surname`,
        `Fullname`,
        `AddressLine1`,
        `AddressLine2`,
        `Town`,
        `Gender`,
        `County`,
        `Country`,
        `PostCode`,
        `Landline`,
        `EmailAddress`,
        `ExtContactIdentifier`,
        `ExtContactIdentifierName`,
        `CustomerNumber`
    )
    select
        p.Title as Title,
        p.First_Names as Forename,
        p.Middle_Name as MiddleName,
        p.Last_Name as Surname,
        concat(ifnull(p.First_Names, ''),' ', 
	    Case when p.Middle_Name NOT like ''
        THEN concat(p.Middle_Name, ' ')
        ELSE ''
        END,
	    ifnull(p.Last_Name, ''))  as FullName,
        a.Address1 as AddressLine1,
        a.Address2 as AddressLine2,
        a.City as Town,
        IF(p.Gender = '1', 'Female', if(p.Gender = '0', 'Male', 'Unknown')) as Gender,
        a.Province as County,
        a.Country as Country,
        a.Postal_Code as PostCode,
        ph.phone as Landline,
        e.email as EmailAddress,
        p.ID as ExtContactIdentifier,
        'ID' as ExtContactIdentifierName,
        null as CustomerNumber
        from persons p
        left join addresses a on p.id = a.parent_id
        left join emails e on e.parent_id = p.id
        left join phones ph on ph.parent_id = p.id
        group by p.id
        ;

/*Religion*/
   
    Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblReligionDenomination (
        `Name`
    )
    select
    p.Religion as Name
    from persons p
    group by p.religion;

/*Burials*/

     Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy (
        `FacilityID`,
        `ContactID`,
        `PlotID`,
        `GenealogyForename`,
        `GenealogyMiddleNames`,
        `GenealogySurname`,
        `DeceasedDate`,
        `BirthDate`,
        `BurialDate`,
        `BurialTime`,
        `AgeAtDeath`,
        `Sex`,
        `ReligionDenominationID`,
        `Occupation`,
        `PlaceOfDeath`,
        `CauseOfDeath`,
        `FuneralType`,
        `MaritalStatus`,
        `KnownAs`,
        `Veteran`,
        `GenealogyMaidenName`,
        `BurialType`,
        `Notes`,
        `DateCreated`,
        `LastUpdatedDateTime`
    )
    select
     CASE
    WHEN i.Cemetery_ID = '11111111-1111-1111-1111-111111111101' then 1
    WHEN i.Cemetery_ID = '11111111-1111-1111-1111-111111111102' then 2
    WHEN i.Cemetery_ID = '11111111-1111-1111-1111-111111111103' then 3
    WHEN i.Cemetery_ID = '11111111-1111-1111-1111-111111111104' then 4
    WHEN i.Cemetery_ID = '11111111-1111-1111-1111-111111111105' then 5
    WHEN i.Cemetery_ID = '11111111-1111-1111-1111-111111111106' then 6
    WHEN i.Cemetery_ID = '11111111-1111-1111-1111-111111111107' then 7
    WHEN i.Cemetery_ID = '11111111-1111-1111-1111-111111111108' then 8
    WHEN i.Cemetery_ID = '11111111-1111-1111-1111-111111111109' then 9
    WHEN i.Cemetery_ID = '11111111-1111-1111-1111-111111111110' then 10
    WHEN i.Cemetery_ID = '11111111-1111-1111-1111-111111111111' then 11
    WHEN i.Cemetery_ID = '11111111-1111-1111-1111-111111111112' then 12
    WHEN i.Cemetery_ID = '11111111-1111-1111-1111-111111111113' then 13
    WHEN i.Cemetery_ID = '11111111-1111-1111-1111-111111111114' then 14
    ELSE 1
    END as FacilityID,
    c.ContactID as ContactID,
    p.plotid as PlotID,
    c.forename as GenealogyForename,
    c.MiddleName as GenealogyMiddleNames,
    c.surname as GenealogySurname,
    FROM_DAYS(e.date_start - 1721060) as DeceasedDate,
    FROM_DAYS(p.DOB - 1721060) as BirthDate,
    FROM_DAYS(i.date_start - 1721060) as BurialDate,
    null as BurialTime,
    p.age as AgeAtDeath,
    IF(p.Gender = '1', 'Female', if(p.Gender = '0', 'Male', 'Unknown')) as Sex,
    rd.ReligionDenominationID as ReligionDenominationID,
    p.occupation as Occupation,
    concat(ifnull(e.Place_Of_death,''), ' ', ifnull(e.POD_City,''), ' ', ifnull(e.POD_Province,'')) as PlaceOfDeath,
    e.Cause_Of_Death as CauseOfDeath,
    'Burial' as FuneralType,
    null as MaritalStatus,
    p.alias as KnownAs,
    IF(p.Is_Veteran = '-1', 1, 0) as Veteran,
    p.Maiden_Name as GenealogyMaidenName,
    d.specification as BurialType,
    concat('Memo: ', ifnull(p.memo, ''), '\n',
    ifnull(e.memo,''),'\n',
    'Container: ', ifnull(d2.specification,''),'\n',
    'Account Number: ', ifnull(p.account_number,''), '\n',
    'Place Of Birth: ',ifnull(p.place_of_birth,''), '\n',
    '\n',
    group_concat('Next Of Kins: ', concat(ifnull(p2.title,''),' ', ifnull(p2.first_names,''), ' ', ifnull(p2.last_name,'')), '\n',
    'Relationship: ', ifnull(d3.specification,'')SEPARATOR '\n')
    ),
    p.Date_Created as DateCreated,
    p.Date_modified as LastUpdatedDateTime
   from persons p
   join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact c on c.ExtContactIdentifier = p.ID
   join inter i on i.person_id = p.id
   join interment_2_site it on it.interment_id = i.id
   left join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblplot p on it.site_id = p.ImportPlotIdentifier
   left join expire e on e.person_id = p.id
   left join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblreligiondenomination rd on p.religion = rd.name
   left join descriptors d on d.id = it.InSiteAt_ID
   left join descriptors d2 on d2.id = it.container_ID
   left join next_of_kins n on p.id = n.parent_id
   left join persons p2 on p2.id = n.person_id
   left join descriptors d3 on n.relationship_id = d3.id
   group by i.person_id, it.site_id
  ;

 INSERT INTO PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblBurial
 (`BurialID`, 
 `FacilityID`, 
 `FuneralDirectorID`,
`GenealogyID`, 
`DeceasedContactID`,
`ApplicantContactID`,
`PlotID`, 
`BurialDate`,
`IntermentNumber`,
`BurialType`,
`RegistrationDate`,
`RegistrationDistrictID`,
`DeceasedDate`,
`BurialTime`,
`GPCauseofDeath`,
`AgeAtDeath`,
`BurialStatus`
  )
select
null as BurialID,
g.facilityID as facilityID,
f.funeraldirectorid as funeraldirectorid,
g.GenealogyID,
g.ContactID as DeceasedContactID,
null as ApplicantContactID,
g.plotid as plotid,
g.BurialDate as burialdate,
i.reference_number as IntermentNumber,
g.BurialType as BurialType,
g.RegistrationDate as RegistrationDate,
g.RegistrationDistrictID as RegistrationDistrictID,
g.deceaseddate as DeceasedDate,
g.BurialTime as BurialTime,
g.CauseofDeath as GPCauseofDeath,
g.AgeAtDeath as AgeAtDeath,
'Complete' as BurialStatus
from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy g 
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact c on g.contactid = c.ContactID
join persons p on c.ExtContactIdentifier = p.ID
join inter i on i.person_id = p.id
left join FuneralInterService fi on fi.person_id = i.person_id
left join businesses b on b.id = fi.Business_ID
left join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact cfd on cfd.extcontactidentifier = b.id
left join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFuneralDirector f on f.contactid = cfd.contactid
group by g.genealogyid;

Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblEvent (
            `DiaryID`,
            `FacilityID`,
            `SiteID`,
            `EventModelID`,
            `EventName`,
            `EventType`,
            `EventDate`,
            `EventStatus`
        )
        select
            1 as DiaryID,
            b.facilityid as FacilityID,
            1 as SiteID,
            b.burialID as EventModelID,
            'Burial' as EventName,
            'Burial' as EventType,
            b.burialdate as EventDate,
            'Complete' as EventStatus
            from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblBurial b;

update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblBurial b
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact c on b.DeceasedContactID = c.ContactID
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblEvent e on e.EventModelID = b.burialID
set b.eventid = e.eventid
where e.EventType = 'Burial';

/*Cremations*/
Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy (
        `FacilityID`,
        `ContactID`,
        `PlotID`,
        `GenealogyForename`,
        `GenealogyMiddleNames`,
        `GenealogySurname`,
        `DeceasedDate`,
        `BirthDate`,
        `BurialDate`,
        `BurialTime`,
        `AgeAtDeath`,
        `Sex`,
        `ReligionDenominationID`,
        `Occupation`,
        `PlaceOfDeath`,
        `CauseOfDeath`,
        `FuneralType`,
        `MaritalStatus`,
        `KnownAs`,
        `Veteran`,
        `GenealogyMaidenName`,
        `Notes`
    )
    select
     CASE
    WHEN cr.Cemetery_ID = '11111111-1111-1111-1111-111111111101' then 1
    WHEN cr.Cemetery_ID = '11111111-1111-1111-1111-111111111102' then 2
    WHEN cr.Cemetery_ID = '11111111-1111-1111-1111-111111111103' then 3
    WHEN cr.Cemetery_ID = '11111111-1111-1111-1111-111111111104' then 4
    WHEN cr.Cemetery_ID = '11111111-1111-1111-1111-111111111105' then 5
    WHEN cr.Cemetery_ID = '11111111-1111-1111-1111-111111111106' then 6
    WHEN cr.Cemetery_ID = '11111111-1111-1111-1111-111111111107' then 7
    WHEN cr.Cemetery_ID = '11111111-1111-1111-1111-111111111108' then 8
    WHEN cr.Cemetery_ID = '11111111-1111-1111-1111-111111111109' then 9
    WHEN cr.Cemetery_ID = '11111111-1111-1111-1111-111111111110' then 10
    WHEN cr.Cemetery_ID = '11111111-1111-1111-1111-111111111111' then 11
    WHEN cr.Cemetery_ID = '11111111-1111-1111-1111-111111111112' then 12
    WHEN cr.Cemetery_ID = '11111111-1111-1111-1111-111111111113' then 13
    WHEN cr.Cemetery_ID = '11111111-1111-1111-1111-111111111114' then 14
    ELSE 1
    END as FacilityID,
    c.ContactID as ContactID,
    null as PlotID,
    c.forename as GenealogyForename,
    c.MiddleName as GenealogyMiddleNames,
    c.surname as GenealogySurname,
    FROM_DAYS(e.date_start - 1721060) as DeceasedDate,
    FROM_DAYS(p.DOB - 1721060) as BirthDate,
    null as BurialDate,
    null as BurialTime,
    p.age as AgeAtDeath,
    IF(p.Gender = '1', 'Female', if(p.Gender = '0', 'Male', 'Unknown')) as Sex,
    null as ReligionDenominationID,
    p.occupation as Occupation,
    concat(ifnull(e.Place_Of_death,''), ' ', ifnull(e.POD_City,''), ' ', ifnull(e.POD_Province,'')) as PlaceOfDeath,
    e.Cause_Of_Death as CauseOfDeath,
    'Cremation' as FuneralType,
    null as MaritalStatus,
    p.alias as KnownAs,
    IF(p.Is_Veteran = '-1', 1, 0) as Veteran,
    p.Maiden_Name as GenealogyMaidenName,
    concat('Memo: ', ifnull(p.memo, ''), '\n',
    'Account Number: ', ifnull(p.account_number,''), '\n',
    'Place Of Birth: ',ifnull(p.place_of_birth,''), '\n',
    'Tag Number: ', ifnull(cr.tag_number,''))
   from persons p
   join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact c on c.ExtContactIdentifier = p.ID
   join cremate cr on cr.person_id = p.id
   left join expire e on e.person_id = p.id;

   insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblCremation (
        `CremationNumber`,
        `FacilityID`,
        `CremationDate`,
        `FuneralDirectorId`,
        `FuneralDirectorContactID`,
        `GenealogyID`,
        `PlotPurchaseID`,
        `PayerContactID`,
        `AgeAtDeath`,
        `PlaceOfDeath`,
        `DeceasedDate`,
        `ReligionDenominationID`,
        `MaritalStatus`,
        `Location`,
        `CremationType`,
        `KnownAs`,
       `CremationStatus`,
        `Source`,
        `SiteID`,
        `DeceasedContactID`,
        `CremationNotes`
    )
    select
        cr.reference_number as CremationNumber,
        g.facilityid as FacilityID,
        FROM_DAYS(cr.date_start - 1721060) as CremationDate,
        null as FuneralDirectorId,
        null as FuneralDirectorContactID,
        g.GenealogyID as GenealogyID,
        null as PlotPurchaseID,
        null as PayerContactID,
        g.AgeAtDeath as AgeAtDeath,
        g.PlaceOfDeath as PlaceOfDeath,
        g.DeceasedDate as DeceasedDate,
        null as ReligionDenominationID,
        g.MaritalStatus as MaritalStatus,
        null as Location,
        'Standard' as CremationType,
        p.alias as KnownAs,
      'Complete' as CremationStatus,
        'Import' as Source,
        1 as SiteID,
        g.ContactID as DeceasedContactID,
       null as CremationNotes
        from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy g
        join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact c on c.ContactID = g.ContactID
        join persons p on c.ExtContactIdentifier = p.ID
        join cremate cr on cr.person_id = p.id
        where g.FuneralType = 'Cremation';

Insert into PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblEvent (
            `DiaryID`,
            `FacilityID`,
            `SiteID`,
            `EventModelID`,
            `EventName`,
            `EventType`,
            `EventDate`,
            `EventStatus`
        )
        select
            1 as DiaryID,
            c.facilityid as FacilityID,
            1 as SiteID,
            c.cremationID as EventModelID,
            'Cremation' as EventName,
            'Cremation' as EventType,
            c.cremationdate as EventDate,
            'Complete' as EventStatus
            from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblcremation c;

update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblCremation c
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact co on c.DeceasedContactID = co.ContactID
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblEvent e on e.EventModelID = c.cremationID
set c.eventid = e.eventid
where e.EventType = 'Cremation';

update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblContact c
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy g on g.contactid = c.contactid
set c.Deceased = 1;

update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy g
set g.deceaseddate = '0000-00-00' where g.deceaseddate = '9999-12-31';

update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy g
set g.BurialDate = '0000-00-00' where g.BurialDate = '9999-12-31';

update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy g
set g.BirthDate = '0000-00-00' where g.BirthDate = '9999-12-31';

update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblBurial b
set b.BurialDate = '0000-00-00' where b.BurialDate = '9999-12-31';

update PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblCremation c
set c.CremationDate = '0000-00-00' where c.CremationDate = '9999-12-31';

select count(*) into genealogy_Count from PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblGenealogy;

RETURN genealogy_Count;

END;
//
DELIMITER ;
