DROP FUNCTION IF EXISTS `_setupUniquePlots`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `_setupUniquePlots`() RETURNS int(11)
    DETERMINISTIC
BEGIN

DECLARE cursor_SECTION VARCHAR(50);
DECLARE cursor_LOT_NO VARCHAR(50);
DECLARE cursor_DIV_E VARCHAR(50);
DECLARE cursor_DIV_W VARCHAR(200);
DECLARE cursor_NO_GRAVES int;
DECLARE cursor_ST_GRAV_NO int;

DECLARE i int;
DECLARE plots_purchased int;
DECLARE currentPlotNumber int;
DECLARE tmp_plotNumber VARCHAR(50);
DECLARE tmp_locationId VARCHAR(200);
DECLARE tmp_numberAppend VARCHAR(2);
DECLARE total_unique_plots int default 0;

DECLARE done INT DEFAULT FALSE;
DECLARE owner_cursor CURSOR FOR SELECT SECTION, LOT_NO, DIV_E, DIV_W, NO_GRAVES, ST_GRAV_NO FROM main_owners WHERE ST_GRAV_NO IS NOT NULL;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

/* add unique burial plots */
insert into all_plots(PlotSection, PlotRow, PlotNumber, LocationID)
select distinct PlotSection, PlotRow, PlotNumber, LocationID from main_burial;

/* process the owners info */
OPEN owner_cursor;
read_loop: LOOP
  FETCH owner_cursor INTO cursor_SECTION, cursor_LOT_NO, cursor_DIV_E, cursor_DIV_W, cursor_NO_GRAVES, cursor_ST_GRAV_NO;
  IF done THEN
    LEAVE read_loop;
  END IF;
  SET tmp_plotNumber = cursor_ST_GRAV_NO;
  IF cursor_DIV_E = 'Y' THEN
    SET tmp_numberAppend = 'E';
  ELSEIF cursor_DIV_W = 'Y' THEN
    SET tmp_numberAppend = 'W';
  ELSE
    SET tmp_numberAppend = '';
  END IF;
  SET tmp_plotNumber = concat(tmp_plotNumber, tmp_numberAppend);
  SET tmp_locationId = concat('[1][', cursor_SECTION, '][', cursor_LOT_NO, '][', tmp_plotNumber, ']');
  INSERT INTO all_plots(PlotSection, PlotRow, PlotNumber, LocationID) VALUES(cursor_SECTION, cursor_LOT_NO, tmp_plotNumber, tmp_locationId);

  SET plots_purchased = cursor_NO_GRAVES;
  IF (plots_purchased > 1 and cursor_ST_GRAV_NO is not null) THEN
    SET i = 1;
    SET currentPlotNumber = cursor_ST_GRAV_NO + 1; /* first one already done above */
    WHILE i<plots_purchased DO
      SET tmp_plotNumber = concat(currentPlotNumber, tmp_numberAppend);
      SET tmp_locationId = concat('[1][', cursor_SECTION, '][', cursor_LOT_NO, '][', tmp_plotNumber, ']');
      INSERT INTO all_plots(PlotSection, PlotRow, PlotNumber, LocationID) VALUES(cursor_SECTION, cursor_LOT_NO, tmp_plotNumber, tmp_locationId);
      SET i = i + 1;
      SET currentPlotNumber = currentPlotNumber + 1;
    END WHILE;
  END IF;
END LOOP;
CLOSE owner_cursor;

/* now create the unique plots */
delete from unique_plots;
INSERT INTO unique_plots(PlotSection, PlotRow, PlotNumber, LocationID)
select distinct PlotSection, PlotRow, PlotNumber, LocationID from all_plots where LocationID is not null;
update unique_plots up
join PLOTBOX_SCHEMA_DB_PLACEHOLDER.tblFacilitySection fs on up.PlotSection = fs.SectionName set up.SectionExistsInPlotBox = 1;
update unique_plots set RowLocationID = concat('[1][', PlotSection, '][', PlotRow, ']');
update unique_plots set DivAppend = 'W' where PlotNumber like '%W';
update unique_plots set DivAppend = 'E' where PlotNumber like '%E';  

select count(*) into total_unique_plots from unique_plots;
return total_unique_plots;

END;
//
DELIMITER ;
