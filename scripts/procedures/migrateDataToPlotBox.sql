DROP PROCEDURE IF EXISTS `migrateOrangeToPlotBox`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `migrateOrangeToPlotBox`()
    DETERMINISTIC
BEGIN

declare plot_count INT default 0;
declare genealogy_count INT default 0;
declare fee_count INT default 0;
declare contract_count INT default 0;
declare payment_count INT default 0;
declare last_return INT default 0;
declare contract_item_count INT default 0;
declare customer_count INT default 0;
declare deed_count INT default 0;
declare debugOut VARCHAR(250) default '';
declare update_status INT default 0;
declare risk_count INT default 0;

/*
 *  Import the base configs
 */
set debugOut = _debug("Calling _importBaseConfiguration()");
set plot_count = _importBaseConfiguration();
set debugOut = _debug("Returned from _importBaseConfiguration()");


/*
 *Import all plots
*/
set debugOut = _debug("Calling _importPlotTables()");
set plot_count = _importPlotTables();
set debugOut = _debug("Returned from _importPlotTables()");

 /* Import all genealogy records
 */
set debugOut = _debug("Calling _importGenealogyTables()");
set genealogy_count = _importGenealogyTables();
set debugOut = _debug("Returned from _importGenealogyTables()");



/*
 *Import all memorials

set debugOut = _debug("Calling _importMemorialTables()");
set plot_count = _importMemorialTables();
set debugOut = _debug("Returned from _importMemorialTables()");
*/


 /*Import Deeds
*/
set debugOut = _debug("Calling _importDeedTables()");
set deed_count = _importDeedTables();
set debugOut = _debug("Returned from _importDeedTables()");

 /*Import Contracts
*/ 
set debugOut = _debug("Calling _importContractTables()");
set deed_count = _importContractTables();
set debugOut = _debug("Returned from _importContactTables()");





set update_status = _updatePlotStatusAndCapacities();

END;
//
DELIMITER ;
