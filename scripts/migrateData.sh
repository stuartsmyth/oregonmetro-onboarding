#!/bin/bash
BASE_SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
BASE_DIR=${BASE_SCRIPT_DIR}/..
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

CUSTOMER_DOMAIN=$1
if [ -z "$1" ];
  then
	echo -e "${RED}Domain name not provdied - please try again providing the domain name!.${NC}"
	exit;
fi

#
#	Database Configuration
#
DB_USER=root
DB_PASSWORD=
DB_HOST=localhost
DB_CONNECTION_STRING=mysql+mysqldb://${DB_USER}:${DB_PASSWORD}@${DB_HOST}


IMPORT_DB=${CUSTOMER_DOMAIN}_source
PLOTBOX_DB=${CUSTOMER_DOMAIN}_plotbox
IMPORT_DATA_DIR=${BASE_DIR}/data
IMPORT_DATA_SOURCE_DIR=${IMPORT_DATA_DIR}/source
IMPORT_FILE_PATH=${BASE_DIR}/data/csvs
SEED_DIR=${BASE_DIR}/data/seed
SEED_FILE=${SEED_DIR}/${CUSTOMER_DOMAIN}Seed.sql
IMPORT_DB_FILE=${BASE_DIR}/output/importDB_${CUSTOMER_DOMAIN}.sql
PLOTBOX_DB_FILE=${BASE_DIR}/output/plotboxDB_${CUSTOMER_DOMAIN}.sql
IMPORT_BEFORE_ALTER_FILE=${BASE_DIR}/output/importDb_BeforeAlter_${CUSTOMER_DOMAIN}.sql
MIGRATED_PLOTBOX_DB_FILE=${BASE_DIR}/output/plotboxDb_afterMigrate_${CUSTOMER_DOMAIN}.sql
#added



echo -e "${GREEN}IMPORT_DB=${IMPORT_DB}${NC}"
echo -e "${GREEN}PLOTBOX_DB=${PLOTBOX_DB}${NC}"

function setupMySQL() {
	compatibilityModeSQL="set @@global.show_compatibility_56=ON;"
	echo -e "${GREEN}Setting up compatibility mode [${compatibilityModeSQL}]${NC}"
  	bash -c "mysql -u ${DB_USER} -e '${compatibilityModeSQL}'"
  	echo -e "${GREEN}Compatibility mode set up.${NC}"
  	engineSQL="SET @@global.default_storage_engine=MyISAM;"
	echo -e "${GREEN}Setting up mysql engine as  [${engineSQL}]${NC}"
  	bash -c "mysql -u ${DB_USER} -e '${engineSQL}'"
  	echo -e "${GREEN}MySQL Engine set up.${NC}"
}

function startMySQL() {
	echo -e "${GREEN}Starting MySQL locally....${NC}"
	bash -c "mysql.server start"

	setupMySQL
}

function createDB() {
	createDb="drop database if exists ${1}; create database ${1} CHARACTER SET utf8 COLLATE utf8_general_ci;"
	echo -e "${GREEN}Database creation string is [${createDb}]${NC}"
  	bash -c "mysql -u ${DB_USER} -e '${createDb}'"
  	echo -e "${GREEN}[${1}] database created.${NC}"
}

function importCSV() {
	importDb=$1
	importFile=$2
	importMode=$3
	echo -e "${GREEN}Importing Data from source csv file ${importFile}${NC}"
	importString="time csvsql -v --db ${DB_CONNECTION_STRING}/${importDb} ${importMode} ${importFile}"
	echo -e "${GREEN}Import string = [${importString}]${NC}"
	bash -c "${importString}"
	echo -e "${GREEN}Importing Data from source csv file completed${NC}"
}

function importPipedTable() {
	importDb=$1
	importFile=$2
	importMode=$3
	echo -e "${GREEN}Importing Data from source text file ${importFile}${NC}"
	importString="time csvsql --db ${DB_CONNECTION_STRING}/${importDb} ${importMode} -d \| ${importFile}"
	echo -e "${GREEN}Import string = [${importString}]${NC}"
	bash -c "${importString}"
	echo -e "${GREEN}Importing Data from source text file completed${NC}"
}

function importSourceData() {
	importDb=$1

	if [ -f ${IMPORT_BEFORE_ALTER_FILE} ]
	then
		echo "Using the existing ${IMPORT_BEFORE_ALTER_FILE} file rather than reloading"
		importSQLFile ${IMPORT_DB} ${IMPORT_BEFORE_ALTER_FILE}
	else
		echo -e "${GREEN}Importing Data from source text files to Database${NC}"
		echo -e "${GREEN}Importing Data from source text files to Database${NC}"

		importSQLFile ${importDb} ${IMPORT_FILE_PATH}/CDMAPP.sql

		echo -e "${GREEN}Importing Data from source text files to Database is now complete.${NC}"
		echo -e "${GREEN}Backing up ${importDb} to ${IMPORT_BEFORE_ALTER_FILE}.${NC}"
		bash -c "time mysqldump -u ${DB_USER} -e ${importDb} > ${IMPORT_BEFORE_ALTER_FILE}"
	
	for importFile in ${BASE_DIR}/data/csvs/*.csv; do

		importDb=${IMPORT_DB}
		importMode='--insert'
		echo -e "${GREEN}Importing Data from source csv file ${importFile}${NC}"
		importString="time csvsql -v --db ${DB_CONNECTION_STRING}/${importDb} ${importMode} ${importFile}"
		echo -e "${GREEN}Import string = [${importString}]${NC}"
		bash -c "${importString}"
		echo -e "${GREEN}Importing Data from source csv file completed${NC}"
	done

	
		echo -e "${GREEN}Importing Data from source text files to Database is now complete.${NC}"
		echo -e "${GREEN}Backing up ${importDb} to ${IMPORT_BEFORE_ALTER_FILE}.${NC}"
		bash -c "time mysqldump -u ${DB_USER} -e ${importDb} > ${IMPORT_BEFORE_ALTER_FILE}"
	fi

}

function importSQLFile() {
	importDatabase=$1
	fileName=$2
	echo -e "${GREEN}Running import [${fileName}] to MySQL${NC}"
	importStatement="time mysql -u ${DB_USER} ${importDatabase} < "
	bash -c "${importStatement}${fileName}"
	echo -e "${GREEN}Import complete.${NC}"
}

function installFunctions() {
	importDb=$1
	plotboxDb=$2
	echo -e "${GREEN}Replacing holding variables for functions functions.${NC}"
	echo -e "${GREEN}Installing functions.${NC}"
	bash -c "rm -rf ${BASE_SCRIPT_DIR}/functions/tmp"
	for f in ${BASE_SCRIPT_DIR}/functions/*.sql; do
		dir=$(dirname "${f}")
		filename=$(basename "${f}")
		tmpdir="${dir}/tmp"
		tmpFile="${tmpdir}/${filename}"
		bash -c "mkdir -p ${tmpdir}"
		findReplaceCommand="sed 's/PLOTBOX_SCHEMA_DB_PLACEHOLDER/${plotboxDb}/g; s/CEM_DOMAIN_PLACEHOLDER/${CUSTOMER_DOMAIN}/g' ${f} > ${tmpFile}"
		echo -e "${GREEN}Running find and replace on function placeholders [${findReplaceCommand}].${NC}"
		bash -c "${findReplaceCommand}"
		importSQLFile ${importDb} ${tmpFile}
	done
	echo -e "${GREEN}Functions installed.${NC}"
}

function installStoredProcedures() {
	importDb=$1
	echo -e "${GREEN}Installing procedures.${NC}"
	bash -c "rm -rf ${BASE_SCRIPT_DIR}/procedures/tmp"
	for f in ${BASE_SCRIPT_DIR}/procedures/*.sql; do importSQLFile ${importDb} ${f}; done
	echo -e "${GREEN}Functions procedures.${NC}"
}

function createIndexes() {
	echo -e "${GREEN}Creating Indexes${NC}"
}

function alterSchemas() {
	importDb=$1
	plotboxDb=$2
	bash -c "mkdir -p ${BASE_SCRIPT_DIR}/indexes/tmp"
	alterDBsSQLFile="${BASE_SCRIPT_DIR}/indexes/alterDBs.sql"
	tmpAlterDBsSQLFile="${BASE_SCRIPT_DIR}/indexes/tmp/alterDBs.sql"
	findReplaceCommand="sed 's/PLOTBOX_SCHEMA_DB_PLACEHOLDER/${plotboxDb}/g' ${alterDBsSQLFile} > ${tmpAlterDBsSQLFile}"
	echo -e "${GREEN}Running find and replace on function placeholders [${findReplaceCommand}].${NC}"
	bash -c "${findReplaceCommand}"
	tmpFinalAlterImportSQLFile=${tmpAlterDBsSQLFile}
	importSQLFile ${importDb} ${tmpFinalAlterImportSQLFile}
}

function migrateData() {
	importDb=$1
	plotboxDb=$2
	echo -e "${GREEN}Will be migrating from [${importDb}] to [${plotboxDb}]${NC}"
	echo -e "${GREEN}Creating PlotBox database to migrate to [${plotboxDb}]${NC}"
	createDB ${plotboxDb}
	if [ -f ${SEED_FILE} ]
	then
		echo -e "${GREEN}Seeding PlotBox database to migrate to [${plotboxDb}] using [${SEED_FILE}]${NC}"
		importSQLFile ${plotboxDb} ${SEED_FILE}
	else
		echo -e "${GREEN}No domain specific seed file so seeding PlotBox database to migrate to [${plotboxDb}] using [${SEED_DIR}/plotboxSeed.sql]${NC}"
		importSQLFile ${plotboxDb} ${SEED_DIR}/plotboxSeed.sql
	fi
	echo -e "${GREEN}Beginning alterations on import and plotbox database [${SEED_FILE}]${NC}"
	alterSchemas ${importDb} ${plotboxDb}
	echo -e "${GREEN}Backing up ${importDb} to ${IMPORT_DB_FILE}.${NC}"
	bash -c "time mysqldump -u ${DB_USER} -e ${importDb} > ${IMPORT_DB_FILE}"
	echo -e "${GREEN}Backing up ${plotboxDb} to ${PLOTBOX_DB_FILE}.${NC}"
	bash -c "time mysqldump -u ${DB_USER} -e ${plotboxDb} > ${PLOTBOX_DB_FILE}"
	callProcSQL="call migrateOrangeToPlotBox()"
	callProcCmd="time mysql -u ${DB_USER} -e '${callProcSQL}' ${importDb}"
	echo -e "${GREEN}[${importDb}] [${plotboxDb}]${NC}"
	echo -e "${GREEN}Calling migrate stored procedure [${callProcCmd}]${NC}"
	bash -c "${callProcCmd}"
	echo -e "${GREEN}Migrate stored procedure complete.${NC}"
	echo -e "${GREEN}Producing final import file for ${plotboxDb} to ${MIGRATED_PLOTBOX_DB_FILE}.${NC}"
	bash -c "time mysqldump -u ${DB_USER} -e ${plotboxDb} > ${MIGRATED_PLOTBOX_DB_FILE}"
	terminal-notifier -message "Import Complete" -title "Import Status"
}

function listDebug() {
	echo -e "${GREEN}Stored procedure output${NC}"
}

if [ -f ${IMPORT_BEFORE_ALTER_FILE} ]
then
	echo "An existing source seed MySQL database exists. Do you wish to use it? (Selecting no will enfore a full import loading data from source data files)"
	select yn in "Use_existing_Import_database" "Reload_data_from_source_files"; do
			case $yn in
					Use_existing_Import_database ) break;;
					Reload_data_from_source_files ) bash -c "rm -f ${IMPORT_BEFORE_ALTER_FILE}"; break;;
			esac
	done
fi

startMySQL

# 1. Create the databases
createDB ${IMPORT_DB}

# 2. Import the source data into the import database
importSourceData ${IMPORT_DB} ${PLOTBOX_DB}

# 3. Import the functions and stored procedures
installFunctions ${IMPORT_DB} ${PLOTBOX_DB}
installStoredProcedures ${IMPORT_DB} ${PLOTBOX_DB}

# 4. Create required indexes
createIndexes ${IMPORT_DB} ${PLOTBOX_DB}

# 5. Run import process
migrateData ${IMPORT_DB} ${PLOTBOX_DB}

# 6. List Debug Output
listDebug ${IMPORT_DB} ${PLOTBOX_DB}

# 7. Tidy up

return