# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.37-82.2)
# Database: Blank_Seed_File
# Generation Time: 2018-10-22 14:13:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tblApplicationSequence
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblApplicationSequence`;

CREATE TABLE `tblApplicationSequence` (
  `ApplicationSequenceID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `Name` varchar(50) NOT NULL DEFAULT '',
  `CurrentValue` int(11) DEFAULT NULL,
  PRIMARY KEY (`ApplicationSequenceID`),
  KEY `FacilityID` (`FacilityID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblApplicationSequence` WRITE;
/*!40000 ALTER TABLE `tblApplicationSequence` DISABLE KEYS */;

INSERT INTO `tblApplicationSequence` (`ApplicationSequenceID`, `FacilityID`, `Name`, `CurrentValue`)
VALUES
	(1,1,'seq_cremation_number',0),
	(2,1,'seq_interment_number',0),
	(3,0,'seq_customer_number',6),
	(4,0,'seq_work_order_number',0);

/*!40000 ALTER TABLE `tblApplicationSequence` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblArchiveReport
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblArchiveReport`;

CREATE TABLE `tblArchiveReport` (
  `ArchiveReportID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ArchiveReportName` varchar(255) NOT NULL DEFAULT '',
  `UserID` int(11) unsigned NOT NULL DEFAULT '0',
  `ArchiveChartTopRange` varchar(255) NOT NULL DEFAULT '',
  `ArchiveReportChartData` longtext NOT NULL,
  `ArchiveChartTableHtml` longtext NOT NULL,
  `ArchiveChartTotalsHtml` longtext NOT NULL,
  `ArchiveChartComparisonsHtml` longtext NOT NULL,
  `ArchiveChartFiltersUsedHtml` longtext NOT NULL,
  `ArchiveChartDateRangeHtml` longtext NOT NULL,
  `ArchiveReportType` enum('Deceased Records Report','Grave Sales Report','Sales Report','') NOT NULL DEFAULT '',
  PRIMARY KEY (`ArchiveReportID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblAuthenticationPermission
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblAuthenticationPermission`;

CREATE TABLE `tblAuthenticationPermission` (
  `PermissionID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PermissionName` varchar(250) NOT NULL DEFAULT '',
  `PermissionPath` varchar(250) NOT NULL DEFAULT '',
  `ParentPermissionID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`PermissionID`),
  KEY `ParentPermissionID` (`ParentPermissionID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tblAuthenticationPermission` WRITE;
/*!40000 ALTER TABLE `tblAuthenticationPermission` DISABLE KEYS */;

INSERT INTO `tblAuthenticationPermission` (`PermissionID`, `PermissionName`, `PermissionPath`, `ParentPermissionID`)
VALUES
	(3178,'Chart of Accounts','Configuration/ChartOfAccounts',0),
	(3179,'View Deleted Contracts','Contracts/ViewDeletedContracts',0),
	(3177,'Books','Books',0),
	(3176,'CRM','CRM',0),
	(3175,'Edit Contacts','Contacts/Edit',0),
	(3174,'Contacts','Contacts',0),
	(3173,'Apply Credit','Contracts/ApplyCredit',0),
	(3172,'Approve Contracts','Contracts/Approve',0),
	(3171,'Set Contract APR','Contracts/Apr',0),
	(3170,'Delete Contracts','Contracts/Delete',0),
	(3169,'Edit Approved Contracts','Contracts/EditApproved',0),
	(3168,'Edit Contracts','Contracts/Edit',0),
	(3167,'Add Contracts','Contracts/Add',0),
	(3166,'Contracts','Contracts',0),
	(3165,'Dashboard','Dashboard',0),
	(3164,'Interment Report','Reports/Internment',0),
	(3163,'Reports','Reports',0),
	(3162,'Delete Records','Records/Delete',0),
	(3161,'Edit Records','Records/Edit',0),
	(3160,'Add Records','Records/Add',0),
	(3159,'Records','Records',0),
	(3158,'Release Plots','Plots/Release',0),
	(3157,'Hold Plots','Plots/Hold',0),
	(3156,'Delete Plots','Plots/Delete',0),
	(3154,'Plots','Plots',0),
	(3155,'Edit Plots','Plots/Edit',0),
	(3153,'Edit Cancelled Contracts','Contracts/CancelledContract/Edit',0),
	(3152,'Add Cancelled Contracts','Contracts/CancelledContract/Add',0),
	(3150,'Unpaid Fees','Finance/UnpaidFees',0),
	(3151,'Contract Payments','Finance/ContractPayments',0),
	(3149,'Payments','Finance/Payments',0),
	(3148,'Invoice Items','Finance/InvoiceItems',0),
	(3147,'Invoices','Finance/Invoices',0),
	(3146,'Making Refunds','Finance/Refunds',0),
	(3145,'Making Adjustments','Finance/Adjustments',0),
	(3144,'Voiding Payments','Finance/VoidingPayments',0),
	(3143,'Finance','Finance',0),
	(3142,'Complete Events','Events/Complete',0),
	(3141,'Cancel Events','Events/Cancel',0),
	(3140,'Edit Events','Events/Edit',0),
	(3139,'Events','Events',0),
	(3138,'Complete Work Permits','Diary/WorkPermits/Complete',0),
	(3136,'Add Work Permits','Diary/WorkPermits/Add',0),
	(3137,'Edit Work Permits','Diary/WorkPermits/Edit',0),
	(3135,'Work Permits','Diary/WorkPermits',0),
	(3134,'Edit Exhumations','Diary/Exhumations/Edit',0),
	(3133,'Exhumations','Diary/Exhumations',0),
	(3132,'Cremations','Diary/Cremations',0),
	(3131,'Burial Orders','Diary/Burials/BurialOrder',0),
	(3130,'Burials','Diary/Burials',0),
	(3129,'Appointment','Diary/Appointments',0),
	(3128,'Diary','Diary',0),
	(3127,'Edit Memorials','Memorials/Edit',0),
	(3126,'Add Memorials','Memorials/Add',0),
	(3125,'Memorials','Memorials',0),
	(3124,'Delete Deeds','Deeds/Delete',0),
	(3122,'Add Deeds','Deeds/Add',0),
	(3123,'Edit Deeds','Deeds/Edit',0),
	(3121,'Deeds','Deeds',0),
	(3120,'Religion Denomination Settings','Configuration/ReligionDenomination',0),
	(3119,'Tax Rate Settings','Configuration/TaxRates',0),
	(3118,'Language Settings','Configuration/LanguageSettings',0),
	(3117,'System Settings','Configuration/SystemSettings',0),
	(3116,'Email Templates','Configuration/EmailTemplate',0),
	(3114,'Role Configuration','Configuration/Roles',0),
	(3115,'Edit Roles','Configuration/Roles/Edit',0),
	(3113,'User Configuration','Configuration/Users',0),
	(3112,'Site Configuration','Configuration/Sites',0),
	(3106,'Mason Configuration','Configuration/MemorialMasons',0),
	(3107,'Add Masons','Configuration/MemorialMasons/Add',0),
	(3108,'Edit Masons','Configuration/MemorialMasons/Edit',0),
	(3109,'Mason Registration Permits','Configuration/MemorialMasons/RegistrationPermits',0),
	(3110,'Add Mason Registration Permits','Configuration/MemorialMasons/RegistrationPermits/Add',0),
	(3111,'Edit Mason Registration Permits','Configuration/MemorialMasons/RegistrationPermits/Edit',0),
	(3104,'Edit Funeral Directors','Configuration/FuneralDirectors/Edit',0),
	(3105,'Medical Contact Configuration','Configuration/MedicalContacts',0),
	(3103,'Add Funeral Directors','Configuration/FuneralDirectors/Add',0),
	(3102,'Funeral Director Configuration','Configuration/FuneralDirectors',0),
	(3101,'Fee Configuration','Configuration/Fees',0),
	(3100,'Section Configuration','Configuration/Sections',0),
	(3099,'Facility Configuration','Configuration/Facilities',0),
	(3098,'Template Configuration','Configuration/DocumentTemplates',0),
	(3097,'Division Configuration','Configuration/Divisions',0),
	(3096,'Diary Configuration','Configuration/Diaries',0),
	(3095,'Configuration','Configuration',0),
	(3180,'Payment Schedules','Finance/PaymentSchedules',0),
	(3181,'Auto Payments','Finance/AutoPayment',0),
	(3182,'Payment Batches','Finance/PaymentBatches',0),
	(3183,'Add Auto Payment Batch','Finance/AutoPayment/AddBatch',0),
	(3184,'Edit Auto Payment','Finance/AutoPayment/EditPayment',0),
	(3185,'Delete Auto Payment','Finance/AutoPayment/DeletePayment',0),
	(3186,'Permanent Hold','Plots/Hold/PermanentHold',0),
	(3187,'Release Permanent Hold','Plots/Release/ReleasePermanentHold',0),
	(3188,'Add Plots From Contract Summary','Contracts/AddPlotsFromContractSummary',0),
	(3189,'Remove Plots From Contract Summary','Contracts/RemovePlotsFromContractSummary',0),
	(3190,'Remove Beneficiaries','Contracts/RemoveBeneficiaries',0),
	(3191,'User','User',0),
	(3192,'Team Management','User/TeamManagement',0),
	(3193,'Delete Memorials','Memorials/Delete',0),
	(3194,'Merge Contacts','Contacts/MergeContacts',0),
	(3195,'CRM Settings','CRM/CRMSettings',0),
	(3196,'CRM Lead Managenent','CRM/LeadManagenent',0),
	(3197,'CRM Team View','CRM/TeamView',0),
	(3198,'Import','Import',0),
	(3199,'Import CRM','Import/CRM',0),
	(3200,'Work','Work',0),
	(3201,'Work Orders','Work/WorkOrders',0),
	(3202,'Add Work Orders','Work/WorkOrders/Add',0),
	(3203,'Edit Work Orders','Work/WorkOrders/Edit',0),
	(3204,'Complete Work Orders','Work/WorkOrders/Complete',0),
	(3205,'Maps','Maps',0),
	(3206,'Edit Maps','Maps/Edit',0),
	(3207,'Stock Manual Override','Configuration/Fees/StockOverride',0),
	(3208,'Integrating Systems Configuration','Configuration/Integration',0),
	(3209,'Reassign Cremation Numbers ','Diary/ReassignCremationNumbers',0),
	(3210,'Reassign Interment Numbers ','Diary/ReassignIntermentNumbers',0),
	(3211,'ArcGIS','Maps/ArcGIS',0),
	(3212,'Cancel Contracts','Contracts/CancelledContract',0),
	(3213,'Reverse Cancelled Contracts','Contracts/CancelledContract/Reverse',0),
	(3214,'Add Plots To Approved Contract Summary','Contracts/AddPlotsToApprovedContractSummary',0),
	(3215,'Remove Plots From Approved Contract Summary','Contracts/RemovePlotsFromApprovedContractSummary',0),
	(3216,'Add Beneficiaries','Contracts/AddBeneficiaries',0),
	(3217,'Add Beneficiaries To Approved Contract','Contracts/AddBeneficiariesToApprovedContract',0),
	(3218,'Remove Beneficiaries From Approved Contract','Contracts/RemoveBeneficiariesFromApprovedContract',0),
	(3219,'Add Plots','Plots/Add',0),
	(3220,'Medical Referee','Records/Edit/MedicalReferee',0),
	(3221,'Medical Contacts','Records/Edit/MedicalContacts',0),
	(3222,'Set Plot Capacity','Deeds/SetPlotCapacity',0),
	(3223,'Returning Payments','Finance/ReturningPayments',0),
	(3224,'Mapping Reporting','Plots/MappingReporting',0),
	(3225,'Approve Complete Work Permits','Diary/WorkPermits/ApproveComplete',0);

/*!40000 ALTER TABLE `tblAuthenticationPermission` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblAuthenticationPermissionAssignment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblAuthenticationPermissionAssignment`;

CREATE TABLE `tblAuthenticationPermissionAssignment` (
  `PermissionAssignmentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UserID` int(11) unsigned NOT NULL DEFAULT '0',
  `RoleID` int(11) unsigned NOT NULL DEFAULT '0',
  `PermissionID` int(11) unsigned NOT NULL DEFAULT '0',
  `Access` enum('Allowed','Denied') NOT NULL DEFAULT 'Denied',
  PRIMARY KEY (`PermissionAssignmentID`),
  KEY `UserID` (`UserID`),
  KEY `RoleID` (`RoleID`),
  KEY `PermissionID` (`PermissionID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tblAuthenticationPermissionAssignment` WRITE;
/*!40000 ALTER TABLE `tblAuthenticationPermissionAssignment` DISABLE KEYS */;

INSERT INTO `tblAuthenticationPermissionAssignment` (`PermissionAssignmentID`, `UserID`, `RoleID`, `PermissionID`, `Access`)
VALUES
	(2718,0,3,3176,'Denied'),
	(2717,0,3,3177,'Denied'),
	(2716,0,3,3162,'Denied'),
	(2715,0,3,3161,'Denied'),
	(2714,0,3,3160,'Denied'),
	(2713,0,3,3159,'Allowed'),
	(2712,0,3,3165,'Allowed'),
	(2711,0,3,3150,'Denied'),
	(2710,0,3,3143,'Allowed'),
	(2709,0,3,3138,'Denied'),
	(2708,0,3,3135,'Allowed'),
	(2707,0,3,3111,'Denied'),
	(2706,0,3,3109,'Allowed'),
	(2705,0,3,3107,'Denied'),
	(2704,0,3,3178,'Denied'),
	(2703,0,3,3106,'Allowed'),
	(2702,0,2,3176,'Denied'),
	(2701,0,2,3177,'Denied'),
	(2700,0,2,3137,'Denied'),
	(2699,0,2,3136,'Denied'),
	(2698,0,2,3135,'Denied'),
	(2697,0,2,3175,'Denied'),
	(2696,0,2,3174,'Denied'),
	(2695,0,2,3162,'Denied'),
	(2694,0,2,3161,'Denied'),
	(2693,0,2,3160,'Denied'),
	(2692,0,2,3159,'Allowed'),
	(2691,0,2,3142,'Denied'),
	(2690,0,2,3140,'Denied'),
	(2689,0,2,3124,'Denied'),
	(2688,0,2,3123,'Denied'),
	(2687,0,2,3122,'Denied'),
	(2686,0,2,3155,'Allowed'),
	(2685,0,2,3121,'Allowed'),
	(2684,0,2,3139,'Allowed'),
	(2683,0,2,3165,'Allowed'),
	(2682,0,2,3151,'Denied'),
	(2681,0,2,3149,'Denied'),
	(2680,0,2,3148,'Denied'),
	(2679,0,2,3150,'Denied'),
	(2678,0,2,3143,'Allowed'),
	(2677,0,2,3131,'Denied'),
	(2676,0,2,3128,'Allowed'),
	(2675,0,2,3103,'Denied'),
	(2674,0,2,3178,'Denied'),
	(2673,0,2,3102,'Allowed'),
	(2672,0,4,3176,'Denied'),
	(2671,0,4,3136,'Denied'),
	(2670,0,4,3135,'Allowed'),
	(2669,0,4,3131,'Allowed'),
	(2668,0,4,3140,'Denied'),
	(2667,0,4,3141,'Denied'),
	(2666,0,4,3162,'Denied'),
	(2665,0,4,3161,'Denied'),
	(2664,0,4,3160,'Denied'),
	(2663,0,4,3178,'Denied'),
	(2662,0,4,3159,'Allowed'),
	(2661,0,4,3139,'Allowed'),
	(2660,0,1,3176,'Allowed'),
	(2659,0,1,3165,'Allowed'),
	(2658,0,1,3163,'Allowed'),
	(2657,0,1,3159,'Allowed'),
	(2656,0,1,3154,'Allowed'),
	(2655,0,1,3143,'Allowed'),
	(2654,0,1,3139,'Allowed'),
	(2653,0,1,3128,'Allowed'),
	(2652,0,1,3125,'Allowed'),
	(2651,0,1,3121,'Allowed'),
	(2650,0,1,3099,'Denied'),
	(2649,0,1,3097,'Denied'),
	(2648,0,1,3112,'Denied'),
	(2647,0,1,3113,'Denied'),
	(2646,0,1,3178,'Denied'),
	(2645,0,1,3095,'Allowed'),
	(2644,0,5,3176,'Allowed'),
	(2643,0,5,3174,'Allowed'),
	(2642,0,5,3166,'Allowed'),
	(2641,0,5,3165,'Allowed'),
	(2640,0,5,3163,'Allowed'),
	(2639,0,5,3159,'Allowed'),
	(2638,0,5,3154,'Allowed'),
	(2637,0,5,3143,'Allowed'),
	(2636,0,5,3139,'Allowed'),
	(2635,0,5,3128,'Allowed'),
	(2634,0,5,3125,'Allowed'),
	(2633,0,5,3121,'Allowed'),
	(2632,0,5,3095,'Allowed'),
	(2719,0,5,3191,'Allowed'),
	(2720,0,5,3192,'Allowed'),
	(2721,0,1,3191,'Allowed'),
	(2722,0,1,3192,'Denied'),
	(2723,0,4,3191,'Allowed'),
	(2724,0,4,3192,'Denied'),
	(2725,0,2,3191,'Allowed'),
	(2726,0,2,3192,'Denied'),
	(2727,0,3,3191,'Allowed'),
	(2728,0,3,3192,'Denied'),
	(2729,0,5,3198,'Allowed'),
	(2730,0,5,3205,'Allowed'),
	(2731,0,1,3197,'Denied'),
	(2732,0,1,3198,'Denied'),
	(2733,0,1,3114,'Denied'),
	(2734,0,1,3206,'Denied'),
	(2735,0,1,3194,'Denied'),
	(2736,0,4,3197,'Denied'),
	(2737,0,4,3195,'Denied'),
	(2738,0,4,3196,'Denied'),
	(2739,0,4,3198,'Denied'),
	(2740,0,4,3114,'Denied'),
	(2741,0,4,3206,'Denied'),
	(2742,0,4,3194,'Denied'),
	(2743,0,2,3198,'Denied'),
	(2744,0,2,3197,'Denied'),
	(2745,0,2,3195,'Denied'),
	(2746,0,2,3196,'Denied'),
	(2747,0,2,3114,'Denied'),
	(2748,0,2,3206,'Denied'),
	(2749,0,2,3194,'Denied'),
	(2750,0,3,3198,'Denied'),
	(2751,0,3,3197,'Denied'),
	(2752,0,3,3195,'Denied'),
	(2753,0,3,3196,'Denied'),
	(2754,0,3,3114,'Denied'),
	(2755,0,3,3206,'Denied'),
	(2756,0,3,3194,'Denied'),
	(2757,0,1,3207,'Denied'),
	(2758,0,1,3208,'Denied'),
	(2759,0,1,3211,'Denied'),
	(2760,0,4,3207,'Denied'),
	(2761,0,4,3208,'Denied'),
	(2762,0,4,3209,'Denied'),
	(2763,0,4,3210,'Denied'),
	(2764,0,4,3211,'Denied'),
	(2765,0,2,3208,'Denied'),
	(2766,0,2,3207,'Denied'),
	(2767,0,2,3209,'Denied'),
	(2768,0,2,3210,'Denied'),
	(2769,0,2,3211,'Denied'),
	(2770,0,3,3208,'Denied'),
	(2771,0,3,3207,'Denied'),
	(2772,0,3,3209,'Denied'),
	(2773,0,3,3210,'Denied'),
	(2774,0,3,3211,'Denied'),
	(2775,0,6,3178,'Denied'),
	(2776,0,6,3179,'Denied'),
	(2777,0,6,3177,'Denied'),
	(2778,0,6,3176,'Denied'),
	(2779,0,6,3175,'Allowed'),
	(2780,0,6,3174,'Allowed'),
	(2781,0,6,3173,'Denied'),
	(2782,0,6,3172,'Denied'),
	(2783,0,6,3171,'Denied'),
	(2784,0,6,3170,'Denied'),
	(2785,0,6,3169,'Denied'),
	(2786,0,6,3168,'Denied'),
	(2787,0,6,3167,'Denied'),
	(2788,0,6,3166,'Denied'),
	(2789,0,6,3165,'Allowed'),
	(2790,0,6,3164,'Denied'),
	(2791,0,6,3163,'Denied'),
	(2792,0,6,3162,'Allowed'),
	(2793,0,6,3161,'Allowed'),
	(2794,0,6,3160,'Allowed'),
	(2795,0,6,3159,'Allowed'),
	(2796,0,6,3158,'Allowed'),
	(2797,0,6,3157,'Allowed'),
	(2798,0,6,3156,'Allowed'),
	(2799,0,6,3154,'Allowed'),
	(2800,0,6,3155,'Allowed'),
	(2801,0,6,3153,'Denied'),
	(2802,0,6,3152,'Denied'),
	(2803,0,6,3150,'Denied'),
	(2804,0,6,3151,'Denied'),
	(2805,0,6,3149,'Denied'),
	(2806,0,6,3148,'Denied'),
	(2807,0,6,3147,'Denied'),
	(2808,0,6,3146,'Denied'),
	(2809,0,6,3145,'Denied'),
	(2810,0,6,3144,'Denied'),
	(2811,0,6,3143,'Denied'),
	(2812,0,6,3142,'Denied'),
	(2813,0,6,3141,'Denied'),
	(2814,0,6,3140,'Denied'),
	(2815,0,6,3139,'Denied'),
	(2816,0,6,3138,'Denied'),
	(2817,0,6,3136,'Denied'),
	(2818,0,6,3137,'Denied'),
	(2819,0,6,3135,'Denied'),
	(2820,0,6,3134,'Denied'),
	(2821,0,6,3133,'Denied'),
	(2822,0,6,3132,'Denied'),
	(2823,0,6,3131,'Denied'),
	(2824,0,6,3130,'Denied'),
	(2825,0,6,3129,'Denied'),
	(2826,0,6,3128,'Denied'),
	(2827,0,6,3127,'Denied'),
	(2828,0,6,3126,'Denied'),
	(2829,0,6,3125,'Denied'),
	(2830,0,6,3124,'Allowed'),
	(2831,0,6,3122,'Allowed'),
	(2832,0,6,3123,'Allowed'),
	(2833,0,6,3121,'Allowed'),
	(2834,0,6,3120,'Allowed'),
	(2835,0,6,3119,'Denied'),
	(2836,0,6,3118,'Allowed'),
	(2837,0,6,3117,'Allowed'),
	(2838,0,6,3116,'Denied'),
	(2839,0,6,3114,'Denied'),
	(2840,0,6,3115,'Denied'),
	(2841,0,6,3113,'Allowed'),
	(2842,0,6,3112,'Allowed'),
	(2843,0,6,3106,'Denied'),
	(2844,0,6,3107,'Denied'),
	(2845,0,6,3108,'Denied'),
	(2846,0,6,3109,'Denied'),
	(2847,0,6,3110,'Denied'),
	(2848,0,6,3111,'Denied'),
	(2849,0,6,3104,'Allowed'),
	(2850,0,6,3105,'Allowed'),
	(2851,0,6,3103,'Allowed'),
	(2852,0,6,3102,'Allowed'),
	(2853,0,6,3101,'Denied'),
	(2854,0,6,3100,'Allowed'),
	(2855,0,6,3099,'Allowed'),
	(2856,0,6,3098,'Denied'),
	(2857,0,6,3097,'Denied'),
	(2858,0,6,3096,'Denied'),
	(2859,0,6,3095,'Allowed'),
	(2860,0,6,3180,'Denied'),
	(2861,0,6,3181,'Denied'),
	(2862,0,6,3182,'Denied'),
	(2863,0,6,3183,'Denied'),
	(2864,0,6,3184,'Denied'),
	(2865,0,6,3185,'Denied'),
	(2866,0,6,3186,'Allowed'),
	(2867,0,6,3187,'Allowed'),
	(2868,0,6,3188,'Denied'),
	(2869,0,6,3189,'Denied'),
	(2870,0,6,3190,'Denied'),
	(2871,0,6,3191,'Allowed'),
	(2872,0,6,3192,'Denied'),
	(2873,0,6,3193,'Denied'),
	(2874,0,6,3194,'Allowed'),
	(2875,0,6,3195,'Denied'),
	(2876,0,6,3196,'Denied'),
	(2877,0,6,3197,'Denied'),
	(2878,0,6,3198,'Denied'),
	(2879,0,6,3199,'Denied'),
	(2880,0,6,3200,'Denied'),
	(2881,0,6,3201,'Denied'),
	(2882,0,6,3202,'Denied'),
	(2883,0,6,3203,'Denied'),
	(2884,0,6,3204,'Denied'),
	(2885,0,6,3205,'Allowed'),
	(2886,0,6,3206,'Allowed'),
	(2887,0,6,3207,'Denied'),
	(2888,0,6,3208,'Denied'),
	(2889,0,6,3209,'Denied'),
	(2890,0,6,3210,'Denied'),
	(2891,0,6,3211,'Allowed'),
	(2892,0,6,3212,'Denied'),
	(2893,0,6,3213,'Denied'),
	(2894,0,6,3214,'Denied'),
	(2895,0,6,3215,'Denied'),
	(2896,0,6,3216,'Denied'),
	(2897,0,6,3217,'Denied'),
	(2898,0,6,3218,'Denied'),
	(2899,0,6,3219,'Allowed'),
	(2900,0,1,3224,'Denied'),
	(2901,0,4,3224,'Denied'),
	(2902,0,2,3224,'Denied'),
	(2903,0,3,3224,'Denied');

/*!40000 ALTER TABLE `tblAuthenticationPermissionAssignment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblAuthenticationRole
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblAuthenticationRole`;

CREATE TABLE `tblAuthenticationRole` (
  `RoleID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(40) NOT NULL DEFAULT '',
  `RedirectMenuItemID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`RoleID`),
  KEY `RedirectMenuItemID` (`RedirectMenuItemID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tblAuthenticationRole` WRITE;
/*!40000 ALTER TABLE `tblAuthenticationRole` DISABLE KEYS */;

INSERT INTO `tblAuthenticationRole` (`RoleID`, `RoleName`, `RedirectMenuItemID`)
VALUES
	(1,'Office Staff',0),
	(2,'Funeral Director',0),
	(3,'Mason',0),
	(4,'Ground Staff',0),
	(5,'Admin',0),
	(6,'Lite User',3);

/*!40000 ALTER TABLE `tblAuthenticationRole` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblAuthenticationUser
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblAuthenticationUser`;

CREATE TABLE `tblAuthenticationUser` (
  `UserID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Username` varchar(150) NOT NULL DEFAULT '',
  `Password` varchar(200) NOT NULL DEFAULT '',
  `RealName` varchar(100) NOT NULL DEFAULT '',
  `Token` varchar(200) NOT NULL DEFAULT '',
  `TokenExpiry` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Enabled` tinyint(1) NOT NULL DEFAULT '0',
  `PasswordResetHash` varchar(200) NOT NULL DEFAULT '',
  `PasswordResetDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `RoleID` int(11) unsigned NOT NULL DEFAULT '0',
  `Forename` varchar(80) NOT NULL DEFAULT '',
  `Surname` varchar(80) NOT NULL DEFAULT '',
  `FuneralDirectorID` int(11) unsigned NOT NULL DEFAULT '0',
  `MemorialMasonID` int(11) unsigned NOT NULL DEFAULT '0',
  `Email` varchar(150) NOT NULL DEFAULT '',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `DateUserAdded` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `JobTitle` varchar(150) NOT NULL DEFAULT '',
  `AboutMe` varchar(1000) NOT NULL DEFAULT '',
  `ImgPath` varchar(150) NOT NULL DEFAULT '',
  `Mobile` varchar(50) NOT NULL DEFAULT '',
  `FacilityIDs` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`UserID`),
  KEY `RoleID` (`RoleID`),
  KEY `FuneralDirectorID` (`FuneralDirectorID`),
  KEY `MemorialMasonID` (`MemorialMasonID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tblAuthenticationUser` WRITE;
/*!40000 ALTER TABLE `tblAuthenticationUser` DISABLE KEYS */;

INSERT INTO `tblAuthenticationUser` (`UserID`, `Username`, `Password`, `RealName`, `Token`, `TokenExpiry`, `Enabled`, `PasswordResetHash`, `PasswordResetDate`, `RoleID`, `Forename`, `Surname`, `FuneralDirectorID`, `MemorialMasonID`, `Email`, `IsDeleted`, `DateUserAdded`, `JobTitle`, `AboutMe`, `ImgPath`, `Mobile`, `FacilityIDs`)
VALUES
	(1,'plotbox','$6$rounds=10000$7203736c94de5d27$mRtwNevNYqkIO6S7wtn3K98J9eYmJdBQAOclCfFE8wMqu0ggDdPItq1BDLU3hB3oCE/8U6Up1gIJl6THZZ6bX1','Plotbox Admin','$6$rounds=10000$a786a167f2b2bbaf$7N17Up7IAmO.C1WxagLNDSNrT.npLqWVbxH9RKU40dZ0inV8mI3tOcD5pX1sxCRqABx3jE3L2iutRvRm8I1mX0','2015-02-13 16:14:55',1,'','0000-00-00 00:00:00',5,'Plotbox','Admin',0,0,'',0,'0000-00-00 00:00:00','','','','','');

/*!40000 ALTER TABLE `tblAuthenticationUser` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblAuthenticationUserRole
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblAuthenticationUserRole`;

CREATE TABLE `tblAuthenticationUserRole` (
  `UserRoleID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UserID` int(11) unsigned NOT NULL DEFAULT '0',
  `RoleID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`UserRoleID`),
  KEY `UserID` (`UserID`),
  KEY `RoleID` (`RoleID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tblAuthenticationUserRole` WRITE;
/*!40000 ALTER TABLE `tblAuthenticationUserRole` DISABLE KEYS */;

INSERT INTO `tblAuthenticationUserRole` (`UserRoleID`, `UserID`, `RoleID`)
VALUES
	(1,1,5);

/*!40000 ALTER TABLE `tblAuthenticationUserRole` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblAutoPayment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblAutoPayment`;

CREATE TABLE `tblAutoPayment` (
  `AutoPaymentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `AutoPaymentBatchID` int(11) unsigned NOT NULL DEFAULT '0',
  `PayeeName` mediumtext NOT NULL,
  `ContractNumber` varchar(50) NOT NULL DEFAULT '',
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `PaymentDate` date NOT NULL DEFAULT '0000-00-00',
  `PaymentType` varchar(50) NOT NULL DEFAULT '',
  `Amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `LinkedPaymentID` int(11) unsigned NOT NULL DEFAULT '0',
  `Status` enum('Pending','Processed','Ignored','Error') NOT NULL DEFAULT 'Pending',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `ErrorMessage` mediumtext NOT NULL,
  PRIMARY KEY (`AutoPaymentID`),
  KEY `AutoPaymentBatchID` (`AutoPaymentBatchID`),
  KEY `ContractID` (`ContractID`),
  KEY `LinkedPaymentID` (`LinkedPaymentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblAutoPaymentBatch
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblAutoPaymentBatch`;

CREATE TABLE `tblAutoPaymentBatch` (
  `AutoPaymentBatchID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Filename` mediumtext NOT NULL,
  `Status` enum('Pending','In Progress','Processed') NOT NULL DEFAULT 'Pending',
  `ProcessedBy` int(11) unsigned NOT NULL DEFAULT '0',
  `ProcessedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`AutoPaymentBatchID`),
  KEY `ProcessedBy` (`ProcessedBy`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblBook
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblBook`;

CREATE TABLE `tblBook` (
  `BookID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `BookName` varchar(200) NOT NULL DEFAULT '',
  `BookType` enum('Interment Register','Purchase Register','Contract','Deed','Map Book','Section Map','Interment Card','Lot Card','Other') NOT NULL DEFAULT 'Interment Register',
  `BookStartDate` date NOT NULL DEFAULT '0000-00-00',
  `BookEndDate` date NOT NULL DEFAULT '0000-00-00',
  `BookCoverPageID` int(11) DEFAULT NULL,
  PRIMARY KEY (`BookID`),
  KEY `FacilityID` (`FacilityID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblBookLink
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblBookLink`;

CREATE TABLE `tblBookLink` (
  `BookLinkID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `BookID` int(11) unsigned NOT NULL DEFAULT '0',
  `BookPageID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `BookType` enum('Interment Register','Purchase Register','Contract','Deed','Map Book','Section Map','Interment Card','Lot Card','Other') NOT NULL DEFAULT 'Interment Register',
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilitySectionID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotRow` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`BookLinkID`),
  KEY `BookID` (`BookID`),
  KEY `BookPageID` (`BookPageID`),
  KEY `ContactID` (`ContactID`),
  KEY `PlotID` (`PlotID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `FacilitySectionID` (`FacilitySectionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblBookPage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblBookPage`;

CREATE TABLE `tblBookPage` (
  `BookPageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `BookID` int(11) unsigned NOT NULL DEFAULT '0',
  `BookPageType` enum('Cover Page','Index Page','Content Page','Appendix Page','Other') NOT NULL DEFAULT 'Content Page',
  `Order` int(11) DEFAULT NULL,
  `UploadedDate` date NOT NULL DEFAULT '0000-00-00',
  `BookPageImageName` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`BookPageID`),
  KEY `BookID` (`BookID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblBurial
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblBurial`;

CREATE TABLE `tblBurial` (
  `BurialID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `FuneralDirectorID` int(11) unsigned NOT NULL DEFAULT '0',
  `PayerContactID` int(11) DEFAULT NULL,
  `FuneralDirectorContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotPurchaseID` int(11) unsigned NOT NULL DEFAULT '0',
  `DeedID` int(11) unsigned NOT NULL DEFAULT '0',
  `GenealogyID` int(11) unsigned NOT NULL DEFAULT '0',
  `ApplicantContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContactName` varchar(50) NOT NULL DEFAULT '',
  `ApplicantsRelation` varchar(50) NOT NULL DEFAULT '',
  `LetterOfAuthorityRequired` tinyint(1) NOT NULL DEFAULT '0',
  `Officiant` tinyint(1) NOT NULL DEFAULT '0',
  `OfficiantName` varchar(100) NOT NULL DEFAULT '',
  `DeceasedContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `KnownAs` varchar(50) NOT NULL DEFAULT '',
  `AgeAtDeath` int(11) DEFAULT NULL,
  `AgeAtDeathDays` int(11) DEFAULT NULL,
  `AgeAtDeathWeeks` int(11) DEFAULT NULL,
  `AgeAtDeathMonths` int(11) DEFAULT NULL,
  `AgeAtDeathHours` int(11) DEFAULT NULL,
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotSpaceID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlaceOfDeath` mediumtext NOT NULL,
  `DeceasedDate` date NOT NULL DEFAULT '0000-00-00',
  `ReligionDenominationID` int(11) unsigned NOT NULL DEFAULT '0',
  `MaritalStatus` varchar(50) NOT NULL DEFAULT '',
  `Occupation` varchar(50) NOT NULL DEFAULT '',
  `Parent1ContactID` int(11) DEFAULT NULL,
  `Parent2ContactID` int(11) DEFAULT NULL,
  `Resident` tinyint(1) NOT NULL DEFAULT '0',
  `PrivateRecord` tinyint(1) NOT NULL DEFAULT '0',
  `BurialType` varchar(50) NOT NULL DEFAULT '',
  `DateTimeBooked` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `BookedBy` int(11) DEFAULT NULL,
  `BurialDate` date NOT NULL DEFAULT '0000-00-00',
  `BurialTime` time NOT NULL DEFAULT '00:00:00',
  `BurialEndTime` time NOT NULL DEFAULT '00:00:00',
  `BurialStatus` enum('Draft','Confirmed','Complete','Cancelled') NOT NULL DEFAULT 'Draft',
  `NewGrave` tinyint(1) NOT NULL DEFAULT '0',
  `CoffinHeightFeet` int(11) DEFAULT NULL,
  `CoffinHeightInches` decimal(5,2) NOT NULL DEFAULT '0.00',
  `CoffinLengthFeet` int(11) DEFAULT NULL,
  `CoffinLengthInches` decimal(5,2) NOT NULL DEFAULT '0.00',
  `CoffinWidthFeet` int(11) DEFAULT NULL,
  `CoffinWidthInches` decimal(5,2) NOT NULL DEFAULT '0.00',
  `CoffinHeadFeet` int(11) DEFAULT NULL,
  `CoffinHeadInches` decimal(5,2) NOT NULL DEFAULT '0.00',
  `CoffinFootFeet` int(11) DEFAULT NULL,
  `CoffinFootInches` decimal(5,2) NOT NULL DEFAULT '0.00',
  `CoffinHeightMeters` int(11) DEFAULT NULL,
  `CoffinHeightCentimeters` decimal(4,2) NOT NULL DEFAULT '0.00',
  `CoffinLengthMeters` int(11) DEFAULT NULL,
  `CoffinLengthCentimeters` decimal(4,2) NOT NULL DEFAULT '0.00',
  `CoffinWidthMeters` int(11) DEFAULT NULL,
  `CoffinWidthCentimeters` decimal(4,2) NOT NULL DEFAULT '0.00',
  `CoffinHeadMeters` int(11) DEFAULT NULL,
  `CoffinHeadCentimeters` decimal(4,2) NOT NULL DEFAULT '0.00',
  `CoffinFootMeters` int(11) DEFAULT NULL,
  `CoffinFootCentimeters` decimal(4,2) NOT NULL DEFAULT '0.00',
  `CasketType` varchar(50) NOT NULL DEFAULT '',
  `MicrochipBarcodeNumber` varchar(100) NOT NULL DEFAULT '',
  `AdditionalInfoNotes` mediumtext NOT NULL,
  `DatePlotOpened` date NOT NULL DEFAULT '0000-00-00',
  `TimePlotOpened` time NOT NULL DEFAULT '00:00:00',
  `DatePlotClosed` date NOT NULL DEFAULT '0000-00-00',
  `TimePlotClosed` time NOT NULL DEFAULT '00:00:00',
  `BurialNotes` text NOT NULL,
  `CompletedByUserID` int(11) unsigned NOT NULL DEFAULT '0',
  `UrnSize` varchar(50) NOT NULL DEFAULT '',
  `UrnDescription` varchar(512) NOT NULL DEFAULT '',
  `UrnDeliveredBy` varchar(50) NOT NULL DEFAULT '',
  `ServiceLocation` varchar(512) NOT NULL DEFAULT '',
  `Veteran` tinyint(1) NOT NULL DEFAULT '0',
  `DeedHolderIsDeceased` tinyint(1) NOT NULL DEFAULT '0',
  `IsFamilyPayer` tinyint(1) NOT NULL DEFAULT '0',
  `DeceasedNotes` mediumtext NOT NULL,
  `RegistrationDate` date NOT NULL DEFAULT '0000-00-00',
  `RegistrationCertNumber` varchar(50) NOT NULL DEFAULT '',
  `IsTranscribed` tinyint(1) NOT NULL DEFAULT '0',
  `MilitaryTranscription` varchar(255) NOT NULL DEFAULT '',
  `RegistrationDistrictID` int(11) unsigned NOT NULL DEFAULT '0',
  `IntermentNumber` varchar(50) NOT NULL DEFAULT '',
  `Source` enum('','Manual','Event','Import') NOT NULL DEFAULT '',
  `GPName` varchar(100) NOT NULL DEFAULT '',
  `GPAddress` mediumtext NOT NULL,
  `GPCauseOfDeath` varchar(200) NOT NULL DEFAULT '',
  `GPNotes` mediumtext NOT NULL,
  `WeightPounds` int(11) DEFAULT NULL,
  `WeightKilos` int(11) DEFAULT NULL,
  `PersonOfInterest` tinyint(1) NOT NULL DEFAULT '0',
  `PersonOfInterestBlurb` mediumtext NOT NULL,
  `EventID` int(11) unsigned NOT NULL DEFAULT '0',
  `DiaryID` int(11) unsigned NOT NULL DEFAULT '0',
  `SiteID` int(11) unsigned NOT NULL DEFAULT '0',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `VeteranActiveDutyDeath` tinyint(1) NOT NULL DEFAULT '0',
  `VeteranBranch` varchar(50) NOT NULL DEFAULT '',
  `VeteranYearsOfService` varchar(50) NOT NULL DEFAULT '',
  `VeteranMilitaryRank` varchar(50) NOT NULL DEFAULT '',
  `VeteranNotes` mediumtext NOT NULL,
  `VaultType` varchar(50) NOT NULL DEFAULT '',
  `UrnType` varchar(50) NOT NULL DEFAULT '',
  `OriginalIntermentNumber` varchar(50) NOT NULL DEFAULT '',
  `IsGestationAge` tinyint(1) NOT NULL DEFAULT '0',
  `MarketingOptedIn` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`BurialID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `FuneralDirectorID` (`FuneralDirectorID`),
  KEY `FuneralDirectorContactID` (`FuneralDirectorContactID`),
  KEY `PlotPurchaseID` (`PlotPurchaseID`),
  KEY `DeedID` (`DeedID`),
  KEY `GenealogyID` (`GenealogyID`),
  KEY `ApplicantContactID` (`ApplicantContactID`),
  KEY `DeceasedContactID` (`DeceasedContactID`),
  KEY `PlotID` (`PlotID`),
  KEY `PlotSpaceID` (`PlotSpaceID`),
  KEY `ReligionDenominationID` (`ReligionDenominationID`),
  KEY `CompletedByUserID` (`CompletedByUserID`),
  KEY `RegistrationDistrictID` (`RegistrationDistrictID`),
  KEY `Burial_IntermentNumber_Index` (`IntermentNumber`),
  KEY `EventID` (`EventID`),
  KEY `DiaryID` (`DiaryID`),
  KEY `SiteID` (`SiteID`),
  KEY `Burial_OriginalIntermentNumber_Index` (`OriginalIntermentNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblBurialTypeCapacityReduction
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblBurialTypeCapacityReduction`;

CREATE TABLE `tblBurialTypeCapacityReduction` (
  `BurialTypeCapacityReductionID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PickListItemID` int(11) unsigned NOT NULL DEFAULT '0',
  `CapacityReductionType` enum('Interment','Inurnment') NOT NULL DEFAULT 'Interment',
  PRIMARY KEY (`BurialTypeCapacityReductionID`),
  KEY `PickListItemID` (`PickListItemID`),
  KEY `BurialTypeCapacityReduction_PickListItemID_Index` (`PickListItemID`),
  KEY `BurialTypeCapacityReduction_CapacityReductionType_Index` (`CapacityReductionType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tblBurialTypeCapacityReduction` WRITE;
/*!40000 ALTER TABLE `tblBurialTypeCapacityReduction` DISABLE KEYS */;

INSERT INTO `tblBurialTypeCapacityReduction` (`BurialTypeCapacityReductionID`, `PickListItemID`, `CapacityReductionType`)
VALUES
	(1,20,'Interment'),
	(2,21,'Interment'),
	(3,22,'Interment'),
	(4,23,'Interment'),
	(5,24,'Inurnment'),
	(6,25,'Interment'),
	(7,35,'Interment'),
	(8,36,'Interment'),
	(9,37,'Interment'),
	(10,38,'Interment'),
	(11,39,'Interment'),
	(12,40,'Interment'),
	(13,91,'Interment'),
	(14,92,'Interment'),
	(15,93,'Interment'),
	(16,94,'Interment'),
	(17,95,'Interment'),
	(18,96,'Interment'),
	(19,97,'Interment'),
	(20,98,'Interment'),
	(21,99,'Interment'),
	(22,122,'Interment'),
	(23,123,'Interment'),
	(24,124,'Interment'),
	(25,125,'Interment'),
	(26,126,'Interment'),
	(27,127,'Interment'),
	(28,128,'Interment'),
	(29,129,'Interment'),
	(30,130,'Interment'),
	(31,131,'Interment'),
	(32,132,'Interment'),
	(33,133,'Interment'),
	(34,141,'Interment'),
	(35,142,'Interment'),
	(36,143,'Interment'),
	(37,144,'Interment'),
	(38,145,'Interment'),
	(39,146,'Interment'),
	(40,147,'Interment'),
	(41,148,'Interment'),
	(42,149,'Interment'),
	(43,150,'Interment'),
	(44,151,'Interment'),
	(45,152,'Interment'),
	(46,153,'Interment'),
	(47,154,'Interment'),
	(48,155,'Interment'),
	(49,156,'Interment'),
	(50,157,'Interment'),
	(51,158,'Interment'),
	(52,159,'Interment'),
	(53,160,'Interment'),
	(54,161,'Interment'),
	(55,162,'Interment'),
	(56,163,'Interment'),
	(57,164,'Interment'),
	(58,168,'Interment'),
	(59,169,'Interment'),
	(60,170,'Interment'),
	(64,20,'Interment'),
	(65,21,'Interment'),
	(66,22,'Interment'),
	(67,23,'Interment'),
	(68,24,'Inurnment'),
	(69,25,'Interment'),
	(70,35,'Interment'),
	(71,36,'Interment'),
	(72,37,'Interment'),
	(73,38,'Interment'),
	(74,39,'Interment'),
	(75,40,'Interment'),
	(76,91,'Interment'),
	(77,92,'Interment'),
	(78,93,'Interment'),
	(79,94,'Interment'),
	(80,95,'Interment'),
	(81,96,'Interment'),
	(82,97,'Interment'),
	(83,98,'Interment'),
	(84,99,'Interment'),
	(85,122,'Interment'),
	(86,123,'Interment'),
	(87,124,'Interment'),
	(88,125,'Interment'),
	(89,126,'Interment'),
	(90,127,'Interment'),
	(91,128,'Interment'),
	(92,129,'Interment'),
	(93,130,'Interment'),
	(94,131,'Interment'),
	(95,132,'Interment'),
	(96,133,'Interment'),
	(97,141,'Interment'),
	(98,142,'Interment'),
	(99,143,'Interment'),
	(100,144,'Interment'),
	(101,145,'Interment'),
	(102,146,'Interment'),
	(103,147,'Interment'),
	(104,148,'Interment'),
	(105,149,'Interment'),
	(106,150,'Interment'),
	(107,151,'Interment'),
	(108,152,'Interment'),
	(109,153,'Interment'),
	(110,154,'Interment'),
	(111,155,'Interment'),
	(112,156,'Interment'),
	(113,157,'Interment'),
	(114,158,'Interment'),
	(115,159,'Interment'),
	(116,160,'Interment'),
	(117,161,'Interment'),
	(118,162,'Interment'),
	(119,163,'Interment'),
	(120,164,'Interment'),
	(121,168,'Interment'),
	(122,169,'Interment'),
	(123,170,'Interment');

/*!40000 ALTER TABLE `tblBurialTypeCapacityReduction` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblCancelledContract
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblCancelledContract`;

CREATE TABLE `tblCancelledContract` (
  `InvoiceID` int(11) unsigned NOT NULL DEFAULT '0',
  `DeedID` int(11) unsigned NOT NULL DEFAULT '0',
  `DivisionID` int(11) unsigned NOT NULL DEFAULT '0',
  `SiteID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilitySectionID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContractNumber` varchar(50) NOT NULL DEFAULT '',
  `Status` enum('New','Draft','Submitted','Approved','Complete','Cancelled','Quotation') NOT NULL DEFAULT 'New',
  `Requirement` enum('Pre-need','At need','Combo','Fulfilled') NOT NULL DEFAULT 'Pre-need',
  `DownPayment` decimal(8,2) NOT NULL DEFAULT '0.00',
  `AnnualPercentageRate` decimal(8,2) NOT NULL DEFAULT '0.00',
  `TotalCashPrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  `InstallmentAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `NumberOfInstallments` int(11) DEFAULT NULL,
  `RemainingInstallments` decimal(8,1) NOT NULL DEFAULT '0.0',
  `InstallmentAutoPay` tinyint(1) NOT NULL DEFAULT '0',
  `TotalSalePrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CreatedDate` date NOT NULL DEFAULT '0000-00-00',
  `NextExpectedPaymentDate` date NOT NULL DEFAULT '0000-00-00',
  `FirstPaymentDate` date NOT NULL DEFAULT '0000-00-00',
  `PurchaseDate` date NOT NULL DEFAULT '0000-00-00',
  `CompletionDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DocuSignID` varchar(50) NOT NULL DEFAULT '',
  `CommissionTotal` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Notes` mediumtext NOT NULL,
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `Source` enum('PlotBox','External') NOT NULL DEFAULT 'PlotBox',
  `ExtContractIdentifierName` varchar(50) NOT NULL DEFAULT '',
  `ExtContractIdentifier` varchar(50) NOT NULL DEFAULT '',
  `CreatedBy` int(11) DEFAULT NULL,
  `DeedRequired` tinyint(1) NOT NULL DEFAULT '1',
  `ContractCancelReason` mediumtext NOT NULL,
  `CancellationDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CancelledBy` int(11) DEFAULT NULL,
  `InstalmentCalculator` enum('NumberOfInstallments','InstallmentAmount') NOT NULL DEFAULT 'NumberOfInstallments',
  `DownPaymentDueDate` date NOT NULL DEFAULT '0000-00-00',
  `Historic` tinyint(1) NOT NULL DEFAULT '0',
  `CreditCarriedForwardContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `CreditCarriedForwardContractAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CreditCarriedForwardContractNotes` mediumtext NOT NULL,
  `CreditCarriedForwardBy` int(11) unsigned NOT NULL DEFAULT '0',
  `HasCancellations` tinyint(1) NOT NULL DEFAULT '0',
  `DeletedBy` int(11) unsigned NOT NULL DEFAULT '0',
  `DeletedDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CancelledContractID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `ApplicantID` int(11) unsigned NOT NULL DEFAULT '0',
  `CancellationType` enum('Partial','Complete') NOT NULL DEFAULT 'Partial',
  `OriginalRemainingBalance` decimal(8,2) NOT NULL DEFAULT '0.00',
  `NewRemainingBalance` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CancelledItemsTotal` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CancellationFeesTotal` decimal(8,2) NOT NULL DEFAULT '0.00',
  `TotalRefund` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CancellationNotes` mediumtext NOT NULL,
  `CancelledItemsQuantity` int(11) DEFAULT NULL,
  `PlotReleaseType` enum('Immediately','Release Date','Do Not Release') NOT NULL DEFAULT 'Do Not Release',
  `PlotReleaseDate` date NOT NULL DEFAULT '0000-00-00',
  `CancelledPlotIdsList` varchar(1000) NOT NULL DEFAULT '',
  `WaiveInterest` tinyint(1) NOT NULL DEFAULT '1',
  `WaiveInterestAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CancelledItemsIdsList` varchar(1000) NOT NULL DEFAULT '',
  `CancelledContractFeeIdsList` varchar(100) NOT NULL DEFAULT '',
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `PaymentScheduleID` int(11) unsigned NOT NULL DEFAULT '0',
  `HistoricCancellation` tinyint(1) NOT NULL DEFAULT '0',
  `CancelledContractDate` date NOT NULL DEFAULT '0000-00-00',
  `PaidAndFullDate` date NOT NULL DEFAULT '0000-00-00',
  `LeadSource` varchar(100) NOT NULL DEFAULT '',
  `LeadDetail` varchar(100) NOT NULL DEFAULT '',
  `LeadOwner` int(11) DEFAULT NULL,
  `DisallowCancellations` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CancelledContractID`),
  KEY `InvoiceID` (`InvoiceID`),
  KEY `DeedID` (`DeedID`),
  KEY `DivisionID` (`DivisionID`),
  KEY `SiteID` (`SiteID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `FacilitySectionID` (`FacilitySectionID`),
  KEY `CreditCarriedForwardContractID` (`CreditCarriedForwardContractID`),
  KEY `CreditCarriedForwardBy` (`CreditCarriedForwardBy`),
  KEY `DeletedBy` (`DeletedBy`),
  KEY `ContractNumber` (`ContractNumber`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `ContractID` (`ContractID`),
  KEY `ApplicantID` (`ApplicantID`),
  KEY `PaymentScheduleID` (`PaymentScheduleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblCancelledContractFee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblCancelledContractFee`;

CREATE TABLE `tblCancelledContractFee` (
  `CancelledContractFeeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CancelledContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceItemID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceRefID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceRefClass` varchar(250) NOT NULL DEFAULT '',
  `TaxRate` decimal(6,3) NOT NULL DEFAULT '0.000',
  `TaxAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `PriceIncTax` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Quantity` int(11) NOT NULL DEFAULT '0',
  `Total` decimal(8,2) NOT NULL DEFAULT '0.00',
  `SalesNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `CostNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `InventoryNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `SalesTaxNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContractItemsID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeType` varchar(250) NOT NULL DEFAULT '',
  `UnitDiscount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `UnitCostPrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CreatedBy` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`CancelledContractFeeID`),
  KEY `CancelledContractID` (`CancelledContractID`),
  KEY `ContractID` (`ContractID`),
  KEY `FeeID` (`FeeID`),
  KEY `InvoiceItemID` (`InvoiceItemID`),
  KEY `InvoiceRefID` (`InvoiceRefID`),
  KEY `SalesNominalCodeID` (`SalesNominalCodeID`),
  KEY `CostNominalCodeID` (`CostNominalCodeID`),
  KEY `InventoryNominalCodeID` (`InventoryNominalCodeID`),
  KEY `SalesTaxNominalCodeID` (`SalesTaxNominalCodeID`),
  KEY `ContractItemsID` (`ContractItemsID`),
  KEY `CreatedBy` (`CreatedBy`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblCancelledContractItems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblCancelledContractItems`;

CREATE TABLE `tblCancelledContractItems` (
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeID` int(11) unsigned NOT NULL DEFAULT '0',
  `Description` varchar(255) NOT NULL DEFAULT '',
  `CreatedDate` date NOT NULL DEFAULT '0000-00-00',
  `FulfilledDate` date NOT NULL DEFAULT '0000-00-00',
  `FulfilledBy` varchar(100) NOT NULL DEFAULT '',
  `Amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `TaxRate` decimal(6,3) NOT NULL DEFAULT '0.000',
  `Tax` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Quantity` int(11) DEFAULT NULL,
  `ReferenceFeeID` int(11) DEFAULT NULL,
  `ItemStatus` enum('Pre-need','At need') NOT NULL DEFAULT 'At need',
  `ConversionDate` date NOT NULL DEFAULT '0000-00-00',
  `QuantityFulfilled` int(11) DEFAULT NULL,
  `CancelledContractItemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CancelledContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContractItemID` int(11) unsigned NOT NULL DEFAULT '0',
  `CancelledQuantity` int(11) DEFAULT NULL,
  `ReasonForCancellation` mediumtext NOT NULL,
  `CancelledBy` int(11) DEFAULT NULL,
  `Notes` mediumtext NOT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CreditContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeType` varchar(250) NOT NULL DEFAULT '',
  `UnitDiscount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Funded` tinyint(1) NOT NULL DEFAULT '0',
  `IsStockUpdated` tinyint(1) NOT NULL DEFAULT '0',
  `UnitCostPrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`CancelledContractItemID`),
  KEY `ContractID` (`ContractID`),
  KEY `FeeID` (`FeeID`),
  KEY `CancelledContractID` (`CancelledContractID`),
  KEY `ContractItemID` (`ContractItemID`),
  KEY `CreditContractID` (`CreditContractID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblCancelledPlot
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblCancelledPlot`;

CREATE TABLE `tblCancelledPlot` (
  `CancelledPlotID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CancelledContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `DeedID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotReleaseType` enum('Immediately','Release Date','Do Not Release') NOT NULL DEFAULT 'Do Not Release',
  `PlotReleaseDate` date NOT NULL DEFAULT '0000-00-00',
  `Released` tinyint(1) NOT NULL DEFAULT '0',
  `DateReleased` date NOT NULL DEFAULT '0000-00-00',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`CancelledPlotID`),
  KEY `CancelledContractID` (`CancelledContractID`),
  KEY `PlotID` (`PlotID`),
  KEY `ContractID` (`ContractID`),
  KEY `DeedID` (`DeedID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblChartOfAccounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblChartOfAccounts`;

CREATE TABLE `tblChartOfAccounts` (
  `ChartOfAccountsID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `AccountsReceivableNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `CashAccountNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `OverPaymentsNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `RefundsNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `RevenueNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `SuspenseNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `CanDelete` tinyint(1) NOT NULL DEFAULT '0',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ChartOfAccountsID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `AccountsReceivableNominalCodeID` (`AccountsReceivableNominalCodeID`),
  KEY `CashAccountNominalCodeID` (`CashAccountNominalCodeID`),
  KEY `OverPaymentsNominalCodeID` (`OverPaymentsNominalCodeID`),
  KEY `RefundsNominalCodeID` (`RefundsNominalCodeID`),
  KEY `RevenueNominalCodeID` (`RevenueNominalCodeID`),
  KEY `SuspenseNominalCodeID` (`SuspenseNominalCodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tblChartOfAccounts` WRITE;
/*!40000 ALTER TABLE `tblChartOfAccounts` DISABLE KEYS */;

INSERT INTO `tblChartOfAccounts` (`ChartOfAccountsID`, `FacilityID`, `AccountsReceivableNominalCodeID`, `CashAccountNominalCodeID`, `OverPaymentsNominalCodeID`, `RefundsNominalCodeID`, `RevenueNominalCodeID`, `SuspenseNominalCodeID`, `CanDelete`, `Deleted`, `CreatedBy`, `DateCreated`, `LastUpdatedBy`, `LastUpdatedDateTime`)
VALUES
	(1,0,1,2,3,5,6,0,0,0,0,'2017-04-26 10:38:37',0,'2017-04-26 10:38:37');

/*!40000 ALTER TABLE `tblChartOfAccounts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblContact
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblContact`;

CREATE TABLE `tblContact` (
  `ContactID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(20) NOT NULL DEFAULT '',
  `Forename` varchar(100) NOT NULL DEFAULT '',
  `MiddleName` varchar(100) NOT NULL DEFAULT '',
  `Surname` varchar(100) NOT NULL DEFAULT '',
  `FullName` varchar(350) NOT NULL DEFAULT '',
  `AddressLine1` varchar(100) NOT NULL DEFAULT '',
  `AddressLine2` varchar(100) NOT NULL DEFAULT '',
  `Town` varchar(100) NOT NULL DEFAULT '',
  `County` varchar(100) NOT NULL DEFAULT '',
  `Country` varchar(100) NOT NULL DEFAULT '',
  `Postcode` varchar(12) NOT NULL DEFAULT '',
  `Mobile` varchar(50) NOT NULL DEFAULT '',
  `Landline` varchar(50) NOT NULL DEFAULT '',
  `EmailAddress` varchar(100) NOT NULL DEFAULT '',
  `Gender` enum('Unknown','Male','Female') NOT NULL DEFAULT 'Unknown',
  `DateOfBirth` date NOT NULL DEFAULT '0000-00-00',
  `Deceased` tinyint(1) NOT NULL DEFAULT '0',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `Notes` varchar(100) NOT NULL DEFAULT '',
  `CustomerNumber` varchar(50) NOT NULL DEFAULT '',
  `MasterContactID` int(11) DEFAULT NULL,
  `ExtContactIdentifier` varchar(50) DEFAULT NULL,
  `ExtContactIdentifierName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ContactID`),
  KEY `Title` (`Title`),
  KEY `Forename` (`Forename`),
  KEY `MiddleName` (`MiddleName`),
  KEY `Surname` (`Surname`),
  KEY `CustomerNumber` (`CustomerNumber`),
  KEY `ExtContactIdentifier_Idx` (`ExtContactIdentifier`),
  KEY `ExtContactIdentifierName_Idx` (`ExtContactIdentifierName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblContactHistories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblContactHistories`;

CREATE TABLE `tblContactHistories` (
  `ContactHistoryID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Type` varchar(100) NOT NULL DEFAULT '',
  `Subject` varchar(100) NOT NULL DEFAULT '',
  `Category` varchar(100) NOT NULL DEFAULT '',
  `Description` varchar(300) NOT NULL DEFAULT '',
  `CreatedBy` varchar(100) NOT NULL DEFAULT '',
  `Status` varchar(100) NOT NULL DEFAULT '',
  `CreatedDate` date NOT NULL DEFAULT '0000-00-00',
  `LastUpdatedDate` date NOT NULL DEFAULT '0000-00-00',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedFor` varchar(100) NOT NULL DEFAULT '',
  `TargetDate` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`ContactHistoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblContactQuotation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblContactQuotation`;

CREATE TABLE `tblContactQuotation` (
  `ContactQuotationID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Status` enum('In Progress','Won','Lost','Expired') NOT NULL DEFAULT 'In Progress',
  `ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `CreatedBy` int(11) unsigned NOT NULL DEFAULT '0',
  `Notes` mediumtext NOT NULL,
  `CreatedDate` date NOT NULL DEFAULT '0000-00-00',
  `LastUpdatedDate` date NOT NULL DEFAULT '0000-00-00',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `ExpiryDate` date NOT NULL DEFAULT '0000-00-00',
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotIDs` varchar(1000) NOT NULL DEFAULT '',
  `Requirement` enum('Pre-need','At need','Combo') NOT NULL DEFAULT 'Pre-need',
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ContactQuotationID`),
  KEY `ContactID` (`ContactID`),
  KEY `CreatedBy` (`CreatedBy`),
  KEY `FacilityID` (`FacilityID`),
  KEY `ContractID` (`ContractID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblContactRelationship
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblContactRelationship`;

CREATE TABLE `tblContactRelationship` (
  `ContactRelationshipID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ContactID1` int(11) DEFAULT NULL,
  `ContactID2` int(11) DEFAULT NULL,
  `RelationshipToContactID2` varchar(50) NOT NULL DEFAULT '',
  `LinkedRelationshipID` int(11) DEFAULT NULL,
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ContactRelationshipID`),
  KEY `ContactID1` (`ContactID1`),
  KEY `ContactID2` (`ContactID2`),
  KEY `RelationshipToContactID2` (`RelationshipToContactID2`),
  KEY `LinkedRelationshipID` (`LinkedRelationshipID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblContactRoles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblContactRoles`;

CREATE TABLE `tblContactRoles` (
  `ContactRoleID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Roles` enum('Contract Owner','Deed Owner','Decedent','Beneficiary','Applicant','Prospect','Payee','Memorial Owner','Other') NOT NULL DEFAULT 'Other',
  `ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ContactRoleID`),
  KEY `ContactID` (`ContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblContract
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblContract`;

CREATE TABLE `tblContract` (
  `ContractID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `InvoiceID` int(11) unsigned NOT NULL DEFAULT '0',
  `DeedID` int(11) unsigned NOT NULL DEFAULT '0',
  `DivisionID` int(11) unsigned NOT NULL DEFAULT '0',
  `SiteID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilitySectionID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContractNumber` varchar(50) NOT NULL DEFAULT '',
  `Status` enum('New','Draft','Submitted','Approved','Complete','Cancelled','Quotation') NOT NULL DEFAULT 'New',
  `Requirement` enum('Pre-need','At need','Combo','Fulfilled') NOT NULL DEFAULT 'Pre-need',
  `DownPayment` decimal(8,2) NOT NULL DEFAULT '0.00',
  `AnnualPercentageRate` decimal(8,2) NOT NULL DEFAULT '0.00',
  `TotalCashPrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  `InstallmentAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `NumberOfInstallments` int(11) DEFAULT NULL,
  `RemainingBalance` decimal(8,2) NOT NULL DEFAULT '0.00',
  `RemainingInstallments` decimal(8,1) NOT NULL DEFAULT '0.0',
  `InstallmentAutoPay` tinyint(1) NOT NULL DEFAULT '0',
  `TotalSalePrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CreatedDate` date NOT NULL DEFAULT '0000-00-00',
  `NextExpectedPaymentDate` date NOT NULL DEFAULT '0000-00-00',
  `FirstPaymentDate` date NOT NULL DEFAULT '0000-00-00',
  `PurchaseDate` date NOT NULL DEFAULT '0000-00-00',
  `CompletionDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DocuSignID` varchar(50) NOT NULL DEFAULT '',
  `CommissionTotal` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Notes` mediumtext NOT NULL,
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `Source` enum('PlotBox','External') NOT NULL DEFAULT 'PlotBox',
  `ExtContractIdentifierName` varchar(50) NOT NULL DEFAULT '',
  `ExtContractIdentifier` varchar(50) NOT NULL DEFAULT '',
  `CreatedBy` int(11) DEFAULT NULL,
  `DeedRequired` tinyint(1) NOT NULL DEFAULT '1',
  `ContractCancelReason` mediumtext NOT NULL,
  `CancellationDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CancelledBy` int(11) DEFAULT NULL,
  `InstalmentCalculator` enum('NumberOfInstallments','InstallmentAmount') NOT NULL DEFAULT 'NumberOfInstallments',
  `DownPaymentDueDate` date NOT NULL DEFAULT '0000-00-00',
  `Historic` tinyint(1) NOT NULL DEFAULT '0',
  `CreditCarriedForwardContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `CreditCarriedForwardContractAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CreditCarriedForwardContractNotes` mediumtext NOT NULL,
  `CreditCarriedForwardBy` int(11) unsigned NOT NULL DEFAULT '0',
  `HasCancellations` tinyint(1) NOT NULL DEFAULT '0',
  `DeletedBy` int(11) unsigned NOT NULL DEFAULT '0',
  `DeletedDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `PaymentScheduleID` int(11) unsigned NOT NULL DEFAULT '0',
  `PaidAndFullDate` date NOT NULL DEFAULT '0000-00-00',
  `LeadSource` varchar(100) NOT NULL DEFAULT '',
  `LeadDetail` varchar(100) NOT NULL DEFAULT '',
  `LeadOwner` int(11) DEFAULT NULL,
  `DisallowCancellations` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ContractID`),
  KEY `InvoiceID` (`InvoiceID`),
  KEY `DeedID` (`DeedID`),
  KEY `DivisionID` (`DivisionID`),
  KEY `SiteID` (`SiteID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `FacilitySectionID` (`FacilitySectionID`),
  KEY `CreditCarriedForwardContractID` (`CreditCarriedForwardContractID`),
  KEY `CreditCarriedForwardBy` (`CreditCarriedForwardBy`),
  KEY `DeletedBy` (`DeletedBy`),
  KEY `ContractNumber` (`ContractNumber`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `PaymentScheduleID` (`PaymentScheduleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblContractDocument
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblContractDocument`;

CREATE TABLE `tblContractDocument` (
  `ContractDocumentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `DocumentName` varchar(255) NOT NULL DEFAULT '',
  `FormTypeID` int(11) unsigned NOT NULL DEFAULT '0',
  `ImageUri` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`ContractDocumentID`),
  KEY `ContractID` (`ContractID`),
  KEY `FormTypeID` (`FormTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblContractFee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblContractFee`;

CREATE TABLE `tblContractFee` (
  `ContractFeeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `CommissionPercent` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Status` enum('Pre-need','At need','Fulfilled') NOT NULL DEFAULT 'At need',
  `FeeID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceItemID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceRefID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceRefClass` varchar(250) NOT NULL DEFAULT '',
  `TaxRate` decimal(6,3) NOT NULL DEFAULT '0.000',
  `TaxAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `PriceIncTax` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Quantity` int(11) NOT NULL DEFAULT '0',
  `Total` decimal(8,2) NOT NULL DEFAULT '0.00',
  `SalesNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `CostNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `InventoryNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `SalesTaxNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `CreditContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeType` varchar(250) NOT NULL DEFAULT '',
  `UnitDiscount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `UnitCostPrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`ContractFeeID`),
  KEY `ContractID` (`ContractID`),
  KEY `FeeID` (`FeeID`),
  KEY `InvoiceItemID` (`InvoiceItemID`),
  KEY `InvoiceRefID` (`InvoiceRefID`),
  KEY `SalesNominalCodeID` (`SalesNominalCodeID`),
  KEY `CostNominalCodeID` (`CostNominalCodeID`),
  KEY `InventoryNominalCodeID` (`InventoryNominalCodeID`),
  KEY `SalesTaxNominalCodeID` (`SalesTaxNominalCodeID`),
  KEY `CreditContractID` (`CreditContractID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblContractItemFulfillmentStatusLog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblContractItemFulfillmentStatusLog`;

CREATE TABLE `tblContractItemFulfillmentStatusLog` (
  `ContractItemFulfillmentStatusLogID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ContractItemID` int(11) unsigned NOT NULL DEFAULT '0',
  `StatusDate` date NOT NULL DEFAULT '0000-00-00',
  `QuantityFulfilled` int(11) DEFAULT NULL,
  `QuantityReceived` int(11) DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ContractItemFulfillmentStatusLogID`),
  KEY `ContractItemID` (`ContractItemID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblContractItems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblContractItems`;

CREATE TABLE `tblContractItems` (
  `ContractItemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeID` int(11) unsigned NOT NULL DEFAULT '0',
  `Description` varchar(255) NOT NULL DEFAULT '',
  `CreatedDate` date NOT NULL DEFAULT '0000-00-00',
  `FulfilledDate` date NOT NULL DEFAULT '0000-00-00',
  `FulfilledBy` varchar(100) NOT NULL DEFAULT '',
  `Amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `TaxRate` decimal(6,3) NOT NULL DEFAULT '0.000',
  `Tax` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Quantity` int(11) DEFAULT NULL,
  `ReferenceFeeID` int(11) DEFAULT NULL,
  `ItemStatus` enum('Pre-need','At need') NOT NULL DEFAULT 'At need',
  `ConversionDate` date NOT NULL DEFAULT '0000-00-00',
  `QuantityFulfilled` int(11) DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CreditContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeType` varchar(250) NOT NULL DEFAULT '',
  `UnitDiscount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Funded` tinyint(1) NOT NULL DEFAULT '0',
  `IsStockUpdated` tinyint(1) NOT NULL DEFAULT '0',
  `UnitCostPrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`ContractItemID`),
  KEY `ContractID` (`ContractID`),
  KEY `FeeID` (`FeeID`),
  KEY `CreditContractID` (`CreditContractID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblContractOwner
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblContractOwner`;

CREATE TABLE `tblContractOwner` (
  `ContractOwnerID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `Precedence` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ContractOwnerID`),
  KEY `ContractID` (`ContractID`),
  KEY `ContactID` (`ContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblContractPlots
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblContractPlots`;

CREATE TABLE `tblContractPlots` (
  `ContractPlotID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ContractPlotID`),
  KEY `ContractID` (`ContractID`),
  KEY `PlotID` (`PlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblContractWriter
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblContractWriter`;

CREATE TABLE `tblContractWriter` (
  `ContractWriterID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `UserID` int(11) unsigned NOT NULL DEFAULT '0',
  `Notes` mediumtext NOT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ContractWriterID`),
  KEY `ContractID` (`ContractID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblCoreLogEntry
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblCoreLogEntry`;

CREATE TABLE `tblCoreLogEntry` (
  `CoreLogEntryID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `LogSession` varchar(30) NOT NULL DEFAULT '',
  `EntryDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Category` varchar(50) NOT NULL DEFAULT '',
  `Message` varchar(400) NOT NULL DEFAULT '',
  `AdditionalData` mediumtext NOT NULL,
  `IpAddress` varchar(15) NOT NULL DEFAULT '',
  `ExecutionTime` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `ExecutionGapTime` decimal(12,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`CoreLogEntryID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table tblCoreMigration
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblCoreMigration`;

CREATE TABLE `tblCoreMigration` (
  `CoreMigrationID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `OldVersion` varchar(10) NOT NULL DEFAULT '',
  `CurrentVersion` varchar(10) NOT NULL DEFAULT '',
  `MigrationTimeStamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`CoreMigrationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblCoreMigration` WRITE;
/*!40000 ALTER TABLE `tblCoreMigration` DISABLE KEYS */;

INSERT INTO `tblCoreMigration` (`CoreMigrationID`, `OldVersion`, `CurrentVersion`, `MigrationTimeStamp`)
VALUES
	(1,'0','2.37','2017-04-26 10:38:37'),
	(2,'2.37','2.6','2017-07-23 21:21:18'),
	(3,'2.6','2.62','2017-08-07 07:37:12'),
	(4,'2.62','2.81','2017-09-28 06:05:32'),
	(5,'2.81','2.82','2017-10-13 07:25:54'),
	(6,'2.82','2.86','2017-11-14 04:52:48'),
	(7,'2.86','2.88','2017-12-05 06:54:47'),
	(8,'2.88','3.12','2018-02-20 01:23:58'),
	(9,'3.12','3.16','2018-03-09 02:53:23'),
	(10,'3.16','3.17','2018-03-16 09:02:58'),
	(11,'3.17','3.18','2018-04-05 07:17:38'),
	(12,'3.18','3.19','2018-04-13 11:13:54'),
	(13,'3.19','3.21','2018-05-15 03:42:01'),
	(14,'3.21','3.25','2018-06-04 12:44:45'),
	(15,'3.25','3.26','2018-06-13 08:40:06'),
	(16,'3.26','3.31','2018-06-18 06:51:47'),
	(17,'3.31','3.32','2018-06-21 12:04:27'),
	(18,'3.32','3.45','2018-09-12 10:50:48'),
	(19,'3.45','3.46','2018-09-25 01:47:47');

/*!40000 ALTER TABLE `tblCoreMigration` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblCremation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblCremation`;

CREATE TABLE `tblCremation` (
  `CremationID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CremationNumber` int(11) DEFAULT NULL,
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `FuneralDirectorID` int(11) unsigned NOT NULL DEFAULT '0',
  `FuneralDirectorContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `GenealogyID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotPurchaseID` int(11) unsigned NOT NULL DEFAULT '0',
  `PayerContactID` int(11) DEFAULT NULL,
  `AgeAtDeath` int(11) DEFAULT NULL,
  `AgeAtDeathDays` int(11) DEFAULT NULL,
  `AgeAtDeathWeeks` int(11) DEFAULT NULL,
  `AgeAtDeathMonths` int(11) DEFAULT NULL,
  `AgeAtDeathHours` int(11) DEFAULT NULL,
  `PlaceOfDeath` varchar(50) NOT NULL DEFAULT '',
  `DeceasedDate` date NOT NULL DEFAULT '0000-00-00',
  `ReligionDenominationID` int(11) unsigned NOT NULL DEFAULT '0',
  `MaritalStatus` varchar(50) NOT NULL DEFAULT '',
  `Parents` varchar(50) NOT NULL DEFAULT '',
  `Location` varchar(100) NOT NULL DEFAULT '',
  `CremationType` enum('Standard','Stillborn baby','Body parts','NVF') NOT NULL DEFAULT 'Standard',
  `ServiceType` enum('Full','Committal','Memorial') NOT NULL DEFAULT 'Full',
  `ApplicantContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `ApplicantIsRelative` tinyint(1) NOT NULL DEFAULT '0',
  `ApplicantsRelation` varchar(50) NOT NULL DEFAULT '',
  `Officiant` tinyint(1) NOT NULL DEFAULT '0',
  `OfficiantName` varchar(100) NOT NULL DEFAULT '',
  `KnownAs` varchar(50) NOT NULL DEFAULT '',
  `MusicRequired` tinyint(1) NOT NULL DEFAULT '0',
  `MusicIn` varchar(100) NOT NULL DEFAULT '',
  `MusicOut` varchar(100) NOT NULL DEFAULT '',
  `WebcastingRequired` tinyint(1) NOT NULL DEFAULT '0',
  `WebcastingRequirements` mediumtext NOT NULL,
  `Bearers` tinyint(1) NOT NULL DEFAULT '0',
  `SizeOfService` varchar(50) NOT NULL DEFAULT '',
  `CollectionPlate` tinyint(1) NOT NULL DEFAULT '0',
  `SpecialInstruction` mediumtext NOT NULL,
  `DeceasedContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `Occupation` varchar(100) NOT NULL DEFAULT '',
  `MedicalRefereeID` int(11) DEFAULT NULL,
  `MedicalContact1ID` int(11) DEFAULT NULL,
  `MedicalContact2ID` int(11) DEFAULT NULL,
  `Parent1ContactID` int(11) DEFAULT NULL,
  `Parent2ContactID` int(11) DEFAULT NULL,
  `DeceasedNotes` mediumtext NOT NULL,
  `GPName` varchar(100) NOT NULL DEFAULT '',
  `GPAddress` mediumtext NOT NULL,
  `GPTimeOfDeath` time NOT NULL DEFAULT '00:00:00',
  `GPPlaceOfDeath` varchar(200) NOT NULL DEFAULT '',
  `GPCauseOfDeath` varchar(200) NOT NULL DEFAULT '',
  `GPNotes` mediumtext NOT NULL,
  `CremationStatus` enum('Draft','Confirmed','Complete','Cancelled') NOT NULL DEFAULT 'Draft',
  `BookedBy` int(11) DEFAULT NULL,
  `CremationDate` date NOT NULL DEFAULT '0000-00-00',
  `CremationTime` time NOT NULL DEFAULT '00:00:00',
  `CremationEndTime` time NOT NULL DEFAULT '00:00:00',
  `DateTimeBooked` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `RemainsDisposal` enum('','Scattered','Scattered (Witnessed)','Buried','Collected','Memorial','Niche','Removed') NOT NULL DEFAULT '',
  `RemainsDisposalDetails` mediumtext NOT NULL,
  `Resident` tinyint(1) NOT NULL DEFAULT '0',
  `CremationNotes` text NOT NULL,
  `PrivateRecord` tinyint(1) NOT NULL DEFAULT '0',
  `Veteran` tinyint(1) NOT NULL DEFAULT '0',
  `RegistrationDistrict` varchar(300) NOT NULL DEFAULT '',
  `IsFamilyPayer` tinyint(1) NOT NULL DEFAULT '0',
  `AshesCollectionDate` date NOT NULL DEFAULT '0000-00-00',
  `IsAshesCollected` tinyint(1) NOT NULL DEFAULT '0',
  `RetentionOfMetals` tinyint(1) NOT NULL DEFAULT '0',
  `CollectionType` enum('','Transit Box','Polytainer','Casket') NOT NULL DEFAULT 'Transit Box',
  `RegistrationDate` date NOT NULL DEFAULT '0000-00-00',
  `RegistrationCertNumber` varchar(50) NOT NULL DEFAULT '',
  `RegistrationDistrictID` int(11) unsigned NOT NULL DEFAULT '0',
  `Source` enum('','Manual','Event','Import') NOT NULL DEFAULT '',
  `AshesCollectedBy` varchar(100) NOT NULL DEFAULT '',
  `WeightPounds` int(11) DEFAULT NULL,
  `WeightKilos` int(11) DEFAULT NULL,
  `PersonOfInterest` tinyint(1) NOT NULL DEFAULT '0',
  `PersonOfInterestBlurb` varchar(50) NOT NULL DEFAULT '',
  `EventID` int(11) unsigned NOT NULL DEFAULT '0',
  `DiaryID` int(11) unsigned NOT NULL DEFAULT '0',
  `SiteID` int(11) unsigned NOT NULL DEFAULT '0',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `VeteranActiveDutyDeath` tinyint(1) NOT NULL DEFAULT '0',
  `VeteranBranch` varchar(50) NOT NULL DEFAULT '',
  `VeteranYearsOfService` varchar(50) NOT NULL DEFAULT '',
  `VeteranMilitaryRank` varchar(50) NOT NULL DEFAULT '',
  `VeteranNotes` mediumtext NOT NULL,
  `OriginalCremationNumber` int(11) DEFAULT NULL,
  `MemorialReminderRequested` tinyint(1) NOT NULL DEFAULT '0',
  `MemorialReminderDate` date NOT NULL DEFAULT '0000-00-00',
  `MemorialReminderDateSent` date NOT NULL DEFAULT '0000-00-00',
  `IsGestationAge` tinyint(1) NOT NULL DEFAULT '0',
  `MusicType` varchar(100) NOT NULL DEFAULT '',
  `MusicalInstructionsOther` mediumtext NOT NULL,
  `MarketingOptedIn` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CremationID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `FuneralDirectorID` (`FuneralDirectorID`),
  KEY `FuneralDirectorContactID` (`FuneralDirectorContactID`),
  KEY `GenealogyID` (`GenealogyID`),
  KEY `PlotPurchaseID` (`PlotPurchaseID`),
  KEY `ReligionDenominationID` (`ReligionDenominationID`),
  KEY `ApplicantContactID` (`ApplicantContactID`),
  KEY `DeceasedContactID` (`DeceasedContactID`),
  KEY `RegistrationDistrictID` (`RegistrationDistrictID`),
  KEY `Cremation_CremationNumber_Index` (`CremationNumber`),
  KEY `EventID` (`EventID`),
  KEY `DiaryID` (`DiaryID`),
  KEY `SiteID` (`SiteID`),
  KEY `Cremation_OriginalCremationNumber_Index` (`OriginalCremationNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblCRMContact
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblCRMContact`;

CREATE TABLE `tblCRMContact` (
  `CRMContactID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `LifeCycleStage` varchar(100) NOT NULL DEFAULT '',
  `LeadSource` varchar(100) NOT NULL DEFAULT '',
  `LeadStatus` varchar(100) NOT NULL DEFAULT '',
  `LeadDetail` varchar(100) NOT NULL DEFAULT '',
  `CreatedBy` varchar(100) NOT NULL DEFAULT '',
  `JobTitle` varchar(100) NOT NULL DEFAULT '',
  `CreatedDate` date NOT NULL DEFAULT '0000-00-00',
  `LastUpdatedDate` date NOT NULL DEFAULT '0000-00-00',
  `Deceased` tinyint(1) NOT NULL DEFAULT '0',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `ProtectionStartDate` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`CRMContactID`),
  KEY `ContactID` (`ContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblDeed
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblDeed`;

CREATE TABLE `tblDeed` (
  `DeedID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `SiteID` int(11) unsigned NOT NULL DEFAULT '0',
  `CreatedForBurialID` int(11) unsigned NOT NULL DEFAULT '0',
  `DeedNumber` varchar(50) NOT NULL DEFAULT '',
  `NumberOfPlots` int(11) NOT NULL DEFAULT '0',
  `NumberOfIntermentRights` int(11) NOT NULL DEFAULT '0',
  `NumberOfInurnmentRights` int(11) NOT NULL DEFAULT '0',
  `ActiveOwner` tinyint(1) NOT NULL DEFAULT '0',
  `ParentDeedID` int(11) unsigned NOT NULL DEFAULT '0',
  `DeedStatus` enum('New','Pending','Live','Cancelled') NOT NULL DEFAULT 'Live',
  `TransferStatus` enum('','Required','In Progress','Approved','Rejected') NOT NULL DEFAULT '',
  `TransferAgreementDocument` varchar(150) NOT NULL DEFAULT '',
  `DeclarationFormDocument` varchar(150) NOT NULL DEFAULT '',
  `SignedByNewOwner` tinyint(1) NOT NULL DEFAULT '0',
  `SignedByAuthority` tinyint(1) NOT NULL DEFAULT '0',
  `TransferNotes` varchar(500) NOT NULL DEFAULT '',
  `PlotPurchaseDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TransferFinishedDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LeaseStartDate` date NOT NULL DEFAULT '0000-00-00',
  `LeaseExpiryDate` date NOT NULL DEFAULT '0000-00-00',
  `LeaseLengthYears` int(11) DEFAULT NULL,
  `PerpetualLease` tinyint(1) NOT NULL DEFAULT '0',
  `NextOfKinDetails` text NOT NULL,
  `PlotsList` varchar(1000) NOT NULL DEFAULT '',
  `PlotIdsList` varchar(1000) NOT NULL DEFAULT '',
  `Cost` decimal(8,2) NOT NULL DEFAULT '0.00',
  `ReceiptNumber` varchar(50) NOT NULL DEFAULT '',
  `Notes` mediumtext NOT NULL,
  `IsProblematic` tinyint(1) NOT NULL DEFAULT '0',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `IsLinkedInContract` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TransferDate` date NOT NULL DEFAULT '0000-00-00',
  `Source` enum('PlotBox','External') NOT NULL DEFAULT 'PlotBox',
  PRIMARY KEY (`DeedID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `SiteID` (`SiteID`),
  KEY `CreatedForBurialID` (`CreatedForBurialID`),
  KEY `ParentDeedID` (`ParentDeedID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblDeedDecedent
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblDeedDecedent`;

CREATE TABLE `tblDeedDecedent` (
  `DeedDecedentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DeedID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `Relation` varchar(50) NOT NULL DEFAULT '',
  `Requirement` enum('','Pre-need','At-need') NOT NULL DEFAULT '',
  `BurialDate` date NOT NULL DEFAULT '0000-00-00',
  `Precedence` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`DeedDecedentID`),
  KEY `DeedID` (`DeedID`),
  KEY `ContactID` (`ContactID`),
  KEY `ContractID` (`ContractID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblDeedDocument
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblDeedDocument`;

CREATE TABLE `tblDeedDocument` (
  `DeedDocumentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DeedID` int(11) unsigned NOT NULL DEFAULT '0',
  `DocumentName` varchar(255) NOT NULL DEFAULT '',
  `FormTypeID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`DeedDocumentID`),
  KEY `DeedID` (`DeedID`),
  KEY `FormTypeID` (`FormTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblDeedFee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblDeedFee`;

CREATE TABLE `tblDeedFee` (
  `DeedFeeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DeedID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `CommissionPercent` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Status` enum('Pre-need','At need','Fulfilled') NOT NULL DEFAULT 'At need',
  `FeeID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceItemID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceRefID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceRefClass` varchar(250) NOT NULL DEFAULT '',
  `TaxRate` decimal(6,3) NOT NULL DEFAULT '0.000',
  `TaxAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `PriceIncTax` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Quantity` int(11) NOT NULL DEFAULT '0',
  `Total` decimal(8,2) NOT NULL DEFAULT '0.00',
  `SalesNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `CostNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `InventoryNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `SalesTaxNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeType` varchar(250) NOT NULL DEFAULT '',
  `UnitDiscount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `UnitCostPrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`DeedFeeID`),
  KEY `DeedID` (`DeedID`),
  KEY `PlotID` (`PlotID`),
  KEY `ContractID` (`ContractID`),
  KEY `FeeID` (`FeeID`),
  KEY `InvoiceItemID` (`InvoiceItemID`),
  KEY `InvoiceRefID` (`InvoiceRefID`),
  KEY `SalesNominalCodeID` (`SalesNominalCodeID`),
  KEY `CostNominalCodeID` (`CostNominalCodeID`),
  KEY `InventoryNominalCodeID` (`InventoryNominalCodeID`),
  KEY `SalesTaxNominalCodeID` (`SalesTaxNominalCodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblDeedOwner
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblDeedOwner`;

CREATE TABLE `tblDeedOwner` (
  `DeedOwnerID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DeedID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `Precedence` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`DeedOwnerID`),
  KEY `DeedID` (`DeedID`),
  KEY `ContactID` (`ContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblDiary
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblDiary`;

CREATE TABLE `tblDiary` (
  `DiaryID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `SiteID` int(11) unsigned NOT NULL DEFAULT '0',
  `DiaryName` varchar(50) NOT NULL DEFAULT '',
  `EventType` enum('Any','Burial','Cremation') NOT NULL DEFAULT 'Any',
  `SingleSlots` tinyint(1) NOT NULL DEFAULT '0',
  `SlotLength` int(11) NOT NULL DEFAULT '0',
  `StartTime` time NOT NULL DEFAULT '00:00:00',
  `EndTime` time NOT NULL DEFAULT '00:00:00',
  `PublicStartTime` time NOT NULL DEFAULT '00:00:00',
  `PublicEndTime` time NOT NULL DEFAULT '00:00:00',
  `DisplayOrder` tinyint(4) NOT NULL DEFAULT '0',
  `VariableLengthSlots` tinyint(1) NOT NULL DEFAULT '0',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`DiaryID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `SiteID` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblDiarySlot
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblDiarySlot`;

CREATE TABLE `tblDiarySlot` (
  `DiarySlotID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DiaryID` int(11) unsigned NOT NULL DEFAULT '0',
  `StartTime` time NOT NULL DEFAULT '00:00:00',
  `EndTime` time NOT NULL DEFAULT '00:00:00',
  `Public` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`DiarySlotID`),
  KEY `DiaryID` (`DiaryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblDivision
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblDivision`;

CREATE TABLE `tblDivision` (
  `DivisionID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DivisionName` varchar(50) NOT NULL DEFAULT '',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`DivisionID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tblDivision` WRITE;
/*!40000 ALTER TABLE `tblDivision` DISABLE KEYS */;

INSERT INTO `tblDivision` (`DivisionID`, `DivisionName`, `IsDeleted`)
VALUES
	(1,'Calvary Cemetery',0);

/*!40000 ALTER TABLE `tblDivision` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblDocumentRequest
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblDocumentRequest`;

CREATE TABLE `tblDocumentRequest` (
  `DocumentRequestID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ModelType` enum('Contract','Deed','Plot','Cremation') NOT NULL DEFAULT 'Contract',
  `DocumentTemplate` enum('','ContractDocument','ContractSummary','ContractMonthlyStatement','CremationCertificate') NOT NULL DEFAULT '',
  `RequestedByUserID` int(11) DEFAULT NULL,
  `UniqueIdentifiers` mediumtext NOT NULL,
  `SaveDir` mediumtext NOT NULL,
  `ZipURL` mediumtext NOT NULL,
  `Processed` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ProcessParams` mediumtext NOT NULL,
  `ExportDataModel` enum('','FacilityPlotExport') NOT NULL DEFAULT '',
  `ExportURL` mediumtext NOT NULL,
  PRIMARY KEY (`DocumentRequestID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblDocumentTemplate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblDocumentTemplate`;

CREATE TABLE `tblDocumentTemplate` (
  `TemplateID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `TemplateName` varchar(70) NOT NULL DEFAULT '',
  `TemplateType` enum('','Burial Order','Deed','Invoice','Payment Receipt','Memorial Mason Permit','Memorial Mason Work Permit','Contract Document','Cremation Certificate','Authority to Cremate','Floral Tribute Labels','Daily Cremation Schedule','Chapel Attendant Instruction Sheet','Invoice Payment Reminders','Identity Label to Cremate','Appointments Diary','Contract Summary','Cancelled Contract Document','Statement Document','Payment Batch Receipt','Daily Burial Schedule','Collection Letter At Need 30 Days Over Due Document','Collection Letter At Need 60 Days Over Due Document','Collection Letter At Need 90 Days Over Due Document','Collection Letter At Need 90 Days Plus Over Due Document','Collection Letter Pre-need 30 Days Over Due Document','Collection Letter Pre-need 60 Days Over Due Document','Collection Letter Pre-need 90 Days Over Due Document','Collection Letter Pre-need 90 Days Plus Over Due Document','Contract Monthly Statement','Work Order Document','Contact Quotation Document','Payments Received Report','Contract Sales Report','Contract Fulfillment Summary') NOT NULL DEFAULT '',
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `FileLocation` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`TemplateID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `TemplateType` (`TemplateType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblDocumentTemplate` WRITE;
/*!40000 ALTER TABLE `tblDocumentTemplate` DISABLE KEYS */;

INSERT INTO `tblDocumentTemplate` (`TemplateID`, `TemplateName`, `TemplateType`, `FacilityID`, `FileLocation`)
VALUES
	(1,'Burial Order','Burial Order',0,'templates/BurialOrder.docx'),
	(2,'Deed Document','Deed',0,'static/calvarycemetery/templates/0/2-DeedDocument.docx'),
	(3,'Memorial Mason Permit','Memorial Mason Permit',0,'templates/MemorialMasonPermit.docx'),
	(4,'Memorial Mason Work Permit','Memorial Mason Work Permit',0,'templates/MemorialMasonWorkPermit.docx'),
	(5,'Invoice','Invoice',0,'templates/Invoice.docx'),
	(6,'Payment Receipt','Payment Receipt',0,'templates/Receipt.docx'),
	(7,'Contract Document','Contract Document',0,'templates/ContractReport.docx'),
	(8,'Cremation Certificate','Cremation Certificate',0,'templates/CertificateOfCremation.docx'),
	(9,'Authority To Cremate','Authority to Cremate',0,'templates/AuthorityToCremate.docx'),
	(10,'Floral Tribute Labels','Floral Tribute Labels',0,'templates/FloralTributeLabels.docx'),
	(11,'Daily Cremation Schedule','Daily Cremation Schedule',0,'templates/DailyCremationSchedule.docx'),
	(12,'Chapel Attendant Instruction Sheet','Chapel Attendant Instruction Sheet',0,'templates/ChapelAttendantInstructionSheet.docx'),
	(13,'Invoice Payment Reminders','Invoice Payment Reminders',0,'templates/InvoicePaymentReminder.docx'),
	(14,'Identity Label','Identity Label to Cremate',0,'templates/IdentityLabel.docx'),
	(15,'Appointments Diary','Appointments Diary',0,'templates/AppointmentsDiary.docx'),
	(16,'Contract Summary','Contract Summary',0,'templates/ContractSummary.docx'),
	(17,'Cancelled Contract Document','Cancelled Contract Document',0,'templates/CancelledContractDocument.docx'),
	(18,'Statement Document','Statement Document',0,'templates/Statement.docx'),
	(19,'Collection Letter At Need 30 Days Over Due Document','Collection Letter At Need 30 Days Over Due Document',0,'templates/CollectionLetterAtNeed30DaysOverDue.docx'),
	(20,'Collection Letter At Need 60 Days Over Due Document','Collection Letter At Need 60 Days Over Due Document',0,'templates/CollectionLetterAtNeed60DaysOverDue.docx'),
	(21,'Collection Letter At Need 90 Days Over Due Document','Collection Letter At Need 90 Days Over Due Document',0,'templates/CollectionLetterAtNeed90DaysOverDue.docx'),
	(22,'Collection Letter At Need 90 Days Plus Over Due Document','Collection Letter At Need 90 Days Plus Over Due Document',0,'templates/CollectionLetterAtNeed90DaysPlusOverDue.docx'),
	(23,'Collection Letter Pre-need 30 Days Over Due Document','Collection Letter Pre-need 30 Days Over Due Document',0,'templates/CollectionLetterPreNeed30DaysOverDue.docx'),
	(24,'Collection Letter Pre-need 60 Days Over Due Document','Collection Letter Pre-need 60 Days Over Due Document',0,'templates/CollectionLetterPreNeed60DaysOverDue.docx'),
	(25,'Collection Letter Pre-need 90 Days Over Due Document','Collection Letter Pre-need 90 Days Over Due Document',0,'templates/CollectionLetterPreNeed90DaysOverDue.docx'),
	(26,'Collection Letter Pre-need 90 Days Plus Over Due Document','Collection Letter Pre-need 90 Days Plus Over Due Document',0,'templates/CollectionLetterPreNeed90DaysPlusOverDue.docx'),
	(27,'Contract Monthly Statement','Contract Monthly Statement',0,'templates/ContractMonthlyStatement.docx'),
	(28,'Payment Batch Receipt Document','Payment Batch Receipt',0,'templates/PaymentBatchReceipt.docx'),
	(29,'Daily Burial Schedule Document','Daily Burial Schedule',0,'templates/DailyBurialScheduleDocument.docx'),
	(30,'Work OrderDocument','Work Order Document',0,'templates/WorkOrderDocument.docx'),
	(31,'Contact Quotation Document','Contact Quotation Document',0,'templates/ContactQuotationDocument.docx'),
	(32,'Payments Received Report','Payments Received Report',0,'templates/PaymentsReceivedReport.docx'),
	(33,'Contract Sales Report','Contract Sales Report',0,'templates/ContractSalesReport.docx'),
	(34,'Contract Fulfillment Summary','Contract Fulfillment Summary',0,'templates/ContractFulfillmentSummary.docx');

/*!40000 ALTER TABLE `tblDocumentTemplate` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblEmailTemplate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblEmailTemplate`;

CREATE TABLE `tblEmailTemplate` (
  `EmailTemplateID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `EmailTemplateName` varchar(50) NOT NULL DEFAULT '',
  `FacilityID` int(11) DEFAULT NULL,
  `ParentTemplateName` varchar(50) NOT NULL DEFAULT '',
  `Template` text NOT NULL,
  `UserEdited` tinyint(1) NOT NULL DEFAULT '0',
  `PrimaryModel` varchar(128) NOT NULL DEFAULT '',
  `Enabled` tinyint(1) NOT NULL DEFAULT '1',
  `EnabledConfigurable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`EmailTemplateID`),
  KEY `EmailTemplateName` (`EmailTemplateName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblEmailTemplate` WRITE;
/*!40000 ALTER TABLE `tblEmailTemplate` DISABLE KEYS */;

INSERT INTO `tblEmailTemplate` (`EmailTemplateID`, `EmailTemplateName`, `FacilityID`, `ParentTemplateName`, `Template`, `UserEdited`, `PrimaryModel`, `Enabled`, `EnabledConfigurable`)
VALUES
	(1,'Main',0,'','{\"Subject\":\"{Subject}\",\"Content\":\"\\t\\t\\t\\t\\t<html>\\n\\t\\t\\t\\t\\t<head>\\n\\t\\t\\t\\t\\t\\t<title>{Subject}<\\/title>\\n\\t\\t\\t\\t\\t<\\/head>\\n\\t\\t\\t\\t\\t<body style=\\\"font-family:Sans-serif\\\">\\n\\t\\t\\t\\t\\t\\t{Content}<br>\\n\\t\\t\\t\\t\\t\\t<p>Sent from {SubDomain}<\\/p>\\n\\t\\t\\t\\t\\t<\\/body>\\n\\t\\t\\t\\t\\t<\\/html>\"}',0,'',1,1),
	(2,'FD Burial Confirmed',0,'Main','{\"Subject\":\"{DeceasedName} Burial Confirmed\",\"Content\":\"\\t\\t\\t\\t<p>We are emailing to inform you that the following {EventType} <strong>has been confirmed.<\\/strong><\\/p>\\n\\t\\t\\t\\t<p>Please read the details below carefully to ensure they are correct.<\\/p>\\n\\n\\t\\t\\t\\t<p>Date\\/Time of Burial: {DateTimeBooked}<br>\\n\\t\\t\\t\\tFacility: {Facility}<br>\\n\\t\\t\\t\\tGrave No: {PlotIdentifier}<br>\\n\\t\\t\\t\\tDeceased Name: {DeceasedName}<br>\\n\\t\\t\\t\\tCasket Dimensions: {CoffinDimensions}<br>\\n\\t\\t\\t\\tDate of Death: {DeceasedDate}<br>\\n\\t\\t\\t\\tFees: {EventFees}<br>\\n\\t\\t\\t\\tDeed Owner: {DeedOwner}<\\/p>\\n\\n\\t\\t\\t\\t<p>Please click to this following <a href=\\\"http:\\/\\/{SubDomain}\\/events\\/burials\\/{BurialID}\\/?acknowledge\\\">link<\\/a>\\n\\t\\t\\t\\tin order to acknowledge the burial<\\/p>\\n\\n\\t\\t\\t\\t<p>For any queries please contact the Cemetery Office.<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\Burial\\Burial',1,1),
	(3,'FD Cremation Confirmed',0,'Main','{\"Subject\":\"Cremation Confirmed\",\"Content\":\"\\t\\t\\t\\t<p>We are emailing to inform you that the following {EventType} <strong>has been confirmed.<\\/strong><\\/p>\\n\\t\\t\\t\\t<p>Please read the details below carefully to ensure they are correct.<\\/p>\\n\\n\\t\\t\\t\\t<p>Date\\/Time Booked: {DateTimeBooked}<br>\\n\\t\\t\\t\\tDate\\/Time of Burial: {BurialDate} {BurialTime}<br>\\n\\t\\t\\t\\tFacility: {Facility}<br>\\n\\t\\t\\t\\tDeceased Name: {DeceasedName}<br>\\n\\t\\t\\t\\tFees: {EventFees}<br>\\n\\t\\t\\t\\t<\\/p>\\n\\n\\t\\t\\t\\t<p>Please click to this <a href=\\\"http:\\/\\/{SubDomain}\\/events\\/cremations\\/{CremationID}\\/?acknowledge\\\">link<\\/a>\\n\\t\\t\\t\\tin order to acknowledge the cremation<\\/p>\\n\\n\\t\\t\\t\\t<p>For any queries please contact the Cemetery Office.<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\Cremation\\Cremation',1,1),
	(4,'FD Self Register Pending',0,'Main','{\"Subject\":\"Thank you for signing up for an online account\",\"Content\":\"\\t\\t\\t<p>Thank you for signing up for an online account. Your request is currently awaiting approval by an administrator. You should receive an email reply shortly<\\/p>\\n\\t\\t\\t<p>If have any queries please contact us.<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\FuneralDirector\\FuneralDirector',1,0),
	(5,'FD Self Register Accepted',0,'Main','{\"Subject\":\"Thank you for signing up for an online account\",\"Content\":\"\\t\\t\\t<p>Thank you for signing up for an online account. Your request has now been approved and you can log in using the following details:<\\/p>\\n\\t\\t\\t<p><a href=\'http:\\/\\/{URL}\'>http:\\/\\/{URL}<\\/a><br \\/>\\n\\t\\t\\tUsername: {EmailAddress}<br\\/>\\n\\t\\t\\tPassword: This will be what you entered during the registration process.\\n\\t\\t\\t<\\/p>\\n\\t\\t\\t<p>If have any queries please contact us.<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\FuneralDirector\\FuneralDirector',1,0),
	(6,'MM Self Register Pending',0,'Main','{\"Subject\":\"Thank you for signing up for an online account\",\"Content\":\"\\t\\t\\t<p>Thank you for signing up for an online account. Your request is currently awaiting approval by an administrator. You should receive an email reply shortly<\\/p>\\n\\t\\t\\t<p>If have any queries please contact us.<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\MemorialMason\\MemorialMason',1,0),
	(7,'MM Self Register Accepted',0,'Main','{\"Subject\":\"Thank you for signing up for an online account\",\"Content\":\"\\t\\t\\t<p>Thank you for signing up for an online account. Your request has now been approved and you can log in using the following details:<\\/p>\\n\\t\\t\\t<p><a href=\'http:\\/\\/{URL}\'>http:\\/\\/{URL}<\\/a><br \\/>\\n\\t\\t\\tUsername: {EmailAddress}<br\\/>\\n\\t\\t\\tPassword: This will be what you entered during the registration process.\\n\\t\\t\\t<\\/p>\\n\\t\\t\\t<p>If have any queries please contact us.<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\MemorialMason\\MemorialMason',1,0),
	(8,'FD Burial Request',0,'Main','{\"Subject\":\"Burial Requested\",\"Content\":\"\\t\\t<p>Thank you for your {EventType} request.<\\/p>\\n\\t\\t<p>The following is a brief summary of the details received<\\/p>\\n\\t\\t<p>\\n\\t\\tDeceased Name: {DeceasedName}<br>\\n\\t\\tFacility: {Facility}<br>\\n\\t\\tDate\\/Time Booked: {DateTimeBooked}<br>\\n\\t\\tDate\\/Time of Burial: {BurialDate} {BurialTime}<br>\\n\\t\\tDate of Death: {DeceasedDate}<br>\\n\\t\\tGrave No: {PlotIdentifier}<br>\\n\\t\\tCasket Dimensions: {CoffinDimensions}<br>\\n\\t\\tFees: {EventFees}<br>\\n\\t\\tDeed Owner: {DeedOwner}\\n\\t\\t<\\/p>\\n\\t\\t<p>Please ensure that the above details submitted are correct<\\/p>\\n\\t\\t<p>We will revert to you when this burial has been confirmed.<\\/p>\\n\\t\\t<strong>Please note that this email does not confirm the {EventType}<\\/strong>\\n\\t\\t<p>For any queries please contact the Cemetery Office.<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\Burial\\Burial',1,1),
	(9,'FD Cremation Request',0,'Main','{\"Subject\":\"Cremation Requested\",\"Content\":\"\\t\\t<p>Thank you for your {EventType} request.<\\/p>\\n\\t\\t<p>The following is a brief summary of the details received<\\/p>\\n\\t\\t<p>\\n\\t\\tDeceased Name: {DeceasedName}<br>\\n\\t\\tFacility: {Facility}<br>\\n\\t\\tDate\\/Time: {DateTimeBooked}<br>\\n\\t\\tFees: {EventFees}<br>\\n\\t\\t<\\/p>\\n\\t\\t<p>Please ensure that the above details submitted are correct<\\/p>\\n\\t\\t<p>We will revert to you when this burial has been confirmed.<\\/p>\\n\\t\\t<strong>Please note that this email does not confirm the {EventType}<\\/strong>\\n\\t\\t<p>For any queries please contact the Cemetery Office.<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\Cremation\\Cremation',1,1),
	(10,'FD Invoice Generated',0,'Main','{\"Subject\":\"Invoice Generated\",\"Content\":\"\\t\\t<p>Hello {FuneralDirectorName}<\\/p>\\n\\t\\t<p>We are just letting you know that an Invoice has been generated<\\/p>\\n\\t\\t<p>Please log into the portal to review and pay<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\FuneralDirector\\FuneralDirector',1,1),
	(11,'MM Invoice Generated',0,'Main','{\"Subject\":\"Invoice Generated\",\"Content\":\"\\t\\t<p>Hello {MemorialMasonName}<\\/p>\\n\\t\\t<p>We are just letting you know that an Invoice has been generated<\\/p>\\n\\t\\t<p>Please log into the portal to review and pay<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\MemorialMason\\MemorialMason',1,1),
	(12,'Registration Permit Renewal',0,'Main','{\"Subject\":\"Registration Permit Renewal\",\"Content\":\"\\t\\t<p>Dear {ContactName} <\\/p>\\n\\t\\t<p>We are just emailing you to let you know that according to the details we have, your Registration Permit is due for renewal before {RegistrationRenewalDate}<\\/p>\\n\\t\\t<p>In order to keep our information up-to-date, please log in to your portal and request a new registration permit as soon as possible.<\\/p>\\n\\t\\t<p>Kind regards<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\MemorialMason\\MemorialMason',1,1),
	(13,'FD Insurance Renewal',0,'Main','{\"Subject\":\"Insurance Renewal\",\"Content\":\"\\t\\t<p>Dear {ContactName}<\\/p>\\n\\t\\t<p>We are just emailing you to let you know that according to the details we have, your Funeral Director Insurance is due for renewal before {InsuranceRenewalDate}<\\/p>\\n\\t\\t<p>In order to keep our information up-to-date, please log in to your portal and upload the new documents as soon as they are available.<\\/p>\\n\\t\\t<p>Kind regards<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\FuneralDirector\\FuneralDirector',1,1),
	(14,'Mason Insurance Renewal',0,'Main','{\"Subject\":\"Insurance Renewal\",\"Content\":\"\\t\\t<p>Dear {ContactName}<\\/p>\\n\\t\\t<p>We are just emailing you to let you know that according to the details we have, your Memorial Mason Insurance is due for renewal before {InsuranceRenewalDate}<\\/p>\\n\\t\\t<p>In order to keep our information up-to-date, please log in to your portal and upload the new documents as soon as they are available.<\\/p>\\n\\t\\t<p>Kind regards<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\MemorialMason\\MemorialMason',1,1),
	(15,'Invoice Reminder',0,'Main','{\"Subject\":\"Invoice Reminder\",\"Content\":\"\\t\\t<p>Dear {ContactName}<\\/p>\\n\\t\\t<p>Payment for invoice # {InvoiceID} is now overdue. The amount outstanding is {AmountOutstanding}. Please log in to your portal or contact us to arrange payment.<\\/p>\\n\\t\\t<p>Kind regards<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\Finance\\Invoice',1,1),
	(16,'FD Insurance Expired',0,'Main','{\"Subject\":\"Insurance Expired\",\"Content\":\"\\t\\t<p>Dear {FuneralDirectorName}<\\/p>\\n\\t\\t<p>We are just emailing you to let you know that according to the details we have, your Funeral Director Insurance expired on the {InsuranceRenewalDate}<\\/p>\\n\\t\\t<p>In order to keep our information up-to-date, please log in to your portal and upload the new documents as soon as they are available.<\\/p>\\n\\t\\t<p>Kind regards<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\FuneralDirector\\FuneralDirector',1,1),
	(17,'Mason Insurance Expired',0,'Main','{\"Subject\":\"Insurance Expired\",\"Content\":\"\\t\\t<p>Dear {MemorialMasonName}<\\/p>\\n\\t\\t<p>We are just emailing you to let you know that according to the details we have, your Memorial Mason Insurance expired on the {InsuranceRenewalDate}<\\/p>\\n\\t\\t<p>In order to keep our information up-to-date, please log in to your portal and upload the new documents as soon as they are available.<\\/p>\\n\\t\\t<p>Kind regards<\\/p>\"}',0,'Everafter\\PlotBox\\Model\\MemorialMason\\MemorialMason',1,1);

/*!40000 ALTER TABLE `tblEmailTemplate` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblEvent
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblEvent`;

CREATE TABLE `tblEvent` (
  `EventID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DiaryID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `SiteID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilitySectionID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `FuneralDirectorID` int(11) unsigned NOT NULL DEFAULT '0',
  `EventModelID` int(11) unsigned NOT NULL DEFAULT '0',
  `EventName` varchar(50) NOT NULL DEFAULT '',
  `EventType` enum('Burial','Cremation','Exhumation','Stone Masonry','Maintenance','Appointment','Blocked','Scattering') NOT NULL DEFAULT 'Burial',
  `EventDate` date NOT NULL DEFAULT '0000-00-00',
  `EventStartTime` time NOT NULL DEFAULT '00:00:00',
  `EventEndTime` time NOT NULL DEFAULT '00:00:00',
  `EventStatus` enum('Draft','Confirmed','Cancelled','Complete') NOT NULL DEFAULT 'Draft',
  `Cost` decimal(8,2) NOT NULL DEFAULT '0.00',
  `EventDetails` text NOT NULL,
  `DeceasedName` varchar(110) NOT NULL DEFAULT '',
  `BurialType` varchar(50) NOT NULL DEFAULT '',
  `ContactName` varchar(110) NOT NULL DEFAULT '',
  `EventSource` enum('Office','Portal','Integration') NOT NULL DEFAULT 'Office',
  `AcknowledgedByFD` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ScatteringLocation` mediumtext NOT NULL,
  `DeceasedContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `IsWitnessedScattering` tinyint(1) NOT NULL DEFAULT '0',
  `ScatteringWitnessContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `PointOfContactID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`EventID`),
  KEY `DiaryID` (`DiaryID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `SiteID` (`SiteID`),
  KEY `FacilitySectionID` (`FacilitySectionID`),
  KEY `PlotID` (`PlotID`),
  KEY `FuneralDirectorID` (`FuneralDirectorID`),
  KEY `EventModelID` (`EventModelID`),
  KEY `DeceasedContactID` (`DeceasedContactID`),
  KEY `ScatteringWitnessContactID` (`ScatteringWitnessContactID`),
  KEY `PointOfContactID` (`PointOfContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblEventDocument
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblEventDocument`;

CREATE TABLE `tblEventDocument` (
  `EventDocumentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `EventID` int(11) unsigned NOT NULL DEFAULT '0',
  `DocumentName` varchar(255) NOT NULL DEFAULT '',
  `FormTypeID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`EventDocumentID`),
  KEY `EventID` (`EventID`),
  KEY `FormTypeID` (`FormTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblEventFee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblEventFee`;

CREATE TABLE `tblEventFee` (
  `EventFeeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `EventID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceItemID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceRefID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceRefClass` varchar(250) NOT NULL DEFAULT '',
  `TaxRate` decimal(6,3) NOT NULL DEFAULT '0.000',
  `TaxAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `PriceIncTax` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Quantity` int(11) NOT NULL DEFAULT '0',
  `Total` decimal(8,2) NOT NULL DEFAULT '0.00',
  `SalesNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `CostNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `InventoryNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `SalesTaxNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeType` varchar(250) NOT NULL DEFAULT '',
  `UnitDiscount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `UnitCostPrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`EventFeeID`),
  KEY `EventID` (`EventID`),
  KEY `FeeID` (`FeeID`),
  KEY `InvoiceItemID` (`InvoiceItemID`),
  KEY `InvoiceRefID` (`InvoiceRefID`),
  KEY `SalesNominalCodeID` (`SalesNominalCodeID`),
  KEY `CostNominalCodeID` (`CostNominalCodeID`),
  KEY `InventoryNominalCodeID` (`InventoryNominalCodeID`),
  KEY `SalesTaxNominalCodeID` (`SalesTaxNominalCodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblExhumation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblExhumation`;

CREATE TABLE `tblExhumation` (
  `ExhumationID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `GenealogyID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `ExhumationName` varchar(50) NOT NULL DEFAULT '',
  `ExhumationDate` date NOT NULL DEFAULT '0000-00-00',
  `ExhumationStartTime` time NOT NULL DEFAULT '00:00:00',
  `ExhumationEndTime` time NOT NULL DEFAULT '00:00:00',
  `ExhumationStatus` enum('Draft','Confirmed','Complete','Cancelled') NOT NULL DEFAULT 'Draft',
  `Notes` text NOT NULL,
  `EventID` int(11) unsigned NOT NULL DEFAULT '0',
  `ApplicantContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `ApplicantsRelation` varchar(50) NOT NULL DEFAULT '',
  `ExhumationRemain` enum('Returned to the same grave','Returned to another grave within the same cemetery','Re-interred in another cemetery','Cremated','Scattered','To examine the remains or coffin contents') NOT NULL DEFAULT 'Returned to the same grave',
  `ExhumationRemainNotes` text NOT NULL,
  `MinistryOfJustice` tinyint(1) NOT NULL DEFAULT '0',
  `PublicHealth` tinyint(1) NOT NULL DEFAULT '0',
  `EnvironmentalHealth` tinyint(1) NOT NULL DEFAULT '0',
  `ExhumationRemainReleasedNotes` text NOT NULL,
  `WhoWillRemainReleased` varchar(50) NOT NULL DEFAULT '',
  `DiaryID` int(11) unsigned NOT NULL DEFAULT '0',
  `SiteID` int(11) unsigned NOT NULL DEFAULT '0',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ExhumationID`),
  KEY `PlotID` (`PlotID`),
  KEY `GenealogyID` (`GenealogyID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `EventID` (`EventID`),
  KEY `ApplicantContactID` (`ApplicantContactID`),
  KEY `DiaryID` (`DiaryID`),
  KEY `SiteID` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblFacility
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFacility`;

CREATE TABLE `tblFacility` (
  `FacilityID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilityName` varchar(100) NOT NULL DEFAULT '',
  `FacilityType` enum('Cemetery','Crematorium') NOT NULL DEFAULT 'Cemetery',
  `Latitude` decimal(11,8) NOT NULL DEFAULT '0.00000000',
  `Longitude` decimal(11,8) NOT NULL DEFAULT '0.00000000',
  `PointConversionLatitudeOffset` decimal(11,10) NOT NULL DEFAULT '0.0000000000',
  `PointConversionLongitudeOffset` decimal(11,10) NOT NULL DEFAULT '0.0000000000',
  `AerialImageTopLeftLatitude` decimal(11,8) NOT NULL DEFAULT '0.00000000',
  `AerialImageTopLeftLongitude` decimal(11,8) NOT NULL DEFAULT '0.00000000',
  `AerialImageBottomRightLatitude` decimal(11,8) NOT NULL DEFAULT '0.00000000',
  `AerialImageBottomRightLongitude` decimal(11,8) NOT NULL DEFAULT '0.00000000',
  `DrawingImageTopLeftLatitude` decimal(11,8) NOT NULL DEFAULT '0.00000000',
  `DrawingImageTopLeftLongitude` decimal(11,8) NOT NULL DEFAULT '0.00000000',
  `DrawingImageBottomRightLatitude` decimal(11,8) NOT NULL DEFAULT '0.00000000',
  `DrawingImageBottomRightLongitude` decimal(11,8) NOT NULL DEFAULT '0.00000000',
  `MapID` varchar(100) NOT NULL DEFAULT '',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `IsWoodlandBurial` tinyint(1) NOT NULL DEFAULT '0',
  `TiledMapURI` varchar(300) NOT NULL DEFAULT '',
  `Map3dURI` varchar(300) NOT NULL DEFAULT '',
  `IsForcedJsRegenRequired` tinyint(1) NOT NULL DEFAULT '0',
  `DisplayOrder` tinyint(4) NOT NULL DEFAULT '0',
  `FacilityCode` varchar(100) NOT NULL DEFAULT '',
  `CurrentMapHash` varchar(255) NOT NULL DEFAULT '',
  `AddressLine1` varchar(100) NOT NULL DEFAULT '',
  `AddressLine2` varchar(100) NOT NULL DEFAULT '',
  `Town` varchar(100) NOT NULL DEFAULT '',
  `County` varchar(100) NOT NULL DEFAULT '',
  `Country` varchar(100) NOT NULL DEFAULT '',
  `Postcode` varchar(12) NOT NULL DEFAULT '',
  `ContactName` varchar(100) NOT NULL DEFAULT '',
  `ContactEmailAddress` varchar(100) NOT NULL DEFAULT '',
  `ContactPhone` varchar(100) NOT NULL DEFAULT '',
  `PrimaryImageID` int(11) NOT NULL DEFAULT '0',
  `CurrentImmersiveViewHash` varchar(255) NOT NULL DEFAULT '',
  `MapIconScaleFactor` decimal(2,1) NOT NULL DEFAULT '0.0',
  `PublicFacilityName` varchar(100) NOT NULL DEFAULT '',
  `FacilityBlurb` varchar(1000) NOT NULL DEFAULT '',
  `ArcGisLink` mediumtext NOT NULL,
  `IsForced360JsRegenRequired` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`FacilityID`),
  KEY `SiteID` (`SiteID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tblFacility` WRITE;
/*!40000 ALTER TABLE `tblFacility` DISABLE KEYS */;

INSERT INTO `tblFacility` (`FacilityID`, `SiteID`, `FacilityName`, `FacilityType`, `Latitude`, `Longitude`, `PointConversionLatitudeOffset`, `PointConversionLongitudeOffset`, `AerialImageTopLeftLatitude`, `AerialImageTopLeftLongitude`, `AerialImageBottomRightLatitude`, `AerialImageBottomRightLongitude`, `DrawingImageTopLeftLatitude`, `DrawingImageTopLeftLongitude`, `DrawingImageBottomRightLatitude`, `DrawingImageBottomRightLongitude`, `MapID`, `IsDeleted`, `IsWoodlandBurial`, `TiledMapURI`, `Map3dURI`, `IsForcedJsRegenRequired`, `DisplayOrder`, `FacilityCode`, `CurrentMapHash`, `AddressLine1`, `AddressLine2`, `Town`, `County`, `Country`, `Postcode`, `ContactName`, `ContactEmailAddress`, `ContactPhone`, `PrimaryImageID`, `CurrentImmersiveViewHash`, `MapIconScaleFactor`, `PublicFacilityName`, `FacilityBlurb`, `ArcGisLink`, `IsForced360JsRegenRequired`)
VALUES
	(1,1,'Calvary Cemetery','Cemetery',0.00000000,0.00000000,0.0000000000,0.0000000000,0.00000000,0.00000000,0.00000000,0.00000000,0.00000000,0.00000000,0.00000000,0.00000000,'f38526adaf5d4170a83775b9bfb949f9',0,0,'https://tiles.arcgis.com/tiles/LTisjkyAR6ZTffiz/arcgis/rest/services/AerialMapAndPaperMap_1_calvarycemetery/MapServer','',1,0,'','dc3a313678a730c8186f66d663256552','','','','','','','','','',0,'',0.0,'','','',0);

/*!40000 ALTER TABLE `tblFacility` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblFacilityImage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFacilityImage`;

CREATE TABLE `tblFacilityImage` (
  `FacilityImageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `UploadedDate` date NOT NULL DEFAULT '0000-00-00',
  `FileType` varchar(100) NOT NULL DEFAULT '',
  `Order` int(11) DEFAULT NULL,
  `Type` enum('Grounds','Office','Other') NOT NULL DEFAULT 'Other',
  PRIMARY KEY (`FacilityImageID`),
  KEY `FacilityID` (`FacilityID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblFacilitySection
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFacilitySection`;

CREATE TABLE `tblFacilitySection` (
  `FacilitySectionID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `SectionName` varchar(50) NOT NULL DEFAULT '',
  `ShortCode` varchar(10) NOT NULL DEFAULT '',
  `BoundaryPoints` mediumtext NOT NULL,
  `DefaultPlotCost` decimal(8,2) NOT NULL DEFAULT '0.00',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `MapIconScaleFactor` decimal(2,1) NOT NULL DEFAULT '0.0',
  PRIMARY KEY (`FacilitySectionID`),
  KEY `FacilityID` (`FacilityID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblFee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFee`;

CREATE TABLE `tblFee` (
  `FeeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilitySectionID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeName` varchar(50) NOT NULL DEFAULT '',
  `FeeType` enum('Deed','Cremation','Burial','Mason Work Permit','Mason Registration Permit','Memorial Stone Works','Memorial Garden Memorials','Memorial Books/Scrolls','Contract Merchandise','Contract Service','Plot','Interest','Exhumation','Credit','Cancellation','Discount') NOT NULL DEFAULT 'Deed',
  `FeeAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `TaxRateID` int(11) unsigned NOT NULL DEFAULT '0',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `NominalCode` varchar(50) NOT NULL DEFAULT '',
  `Quantity` int(11) NOT NULL DEFAULT '0',
  `CommissionPreNeed` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CommissionAtNeed` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CommissionAtFill` decimal(8,2) NOT NULL DEFAULT '0.00',
  `VariablePrice` tinyint(1) NOT NULL DEFAULT '0',
  `NominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `ConversionNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `TrustTypeID` int(11) unsigned NOT NULL DEFAULT '0',
  `CatalogItemCode` varchar(50) NOT NULL DEFAULT '',
  `Trusted` tinyint(1) NOT NULL DEFAULT '0',
  `RetainedIncome` decimal(8,2) NOT NULL DEFAULT '0.00',
  `DeferedRevenue` decimal(8,2) NOT NULL DEFAULT '0.00',
  `FeeAN` tinyint(1) NOT NULL DEFAULT '0',
  `FeePN` tinyint(1) NOT NULL DEFAULT '0',
  `NeedCategory` enum('Both','At-Need','Pre-Need') NOT NULL DEFAULT 'Both',
  `CostPrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CostPriceNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `IsHistoric` tinyint(1) NOT NULL DEFAULT '0',
  `PreNeedCostNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `InventoryNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `PreNeedInventoryNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `SystemOnly` tinyint(1) NOT NULL DEFAULT '0',
  `StockLevel` int(11) NOT NULL DEFAULT '0',
  `FeeCategoryID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeSubCategoryID` int(11) unsigned NOT NULL DEFAULT '0',
  `FundedRevenue` decimal(8,2) NOT NULL DEFAULT '0.00',
  `IsReceivedLiabilityExclusion` tinyint(1) NOT NULL DEFAULT '0',
  `IsFulfilledLiabilityExclusion` tinyint(1) NOT NULL DEFAULT '0',
  `InternalDescription` mediumtext NOT NULL,
  PRIMARY KEY (`FeeID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `FacilitySectionID` (`FacilitySectionID`),
  KEY `TaxRateID` (`TaxRateID`),
  KEY `NominalCodeID` (`NominalCodeID`),
  KEY `ConversionNominalCodeID` (`ConversionNominalCodeID`),
  KEY `TrustTypeID` (`TrustTypeID`),
  KEY `CostPriceNominalCodeID` (`CostPriceNominalCodeID`),
  KEY `PreNeedCostNominalCodeID` (`PreNeedCostNominalCodeID`),
  KEY `InventoryNominalCodeID` (`InventoryNominalCodeID`),
  KEY `PreNeedInventoryNominalCodeID` (`PreNeedInventoryNominalCodeID`),
  KEY `FeeCategoryID` (`FeeCategoryID`),
  KEY `FeeSubCategoryID` (`FeeSubCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblFee` WRITE;
/*!40000 ALTER TABLE `tblFee` DISABLE KEYS */;

INSERT INTO `tblFee` (`FeeID`, `FacilityID`, `FacilitySectionID`, `FeeName`, `FeeType`, `FeeAmount`, `TaxRateID`, `IsDeleted`, `NominalCode`, `Quantity`, `CommissionPreNeed`, `CommissionAtNeed`, `CommissionAtFill`, `VariablePrice`, `NominalCodeID`, `ConversionNominalCodeID`, `TrustTypeID`, `CatalogItemCode`, `Trusted`, `RetainedIncome`, `DeferedRevenue`, `FeeAN`, `FeePN`, `NeedCategory`, `CostPrice`, `CostPriceNominalCodeID`, `IsHistoric`, `PreNeedCostNominalCodeID`, `InventoryNominalCodeID`, `PreNeedInventoryNominalCodeID`, `SystemOnly`, `StockLevel`, `FeeCategoryID`, `FeeSubCategoryID`, `FundedRevenue`, `IsReceivedLiabilityExclusion`, `IsFulfilledLiabilityExclusion`, `InternalDescription`)
VALUES
	(1174,0,0,'Plot','Plot',0.00,21,0,'',0,0.00,0.00,0.00,0,0,0,0,'',0,0.00,0.00,0,0,'Both',0.00,0,0,0,0,0,0,0,0,0,0.00,0,0,''),
	(1175,0,0,'Interest','Interest',0.00,0,0,'',0,0.00,0.00,0.00,0,0,0,0,'',0,0.00,0.00,0,0,'Both',0.00,0,0,0,0,0,0,0,0,0,0.00,0,0,''),
	(1176,0,0,'Credit','Credit',0.00,21,0,'',0,0.00,0.00,0.00,0,0,0,0,'',0,0.00,0.00,0,0,'Both',0.00,0,0,0,0,0,0,0,0,0,0.00,0,0,'');

/*!40000 ALTER TABLE `tblFee` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblFeeCategory
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFeeCategory`;

CREATE TABLE `tblFeeCategory` (
  `FeeCategoryID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FeeType` varchar(50) NOT NULL DEFAULT '',
  `Name` varchar(150) NOT NULL DEFAULT '',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`FeeCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblFeeHistory
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFeeHistory`;

CREATE TABLE `tblFeeHistory` (
  `FeeHistoryID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FeeID` int(11) unsigned NOT NULL DEFAULT '0',
  `UserID` int(11) unsigned NOT NULL DEFAULT '0',
  `DateTimeChanged` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ChangesBefore` mediumtext NOT NULL,
  `ChangesAfter` mediumtext NOT NULL,
  PRIMARY KEY (`FeeHistoryID`),
  KEY `FeeID` (`FeeID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblFeeHistory` WRITE;
/*!40000 ALTER TABLE `tblFeeHistory` DISABLE KEYS */;

INSERT INTO `tblFeeHistory` (`FeeHistoryID`, `FeeID`, `UserID`, `DateTimeChanged`, `ChangesBefore`, `ChangesAfter`)
VALUES
	(1,1174,0,'2017-04-26 10:38:37','[]','{\"FeeName\":\"Plot\",\"FeeType\":\"Plot\",\"NeedCategory\":\"Both\"}'),
	(2,1175,0,'2017-04-26 10:38:37','[]','{\"FeeName\":\"Interest\",\"FeeType\":\"Interest\",\"NeedCategory\":\"Both\"}'),
	(3,1176,0,'2017-07-23 21:21:18','[]','{\"FeeName\":\"Credit\",\"FeeType\":\"Credit\",\"TaxRateID\":\"21\",\"NeedCategory\":\"Both\"}'),
	(4,1174,1,'2018-03-14 03:13:51','{\"FeeAmount\":\"0.00\",\"TaxRateID\":\"0\",\"FeeCategoryID\":\"0\",\"FeeSubCategoryID\":\"0\"}','{\"FeeAmount\":\"600\",\"TaxRateID\":\"21\",\"FeeCategoryID\":\"\",\"FeeSubCategoryID\":\"\"}'),
	(5,1177,1,'2018-03-14 03:15:01','[]','{\"FeeName\":\"Opening & Closing for Casket\",\"FeeType\":\"Burial\",\"FeeAmount\":\"600\",\"TaxRateID\":\"21\",\"CommissionPreNeed\":\"0.00\",\"CommissionAtNeed\":\"0.00\",\"CommissionAtFill\":\"0.00\",\"RetainedIncome\":\"0.00\",\"DeferedRevenue\":\"0.00\",\"FundedRevenue\":\"0.00\",\"NeedCategory\":\"Both\",\"CostPrice\":\"0.00\"}'),
	(6,1178,1,'2018-03-14 03:16:18','[]','{\"FeeName\":\"Opening & Closing for Cremation\",\"FeeType\":\"Burial\",\"FeeAmount\":\"300\",\"TaxRateID\":\"21\",\"CommissionPreNeed\":\"0.00\",\"CommissionAtNeed\":\"0.00\",\"CommissionAtFill\":\"0.00\",\"RetainedIncome\":\"0.00\",\"DeferedRevenue\":\"0.00\",\"FundedRevenue\":\"0.00\",\"NeedCategory\":\"Both\",\"CostPrice\":\"0.00\"}'),
	(7,1178,1,'2018-06-15 11:40:58','{\"FeeAmount\":\"300.00\",\"FeeCategoryID\":\"0\",\"FeeSubCategoryID\":\"0\"}','{\"FeeAmount\":\"400.00\",\"FeeCategoryID\":\"\",\"FeeSubCategoryID\":\"\"}'),
	(8,1174,1,'2018-06-15 11:41:58','{\"FeeAmount\":\"600.00\",\"FeeCategoryID\":\"0\",\"FeeSubCategoryID\":\"0\"}','{\"FeeAmount\":\"0.00\",\"FeeCategoryID\":\"\",\"FeeSubCategoryID\":\"\"}'),
	(9,1179,1,'2018-06-15 11:42:48','[]','{\"FeeName\":\"Grave Purchase \",\"FeeType\":\"Deed\",\"FeeAmount\":\"600.00\",\"TaxRateID\":\"21\",\"CommissionPreNeed\":\"0.00\",\"CommissionAtNeed\":\"0.00\",\"CommissionAtFill\":\"0.00\",\"RetainedIncome\":\"0.00\",\"DeferedRevenue\":\"0.00\",\"FundedRevenue\":\"0.00\",\"NeedCategory\":\"Both\",\"CostPrice\":\"0.00\"}');

/*!40000 ALTER TABLE `tblFeeHistory` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblFeeLocation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFeeLocation`;

CREATE TABLE `tblFeeLocation` (
  `LocationFeeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`LocationFeeID`),
  KEY `PlotID` (`PlotID`),
  KEY `FeeID` (`FeeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblFeePackage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFeePackage`;

CREATE TABLE `tblFeePackage` (
  `FeePackageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FeePackageName` varchar(50) NOT NULL DEFAULT '',
  `FeeIds` varchar(100) NOT NULL DEFAULT '',
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilitySectionID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeType` enum('Contract Merchandise','Contract Service') NOT NULL DEFAULT 'Contract Merchandise',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`FeePackageID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `FacilitySectionID` (`FacilitySectionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblFeePackageQuantity
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFeePackageQuantity`;

CREATE TABLE `tblFeePackageQuantity` (
  `FeePackageQuantityID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeePackageID` int(11) unsigned NOT NULL DEFAULT '0',
  `Quantity` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`FeePackageQuantityID`),
  KEY `ContractID` (`ContractID`),
  KEY `FeePackageID` (`FeePackageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblFeeSection
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFeeSection`;

CREATE TABLE `tblFeeSection` (
  `FeeSectionID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FeeID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilitySectionID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`FeeSectionID`),
  KEY `FeeID` (`FeeID`),
  KEY `FacilitySectionID` (`FacilitySectionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblFeeSubCategory
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFeeSubCategory`;

CREATE TABLE `tblFeeSubCategory` (
  `FeeSubCategoryID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(150) NOT NULL DEFAULT '',
  `FeeCategoryID` int(11) unsigned NOT NULL DEFAULT '0',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`FeeSubCategoryID`),
  KEY `FeeCategoryID` (`FeeCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblFinanceExportTypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFinanceExportTypes`;

CREATE TABLE `tblFinanceExportTypes` (
  `FinanceExportID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FinanceType` varchar(100) NOT NULL DEFAULT '',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`FinanceExportID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblFormType
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFormType`;

CREATE TABLE `tblFormType` (
  `FormTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FormTypeName` varchar(50) NOT NULL DEFAULT '',
  `Tags` enum('Burial','Cremation','Deed','Mason','Mason Work','Funeral Director','Burial Completion','Declaration','Exhumation','Memorial','Contract','Work Order') NOT NULL DEFAULT 'Burial',
  PRIMARY KEY (`FormTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblFormType` WRITE;
/*!40000 ALTER TABLE `tblFormType` DISABLE KEYS */;

INSERT INTO `tblFormType` (`FormTypeID`, `FormTypeName`, `Tags`)
VALUES
	(1,'Bereavement Schedule','Burial'),
	(2,'Notice of interment','Burial'),
	(3,'Statutory declaration','Burial'),
	(4,'Indemnity Form','Burial'),
	(5,'Other Burial Doc','Burial'),
	(6,'Deed document a','Deed'),
	(7,'Signed T&Cs','Mason'),
	(8,'Certificates','Mason'),
	(9,'Mason Drawing','Mason Work'),
	(10,'References','Mason'),
	(11,'Insurance Docs','Funeral Director'),
	(12,' Other','Funeral Director'),
	(13,'Burial Order','Burial Completion'),
	(14,'Other','Burial Completion'),
	(15,'Deed Register Image','Deed'),
	(16,'Other Cremation Doc','Cremation'),
	(17,'Declaration','Declaration'),
	(18,'Memorial document','Memorial'),
	(19,'Contract Related Doc','Contract'),
	(20,'Work Order Related Doc','Work Order');

/*!40000 ALTER TABLE `tblFormType` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblFuneralDirector
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFuneralDirector`;

CREATE TABLE `tblFuneralDirector` (
  `FuneralDirectorID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `FuneralDirectorName` varchar(100) NOT NULL DEFAULT '',
  `EmailAddress` varchar(50) NOT NULL DEFAULT '',
  `PhoneNumber` varchar(50) NOT NULL DEFAULT '',
  `AddressLine1` varchar(50) NOT NULL DEFAULT '',
  `AddressLine2` varchar(50) NOT NULL DEFAULT '',
  `Town` varchar(50) NOT NULL DEFAULT '',
  `State` varchar(50) NOT NULL DEFAULT '',
  `Active` tinyint(1) NOT NULL DEFAULT '1',
  `Postcode` varchar(20) NOT NULL DEFAULT '',
  `ProductLiabilityRequiredValue` tinyint(1) NOT NULL DEFAULT '0',
  `PublicLiabilityRequiredValue` tinyint(1) NOT NULL DEFAULT '0',
  `EmployerLiabilityRequiredValue` tinyint(1) NOT NULL DEFAULT '0',
  `InsuranceRenewalDate` date NOT NULL DEFAULT '0000-00-00',
  `UserID` int(11) unsigned NOT NULL DEFAULT '0',
  `CustomerNumber` varchar(50) NOT NULL DEFAULT '',
  `DateRegistered` date NOT NULL DEFAULT '0000-00-00',
  `InsuranceCompany` varchar(50) NOT NULL DEFAULT '',
  `PolicyNumber` varchar(50) NOT NULL DEFAULT '',
  `Indemnification` tinyint(1) NOT NULL DEFAULT '0',
  `InsuranceRenewalNotified` tinyint(1) NOT NULL DEFAULT '0',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `FuneralDirectorNumber` varchar(50) NOT NULL DEFAULT '',
  `EmbalmingLicenceNumber` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`FuneralDirectorID`),
  KEY `ContactID` (`ContactID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblFuneralDirectorContact
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFuneralDirectorContact`;

CREATE TABLE `tblFuneralDirectorContact` (
  `Title` varchar(20) NOT NULL DEFAULT '',
  `Forename` varchar(100) NOT NULL DEFAULT '',
  `MiddleName` varchar(100) NOT NULL DEFAULT '',
  `Surname` varchar(100) NOT NULL DEFAULT '',
  `FullName` varchar(350) NOT NULL DEFAULT '',
  `AddressLine1` varchar(100) NOT NULL DEFAULT '',
  `AddressLine2` varchar(100) NOT NULL DEFAULT '',
  `Town` varchar(100) NOT NULL DEFAULT '',
  `County` varchar(100) NOT NULL DEFAULT '',
  `Country` varchar(100) NOT NULL DEFAULT '',
  `Postcode` varchar(12) NOT NULL DEFAULT '',
  `Mobile` varchar(50) NOT NULL DEFAULT '',
  `Landline` varchar(50) NOT NULL DEFAULT '',
  `EmailAddress` varchar(100) NOT NULL DEFAULT '',
  `Gender` enum('Unknown','Male','Female') NOT NULL DEFAULT 'Unknown',
  `DateOfBirth` date NOT NULL DEFAULT '0000-00-00',
  `Deceased` tinyint(1) NOT NULL DEFAULT '0',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `Notes` varchar(100) NOT NULL DEFAULT '',
  `FuneralDirectorContactID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FuneralDirectorID` int(11) unsigned NOT NULL DEFAULT '0',
  `CustomerNumber` varchar(50) NOT NULL DEFAULT '',
  `MasterContactID` int(11) DEFAULT NULL,
  PRIMARY KEY (`FuneralDirectorContactID`),
  KEY `Title` (`Title`),
  KEY `Forename` (`Forename`),
  KEY `MiddleName` (`MiddleName`),
  KEY `Surname` (`Surname`),
  KEY `FuneralDirectorID` (`FuneralDirectorID`),
  KEY `CustomerNumber` (`CustomerNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblFuneralDirectorDocument
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFuneralDirectorDocument`;

CREATE TABLE `tblFuneralDirectorDocument` (
  `FuneralDirectorDocumentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FuneralDirectorID` int(11) DEFAULT NULL,
  `DocumentName` varchar(255) NOT NULL DEFAULT '',
  `DocumentType` varchar(255) NOT NULL DEFAULT '',
  `FormTypeID` int(11) DEFAULT NULL,
  PRIMARY KEY (`FuneralDirectorDocumentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblGenealogy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblGenealogy`;

CREATE TABLE `tblGenealogy` (
  `GenealogyID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotSpaceID` int(11) unsigned NOT NULL DEFAULT '0',
  `SiteID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `Parent1ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `Parent2ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `PrimaryImageID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `DeedID` int(11) DEFAULT NULL,
  `PlotPurchaseID` int(11) DEFAULT NULL,
  `KnownAs` varchar(100) NOT NULL DEFAULT '',
  `ReligionDenominationID` int(11) unsigned NOT NULL DEFAULT '0',
  `MaritalStatus` varchar(50) NOT NULL DEFAULT '',
  `ChildOf` varchar(50) NOT NULL DEFAULT '',
  `GenealogyTitle` varchar(10) NOT NULL DEFAULT '',
  `GenealogyForename` varchar(50) NOT NULL DEFAULT '',
  `GenealogyMiddleNames` varchar(50) NOT NULL DEFAULT '',
  `GenealogySurname` varchar(50) NOT NULL DEFAULT '',
  `Sex` enum('Unknown','Male','Female') NOT NULL DEFAULT 'Unknown',
  `AgeAtDeath` int(11) DEFAULT NULL,
  `AgeAtDeathDays` int(11) DEFAULT NULL,
  `AgeAtDeathWeeks` int(11) DEFAULT NULL,
  `AgeAtDeathMonths` int(11) DEFAULT NULL,
  `AgeAtDeathHours` int(11) DEFAULT NULL,
  `DeceasedDate` date NOT NULL DEFAULT '0000-00-00',
  `BirthDate` date NOT NULL DEFAULT '0000-00-00',
  `Occupation` varchar(100) NOT NULL DEFAULT '',
  `CauseOfDeath` varchar(100) NOT NULL DEFAULT '',
  `PlaceOfDeath` varchar(50) NOT NULL DEFAULT '',
  `Notes` mediumtext NOT NULL,
  `LastResidence` varchar(250) NOT NULL DEFAULT '',
  `PersonManagingFuneral` varchar(50) NOT NULL DEFAULT '',
  `NameOfRegistrar` varchar(50) NOT NULL DEFAULT '',
  `Resident` tinyint(1) NOT NULL DEFAULT '0',
  `FuneralType` enum('Burial','Cremation') NOT NULL DEFAULT 'Cremation',
  `PrivateRecord` tinyint(1) NOT NULL DEFAULT '0',
  `BurialDate` date NOT NULL DEFAULT '0000-00-00',
  `SecondBurialDate` date NOT NULL DEFAULT '0000-00-00',
  `BurialTime` time NOT NULL DEFAULT '00:00:00',
  `BurialEndTime` time NOT NULL DEFAULT '00:00:00',
  `Source` enum('Manual','Event','Import') NOT NULL DEFAULT 'Event',
  `BurialType` varchar(50) NOT NULL DEFAULT '',
  `DepthRemainingString` varchar(50) NOT NULL DEFAULT '',
  `Veteran` tinyint(1) NOT NULL DEFAULT '0',
  `VeteranBranch` varchar(50) NOT NULL DEFAULT '',
  `IsProblematic` tinyint(1) NOT NULL DEFAULT '0',
  `Officiant` varchar(128) NOT NULL DEFAULT '',
  `RegistrationDate` date NOT NULL DEFAULT '0000-00-00',
  `RegistrationCertNumber` varchar(50) NOT NULL DEFAULT '',
  `RegistrationDistrictID` int(11) unsigned NOT NULL DEFAULT '0',
  `IsTranscribed` tinyint(1) NOT NULL DEFAULT '0',
  `MilitaryTranscription` varchar(50) NOT NULL DEFAULT '',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `Exhumed` tinyint(1) NOT NULL DEFAULT '0',
  `BirthDay` int(11) DEFAULT NULL,
  `BirthMonth` int(11) DEFAULT NULL,
  `BirthYear` int(11) DEFAULT NULL,
  `DeathDay` int(11) DEFAULT NULL,
  `DeathMonth` int(11) DEFAULT NULL,
  `DeathYear` int(11) DEFAULT NULL,
  `MedicalRefereeID` int(11) DEFAULT NULL,
  `GenealogyMaidenName` varchar(50) NOT NULL DEFAULT '',
  `PersonOfInterest` tinyint(1) NOT NULL DEFAULT '0',
  `PersonOfInterestBlurb` varchar(1000) NOT NULL DEFAULT '',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `VeteranActiveDutyDeath` tinyint(1) NOT NULL DEFAULT '0',
  `VeteranYearsOfService` varchar(50) NOT NULL DEFAULT '',
  `VeteranMilitaryRank` varchar(50) NOT NULL DEFAULT '',
  `VeteranNotes` mediumtext NOT NULL,
  `PublicGenealogyNotes` mediumtext NOT NULL,
  `IsGestationAge` tinyint(1) NOT NULL DEFAULT '0',
  `MedicalContact1ID` int(11) DEFAULT NULL,
  `MedicalContact2ID` int(11) DEFAULT NULL,
  `MusicRequired` tinyint(1) NOT NULL DEFAULT '0',
  `MusicIn` varchar(100) NOT NULL DEFAULT '',
  `MusicOut` varchar(100) NOT NULL DEFAULT '',
  `CremationNotes` text NOT NULL,
  `RemainsDisposal` enum('','Scattered','Scattered (Witnessed)','Buried','Collected','Memorial','Niche','Removed') NOT NULL DEFAULT '',
  `ScatterLocation` varchar(100) NOT NULL DEFAULT '',
  `AshesCollectionDate` date NOT NULL DEFAULT '0000-00-00',
  `AshesCollectionType` enum('','Transit Box','Polytainer','Casket') NOT NULL DEFAULT '',
  `AshesCollectedBy` varchar(100) NOT NULL DEFAULT '',
  `RemainsDisposalDetails` mediumtext NOT NULL,
  `RetentionOfMetals` tinyint(1) NOT NULL DEFAULT '0',
  `MusicType` varchar(100) NOT NULL DEFAULT '',
  `MusicalInstructionsOther` mediumtext NOT NULL,
  `MarketingOptedIn` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`GenealogyID`),
  KEY `PlotID` (`PlotID`),
  KEY `PlotSpaceID` (`PlotSpaceID`),
  KEY `SiteID` (`SiteID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `Parent1ContactID` (`Parent1ContactID`),
  KEY `Parent2ContactID` (`Parent2ContactID`),
  KEY `PrimaryImageID` (`PrimaryImageID`),
  KEY `ContactID` (`ContactID`),
  KEY `ReligionDenominationID` (`ReligionDenominationID`),
  KEY `RegistrationDistrictID` (`RegistrationDistrictID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblGenealogyImage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblGenealogyImage`;

CREATE TABLE `tblGenealogyImage` (
  `GenealogyImageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `GenealogyID` int(11) unsigned NOT NULL DEFAULT '0',
  `UploadedDate` date NOT NULL DEFAULT '0000-00-00',
  `ImageName` varchar(255) NOT NULL DEFAULT '',
  `Type` enum('Other','Register Image','Headstone Image') NOT NULL DEFAULT 'Other',
  `Order` int(11) DEFAULT NULL,
  PRIMARY KEY (`GenealogyImageID`),
  KEY `GenealogyID` (`GenealogyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblGenealogyTourOfDuty
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblGenealogyTourOfDuty`;

CREATE TABLE `tblGenealogyTourOfDuty` (
  `GenealogyTourOfDutyID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `EventID` int(11) unsigned NOT NULL DEFAULT '0',
  `GenealogyID` int(11) unsigned NOT NULL DEFAULT '0',
  `TourOfDuty` varchar(50) NOT NULL DEFAULT '',
  `StartDate` date NOT NULL DEFAULT '0000-00-00',
  `EndDate` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`GenealogyTourOfDutyID`),
  KEY `EventID` (`EventID`),
  KEY `GenealogyID` (`GenealogyID`),
  KEY `TourOfDuty` (`TourOfDuty`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblGLTransactionLog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblGLTransactionLog`;

CREATE TABLE `tblGLTransactionLog` (
  `GLTransactionLogID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `TransactionDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TransactionType` enum('Sale','Payment','Cancellation') NOT NULL DEFAULT 'Sale',
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `PaymentID` int(11) unsigned NOT NULL DEFAULT '0',
  `TransactionModelID` int(11) unsigned NOT NULL DEFAULT '0',
  `TransactionModel` enum('Contract','Invoice','Payment') NOT NULL DEFAULT 'Contract',
  `FeeType` enum('','DeedFee','ContractFee','CancelledContractFee') NOT NULL DEFAULT '',
  `FeeItemID` int(11) unsigned NOT NULL DEFAULT '0',
  `NominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `NominalCode` varchar(255) NOT NULL DEFAULT '',
  `NominalCodeDescription` varchar(255) NOT NULL DEFAULT '',
  `AccountType` enum('Asset','Liability','Revenue','Cost of Sales','Equity','Expense') NOT NULL DEFAULT 'Revenue',
  `Debit` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Credit` decimal(8,2) NOT NULL DEFAULT '0.00',
  `TotallingAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CancelledContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`GLTransactionLogID`),
  KEY `ContractID` (`ContractID`),
  KEY `PaymentID` (`PaymentID`),
  KEY `TransactionModelID` (`TransactionModelID`),
  KEY `FeeItemID` (`FeeItemID`),
  KEY `NominalCodeID` (`NominalCodeID`),
  KEY `CancelledContractID` (`CancelledContractID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblHoldPlot
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblHoldPlot`;

CREATE TABLE `tblHoldPlot` (
  `HoldPlotID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `HoldFor` int(11) NOT NULL DEFAULT '0',
  `StartDate` date NOT NULL DEFAULT '0000-00-00',
  `EndDate` date NOT NULL DEFAULT '0000-00-00',
  `CreatedBy` int(11) DEFAULT NULL,
  `HoldReason` mediumtext NOT NULL,
  `ReleaseDate` date NOT NULL DEFAULT '0000-00-00',
  `ReleasedBy` int(11) DEFAULT NULL,
  `ReleasedReason` mediumtext NOT NULL,
  `ReleasedCode` enum('Auto-expire','Manual','Purchased') NOT NULL DEFAULT 'Auto-expire',
  `Active` tinyint(1) NOT NULL DEFAULT '0',
  `PlotReleased` tinyint(1) NOT NULL DEFAULT '0',
  `HoldType` enum('Temporary Hold','Permanent Hold') NOT NULL DEFAULT 'Temporary Hold',
  PRIMARY KEY (`HoldPlotID`),
  KEY `PlotID` (`PlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblImmersiveView
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblImmersiveView`;

CREATE TABLE `tblImmersiveView` (
  `ImmersiveViewID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilitySectionID` int(11) unsigned NOT NULL DEFAULT '0',
  `VideoID` varchar(300) NOT NULL DEFAULT '',
  `Latitude` decimal(11,8) NOT NULL DEFAULT '0.00000000',
  `Longitude` decimal(11,8) NOT NULL DEFAULT '0.00000000',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ImmersiveViewID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `FacilitySectionID` (`FacilitySectionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblIntegration
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblIntegration`;

CREATE TABLE `tblIntegration` (
  `IntegrationID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IntegrationName` varchar(100) NOT NULL DEFAULT '',
  `ClientID` varchar(40) NOT NULL DEFAULT '',
  `ClientSecret` varchar(100) NOT NULL DEFAULT '',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IntegrationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblInvoice
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblInvoice`;

CREATE TABLE `tblInvoice` (
  `InvoiceID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Status` enum('Draft','Created','Sent','Partially Paid','Paid') NOT NULL DEFAULT 'Draft',
  `BurialID` int(11) unsigned NOT NULL DEFAULT '0',
  `DeedID` int(11) unsigned NOT NULL DEFAULT '0',
  `MemorialID` int(11) unsigned NOT NULL DEFAULT '0',
  `CremationID` int(11) unsigned NOT NULL DEFAULT '0',
  `FuneralDirectorID` int(11) unsigned NOT NULL DEFAULT '0',
  `MemorialMasonID` int(11) unsigned NOT NULL DEFAULT '0',
  `SiteID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceRefID` int(11) unsigned NOT NULL DEFAULT '0',
  `PayerContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceRefClass` varchar(250) NOT NULL DEFAULT '',
  `IsContract` tinyint(1) NOT NULL DEFAULT '0',
  `ContactSource` enum('','Funeral Director','Burial Applicant','Cremation Applicant','Deed Holder','Memorial Holder','Memorial Mason','Contract Owner','Exhumation Applicant','Payer') NOT NULL DEFAULT '',
  `Amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `TaxAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `AmountOutstanding` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CreatedDate` date NOT NULL DEFAULT '0000-00-00',
  `ExhumationID` int(11) unsigned NOT NULL DEFAULT '0',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `TransactionID` int(11) unsigned NOT NULL DEFAULT '0',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`InvoiceID`),
  KEY `BurialID` (`BurialID`),
  KEY `DeedID` (`DeedID`),
  KEY `MemorialID` (`MemorialID`),
  KEY `CremationID` (`CremationID`),
  KEY `FuneralDirectorID` (`FuneralDirectorID`),
  KEY `MemorialMasonID` (`MemorialMasonID`),
  KEY `SiteID` (`SiteID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `InvoiceRefID` (`InvoiceRefID`),
  KEY `PayerContactID` (`PayerContactID`),
  KEY `ExhumationID` (`ExhumationID`),
  KEY `TransactionID` (`TransactionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblInvoiceExport
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblInvoiceExport`;

CREATE TABLE `tblInvoiceExport` (
  `InvoiceExportID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ExportDate` date NOT NULL DEFAULT '0000-00-00',
  `ExportFormat` enum('','AGRES','QUICK','CSV') NOT NULL DEFAULT '',
  `InvoiceID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`InvoiceExportID`),
  KEY `InvoiceID` (`InvoiceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblInvoiceItem
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblInvoiceItem`;

CREATE TABLE `tblInvoiceItem` (
  `InvoiceItemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `InvoiceID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoicedModelID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoicedModelType` varchar(250) NOT NULL DEFAULT '',
  `Description` varchar(250) NOT NULL DEFAULT '',
  `BurialID` int(11) NOT NULL DEFAULT '0',
  `CremationID` int(11) NOT NULL DEFAULT '0',
  `FuneralDirectorID` int(11) NOT NULL DEFAULT '0',
  `DeedID` int(11) NOT NULL DEFAULT '0',
  `MemorialMasonID` int(11) NOT NULL DEFAULT '0',
  `MemorialID` int(11) NOT NULL DEFAULT '0',
  `ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContactSource` enum('','Funeral Director','Burial Applicant','Cremation Applicant','Deed Holder','Memorial Holder','Memorial Mason','Contract Owner','Exhumation Applicant','Payer') NOT NULL DEFAULT '',
  `EventType` varchar(100) NOT NULL DEFAULT '',
  `Amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Tax` decimal(8,2) NOT NULL DEFAULT '0.00',
  `TaxRate` decimal(6,3) NOT NULL DEFAULT '0.000',
  `CreatedDate` date NOT NULL DEFAULT '0000-00-00',
  `NominalCode` varchar(50) NOT NULL DEFAULT '',
  `ExhumationID` int(11) unsigned NOT NULL DEFAULT '0',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`InvoiceItemID`),
  KEY `InvoiceID` (`InvoiceID`),
  KEY `InvoicedModelID` (`InvoicedModelID`),
  KEY `FeeID` (`FeeID`),
  KEY `ContactID` (`ContactID`),
  KEY `ExhumationID` (`ExhumationID`),
  KEY `ContactSource` (`ContactSource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblInvoiceItemsExport
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblInvoiceItemsExport`;

CREATE TABLE `tblInvoiceItemsExport` (
  `InvoiceItemExportID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `InvoiceItemID` int(11) unsigned NOT NULL DEFAULT '0',
  `ExportDate` date NOT NULL DEFAULT '0000-00-00',
  `ExportFormat` enum('','AGRES','QUICK','CSV') NOT NULL DEFAULT '',
  PRIMARY KEY (`InvoiceItemExportID`),
  KEY `InvoiceItemID` (`InvoiceItemID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblLanguage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblLanguage`;

CREATE TABLE `tblLanguage` (
  `LanguageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FieldName` varchar(400) NOT NULL DEFAULT '',
  `LocaleName` varchar(50) NOT NULL DEFAULT 'Default',
  `LocalisedValue` varchar(400) NOT NULL DEFAULT '',
  PRIMARY KEY (`LanguageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblLanguage` WRITE;
/*!40000 ALTER TABLE `tblLanguage` DISABLE KEYS */;

INSERT INTO `tblLanguage` (`LanguageID`, `FieldName`, `LocaleName`, `LocalisedValue`)
VALUES
	(1,'DefaultCountry','Default','en-US'),
	(2,'County','en-US','State'),
	(3,'Postcode','en-US','Zipcode'),
	(4,'Town','en-US','City'),
	(5,'Mobile','en-US','Cellphone'),
	(6,'Cheque','en-US','Check'),
	(7,'DefaultCountry','en-US','USA');

/*!40000 ALTER TABLE `tblLanguage` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblLegacyDataImport
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblLegacyDataImport`;

CREATE TABLE `tblLegacyDataImport` (
  `LegacyDataImportID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ExtPartyIdentiferName` varchar(50) NOT NULL DEFAULT '',
  `ExtPartyIdentifer` varchar(50) NOT NULL DEFAULT '',
  `ExtPlotIdentiferName` varchar(50) NOT NULL DEFAULT '',
  `ExtPlotIdentifer` varchar(50) NOT NULL DEFAULT '',
  `ExtOwnerIdentiferName` varchar(50) NOT NULL DEFAULT '',
  `ExtOwnerIdentifer` varchar(50) NOT NULL DEFAULT '',
  `ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `ImportType` enum('Undefined','Burial','Contract','Owner','Plot','Contact','Deed') NOT NULL DEFAULT 'Undefined',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ExtContractIdentifierName` varchar(50) NOT NULL DEFAULT '',
  `ExtContractIdentifier` varchar(50) NOT NULL DEFAULT '',
  `DeedID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`LegacyDataImportID`),
  KEY `ContactID` (`ContactID`),
  KEY `PlotID` (`PlotID`),
  KEY `ExtPartyIdentiferName_Index` (`ExtPartyIdentiferName`),
  KEY `ExtPartyIdentifer_Index` (`ExtPartyIdentifer`),
  KEY `ExtPlotIdentiferName_Index` (`ExtPlotIdentiferName`),
  KEY `ExtPlotIdentifer_Index` (`ExtPlotIdentifer`),
  KEY `ExtOwnerIdentiferName_Index` (`ExtOwnerIdentiferName`),
  KEY `ExtOwnerIdentifer_Index` (`ExtOwnerIdentifer`),
  KEY `DeedID` (`DeedID`),
  KEY `ContractID` (`ContractID`),
  KEY `ExtContractIdentifierName_Index` (`ExtContractIdentifierName`),
  KEY `ExtContractIdentifier_Index` (`ExtContractIdentifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblLinkedDocument
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblLinkedDocument`;

CREATE TABLE `tblLinkedDocument` (
  `LinkedDocumentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Hash` varchar(40) NOT NULL DEFAULT '',
  `Name` varchar(255) NOT NULL DEFAULT '',
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilitySectionID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotRow` varchar(50) DEFAULT NULL,
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`LinkedDocumentID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `FacilitySectionID` (`FacilitySectionID`),
  KEY `PlotID` (`PlotID`),
  KEY `ContactID` (`ContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblLog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblLog`;

CREATE TABLE `tblLog` (
  `LogID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DateUpdated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `UniqueID` int(11) DEFAULT NULL,
  `ClassName` mediumtext NOT NULL,
  `UserID` int(11) DEFAULT NULL,
  `BeforeDifferences` mediumtext NOT NULL,
  `AfterDifferences` mediumtext NOT NULL,
  `ChangeType` enum('Created','Updated','Deleted') NOT NULL DEFAULT 'Updated',
  PRIMARY KEY (`LogID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblLookup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblLookup`;

CREATE TABLE `tblLookup` (
  `LookupID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Key` varchar(100) NOT NULL DEFAULT '',
  `Value` varchar(200) NOT NULL DEFAULT '',
  `Order` int(11) DEFAULT NULL,
  `GroupKey` varchar(100) NOT NULL DEFAULT '',
  `EffectiveFrom` date NOT NULL DEFAULT '0000-00-00',
  `CloseDate` date NOT NULL DEFAULT '0000-00-00',
  `CreatedDate` date NOT NULL DEFAULT '0000-00-00',
  `LastUpdatedDate` date NOT NULL DEFAULT '0000-00-00',
  `Active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`LookupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblMailLog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMailLog`;

CREATE TABLE `tblMailLog` (
  `LogID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MessageData` mediumtext NOT NULL,
  `ReturnValue` mediumtext NOT NULL,
  `CurlError` mediumtext NOT NULL,
  `HttpCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`LogID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblMedicalContact
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMedicalContact`;

CREATE TABLE `tblMedicalContact` (
  `Title` varchar(20) NOT NULL DEFAULT '',
  `Forename` varchar(100) NOT NULL DEFAULT '',
  `MiddleName` varchar(100) NOT NULL DEFAULT '',
  `Surname` varchar(100) NOT NULL DEFAULT '',
  `FullName` varchar(350) NOT NULL DEFAULT '',
  `AddressLine1` varchar(100) NOT NULL DEFAULT '',
  `AddressLine2` varchar(100) NOT NULL DEFAULT '',
  `Town` varchar(100) NOT NULL DEFAULT '',
  `County` varchar(100) NOT NULL DEFAULT '',
  `Country` varchar(100) NOT NULL DEFAULT '',
  `Postcode` varchar(12) NOT NULL DEFAULT '',
  `Mobile` varchar(50) NOT NULL DEFAULT '',
  `Landline` varchar(50) NOT NULL DEFAULT '',
  `EmailAddress` varchar(100) NOT NULL DEFAULT '',
  `Gender` enum('Unknown','Male','Female') NOT NULL DEFAULT 'Unknown',
  `DateOfBirth` date NOT NULL DEFAULT '0000-00-00',
  `Deceased` tinyint(1) NOT NULL DEFAULT '0',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `Notes` varchar(100) NOT NULL DEFAULT '',
  `MedicalContactID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MedicalContactType` enum('Medical Referee','Coroner','Midwife','Doctor') NOT NULL DEFAULT 'Medical Referee',
  `RegistrationNumber` varchar(50) NOT NULL DEFAULT '',
  `IsHistoric` tinyint(1) NOT NULL DEFAULT '0',
  `HospitalName` varchar(100) NOT NULL DEFAULT '',
  `CustomerNumber` varchar(50) NOT NULL DEFAULT '',
  `MasterContactID` int(11) DEFAULT NULL,
  PRIMARY KEY (`MedicalContactID`),
  KEY `Title` (`Title`),
  KEY `Forename` (`Forename`),
  KEY `MiddleName` (`MiddleName`),
  KEY `Surname` (`Surname`),
  KEY `CustomerNumber` (`CustomerNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblMemorial
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMemorial`;

CREATE TABLE `tblMemorial` (
  `MemorialID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MemorialType` varchar(50) NOT NULL DEFAULT '',
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `SiteID` int(11) unsigned NOT NULL DEFAULT '0',
  `OwnerContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `MemorialDeedNumber` varchar(50) NOT NULL DEFAULT '',
  `MemorialPurchaseDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LeaseStartDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LeaseExpiryDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LeaseLengthYears` int(11) DEFAULT NULL,
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `Notes` mediumtext NOT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `DeletedBy` int(11) unsigned NOT NULL DEFAULT '0',
  `DeletedDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`MemorialID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `SiteID` (`SiteID`),
  KEY `OwnerContactID` (`OwnerContactID`),
  KEY `PlotID` (`PlotID`),
  KEY `DeletedBy` (`DeletedBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblMemorialDocument
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMemorialDocument`;

CREATE TABLE `tblMemorialDocument` (
  `MemorialDocumentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MemorialID` int(11) unsigned NOT NULL DEFAULT '0',
  `DocumentName` varchar(255) NOT NULL DEFAULT '',
  `FormTypeID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`MemorialDocumentID`),
  KEY `MemorialID` (`MemorialID`),
  KEY `FormTypeID` (`FormTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblMemorialFee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMemorialFee`;

CREATE TABLE `tblMemorialFee` (
  `MemorialFeeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MemorialFeeName` varchar(50) NOT NULL DEFAULT '',
  `MemorialLeaseID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceItemID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceRefID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceRefClass` varchar(250) NOT NULL DEFAULT '',
  `TaxRate` decimal(6,3) NOT NULL DEFAULT '0.000',
  `TaxAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `PriceIncTax` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Quantity` int(11) NOT NULL DEFAULT '0',
  `Total` decimal(8,2) NOT NULL DEFAULT '0.00',
  `SalesNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `CostNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `InventoryNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `SalesTaxNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeType` varchar(250) NOT NULL DEFAULT '',
  `UnitDiscount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `UnitCostPrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`MemorialFeeID`),
  KEY `MemorialLeaseID` (`MemorialLeaseID`),
  KEY `FeeID` (`FeeID`),
  KEY `InvoiceItemID` (`InvoiceItemID`),
  KEY `InvoiceRefID` (`InvoiceRefID`),
  KEY `SalesNominalCodeID` (`SalesNominalCodeID`),
  KEY `CostNominalCodeID` (`CostNominalCodeID`),
  KEY `InventoryNominalCodeID` (`InventoryNominalCodeID`),
  KEY `SalesTaxNominalCodeID` (`SalesTaxNominalCodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblMemorialGenealogy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMemorialGenealogy`;

CREATE TABLE `tblMemorialGenealogy` (
  `MemorialGenealogyID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MemorialGenealogyName` varchar(50) NOT NULL DEFAULT '',
  `MemorialID` int(11) unsigned NOT NULL DEFAULT '0',
  `GenealogyID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`MemorialGenealogyID`),
  KEY `MemorialID` (`MemorialID`),
  KEY `GenealogyID` (`GenealogyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblMemorialItem
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMemorialItem`;

CREATE TABLE `tblMemorialItem` (
  `MemorialItemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MemorialID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeType` varchar(250) NOT NULL DEFAULT '',
  `Description` varchar(255) NOT NULL DEFAULT '',
  `CreatedDate` date NOT NULL DEFAULT '0000-00-00',
  `UnitDiscount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `TaxRate` decimal(6,3) NOT NULL DEFAULT '0.000',
  `Tax` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Funded` tinyint(1) NOT NULL DEFAULT '0',
  `IsStockUpdated` tinyint(1) NOT NULL DEFAULT '0',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `DeletedBy` int(11) unsigned NOT NULL DEFAULT '0',
  `DeletedDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`MemorialItemID`),
  KEY `MemorialID` (`MemorialID`),
  KEY `FeeID` (`FeeID`),
  KEY `DeletedBy` (`DeletedBy`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblMemorialItemDetail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMemorialItemDetail`;

CREATE TABLE `tblMemorialItemDetail` (
  `MemorialItemDetailID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MemorialItemID` int(11) unsigned NOT NULL DEFAULT '0',
  `InscriptionText` mediumtext NOT NULL,
  `Precedence` int(11) DEFAULT NULL,
  `ManufactureDate` date NOT NULL DEFAULT '0000-00-00',
  `ReceivedDate` date NOT NULL DEFAULT '0000-00-00',
  `InstallationDate` date NOT NULL DEFAULT '0000-00-00',
  `NotificationDate` date NOT NULL DEFAULT '0000-00-00',
  `CreatedDate` date NOT NULL DEFAULT '0000-00-00',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `DeletedBy` int(11) unsigned NOT NULL DEFAULT '0',
  `DeletedDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`MemorialItemDetailID`),
  KEY `MemorialItemID` (`MemorialItemID`),
  KEY `DeletedBy` (`DeletedBy`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblMemorialLease
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMemorialLease`;

CREATE TABLE `tblMemorialLease` (
  `MemorialLeaseID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MemorialLeaseName` varchar(50) NOT NULL DEFAULT '',
  `MemorialID` int(11) unsigned NOT NULL DEFAULT '0',
  `RegistrationCost` decimal(8,2) NOT NULL DEFAULT '0.00',
  `LeaseStartDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LeaseExpiryDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LeaseLengthYears` int(11) DEFAULT NULL,
  `PerpetualLease` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`MemorialLeaseID`),
  KEY `MemorialID` (`MemorialID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblMemorialMason
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMemorialMason`;

CREATE TABLE `tblMemorialMason` (
  `MemorialMasonID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MemorialMasonName` varchar(100) NOT NULL DEFAULT '',
  `ContactName` varchar(50) NOT NULL DEFAULT '',
  `EmailAddress` varchar(50) NOT NULL DEFAULT '',
  `PhoneNumber` varchar(50) NOT NULL DEFAULT '',
  `AddressLine1` varchar(50) NOT NULL DEFAULT '',
  `AddressLine2` varchar(50) NOT NULL DEFAULT '',
  `Town` varchar(50) NOT NULL DEFAULT '',
  `County` varchar(50) NOT NULL DEFAULT '',
  `Active` tinyint(1) NOT NULL DEFAULT '1',
  `Postcode` varchar(20) NOT NULL DEFAULT '',
  `InsuranceCompanyName` varchar(100) NOT NULL DEFAULT '',
  `InsurancePolicyNumber` varchar(50) NOT NULL DEFAULT '',
  `InsuranceRenewalDate` date NOT NULL DEFAULT '0000-00-00',
  `EmployerLiabilityAtRequiredValue` tinyint(1) NOT NULL DEFAULT '0',
  `ProductLiabilityAtRequiredValue` tinyint(1) NOT NULL DEFAULT '0',
  `PublicLiabilityAtRequiredValue` tinyint(1) NOT NULL DEFAULT '0',
  `Indemnity` tinyint(1) NOT NULL DEFAULT '0',
  `MasonCompliesWithTerms` tinyint(1) NOT NULL DEFAULT '0',
  `NoTwoYearBan` tinyint(1) NOT NULL DEFAULT '0',
  `BritishRegisterAccredited` tinyint(1) NOT NULL DEFAULT '0',
  `TestingCertsReceived` tinyint(1) NOT NULL DEFAULT '0',
  `TenYearGuarantee` tinyint(1) NOT NULL DEFAULT '0',
  `ReferencesReceived` tinyint(1) NOT NULL DEFAULT '0',
  `UserID` int(11) unsigned NOT NULL DEFAULT '0',
  `FaxNumber` varchar(20) NOT NULL DEFAULT '',
  `InsuranceRenewalNotified` tinyint(1) NOT NULL DEFAULT '0',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`MemorialMasonID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblMemorialMasonDocument
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMemorialMasonDocument`;

CREATE TABLE `tblMemorialMasonDocument` (
  `MemorialMasonDocumentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MemorialMasonID` int(11) DEFAULT NULL,
  `DocumentName` varchar(255) NOT NULL DEFAULT '',
  `DocumentType` varchar(255) NOT NULL DEFAULT '',
  `FormTypeID` int(11) DEFAULT NULL,
  PRIMARY KEY (`MemorialMasonDocumentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblMemorialMasonFee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMemorialMasonFee`;

CREATE TABLE `tblMemorialMasonFee` (
  `MemorialMasonFeeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MemorialMasonID` int(11) unsigned NOT NULL DEFAULT '0',
  `MemorialMasonFeeName` varchar(100) NOT NULL DEFAULT '',
  `PermitRegistrationID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceItemID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceRefID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceRefClass` varchar(250) NOT NULL DEFAULT '',
  `TaxRate` decimal(6,3) NOT NULL DEFAULT '0.000',
  `TaxAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `PriceIncTax` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Quantity` int(11) NOT NULL DEFAULT '0',
  `Total` decimal(8,2) NOT NULL DEFAULT '0.00',
  `SalesNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `CostNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `InventoryNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `SalesTaxNominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeType` varchar(250) NOT NULL DEFAULT '',
  `UnitDiscount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `UnitCostPrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`MemorialMasonFeeID`),
  KEY `MemorialMasonID` (`MemorialMasonID`),
  KEY `PermitRegistrationID` (`PermitRegistrationID`),
  KEY `FeeID` (`FeeID`),
  KEY `InvoiceItemID` (`InvoiceItemID`),
  KEY `InvoiceRefID` (`InvoiceRefID`),
  KEY `SalesNominalCodeID` (`SalesNominalCodeID`),
  KEY `CostNominalCodeID` (`CostNominalCodeID`),
  KEY `InventoryNominalCodeID` (`InventoryNominalCodeID`),
  KEY `SalesTaxNominalCodeID` (`SalesTaxNominalCodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblMemorialMasonPermitRegistration
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMemorialMasonPermitRegistration`;

CREATE TABLE `tblMemorialMasonPermitRegistration` (
  `PermitRegistrationID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `RegistrationStatus` enum('In Progress','Approved','Rejected','Probation','Inactive') NOT NULL DEFAULT 'In Progress',
  `RegistrationRenewalDate` date NOT NULL DEFAULT '0000-00-00',
  `RegistrationStartDate` date NOT NULL DEFAULT '0000-00-00',
  `RegistrationNotes` mediumtext NOT NULL,
  `RegistrationNumber` varchar(50) NOT NULL DEFAULT '',
  `RegistrationCost` decimal(8,2) NOT NULL DEFAULT '0.00',
  `MemorialMasonID` int(11) unsigned NOT NULL DEFAULT '0',
  `RegistrationRenewalNotified` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PermitRegistrationID`),
  KEY `MemorialMasonID` (`MemorialMasonID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblMemorialMasonWorkPermit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMemorialMasonWorkPermit`;

CREATE TABLE `tblMemorialMasonWorkPermit` (
  `MemorialMasonWorkPermitID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MemorialMasonID` int(11) DEFAULT NULL,
  `Notes` text NOT NULL,
  `Inscriptions` text NOT NULL,
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `SiteID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `WorkDate` date NOT NULL DEFAULT '0000-00-00',
  `WorkTime` time NOT NULL DEFAULT '00:00:00',
  `WorkEndTime` time NOT NULL DEFAULT '00:00:00',
  `WorkStatus` enum('Requested','In Progress','Approved','Rejected','Complete') NOT NULL DEFAULT 'Requested',
  `ApprovedDate` date NOT NULL DEFAULT '0000-00-00',
  `ApprovedByUserID` int(11) unsigned NOT NULL DEFAULT '0',
  `ApprovalNotes` text NOT NULL,
  `NatureOfWork` enum('Erection of a memorial','Inscription on an existing memorial','Remedial works to an existing memorial','Replacement Monument') NOT NULL DEFAULT 'Erection of a memorial',
  `FoundationIsInPlace` tinyint(1) NOT NULL DEFAULT '0',
  `DrawingsSignedByRegisteredOwnerOrNextOfKin` tinyint(1) NOT NULL DEFAULT '0',
  `DrawingsApprovedByGroundStaff` tinyint(1) NOT NULL DEFAULT '0',
  `RegistrationChecked` tinyint(1) NOT NULL DEFAULT '0',
  `PlotOwnershipConfirmed` tinyint(1) NOT NULL DEFAULT '0',
  `PaymentReceivedInFull` tinyint(1) NOT NULL DEFAULT '0',
  `PermitSentToMason` tinyint(1) NOT NULL DEFAULT '0',
  `WorkPermitRegistrationNumber` varchar(50) NOT NULL DEFAULT '',
  `GraveDeedProduced` tinyint(1) NOT NULL DEFAULT '0',
  `NotesOnCompletion` mediumtext NOT NULL,
  `WorkCheckedBy` int(11) unsigned NOT NULL DEFAULT '0',
  `WorkCheckedDate` date NOT NULL DEFAULT '0000-00-00',
  `EventID` int(11) unsigned NOT NULL DEFAULT '0',
  `DiaryID` int(11) unsigned NOT NULL DEFAULT '0',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `WorkStatusSetDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `DeletedBy` int(11) unsigned NOT NULL DEFAULT '0',
  `DeletedDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`MemorialMasonWorkPermitID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `SiteID` (`SiteID`),
  KEY `PlotID` (`PlotID`),
  KEY `ApprovedByUserID` (`ApprovedByUserID`),
  KEY `WorkCheckedBy` (`WorkCheckedBy`),
  KEY `EventID` (`EventID`),
  KEY `DiaryID` (`DiaryID`),
  KEY `DeletedBy` (`DeletedBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblMemorialMasonWorkPermitDocument
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMemorialMasonWorkPermitDocument`;

CREATE TABLE `tblMemorialMasonWorkPermitDocument` (
  `MemorialMasonWorkPermitDocumentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MemorialMasonWorkPermitID` int(11) DEFAULT NULL,
  `DocumentName` varchar(255) NOT NULL DEFAULT '',
  `DocumentType` varchar(255) NOT NULL DEFAULT '',
  `FormTypeID` int(11) DEFAULT NULL,
  PRIMARY KEY (`MemorialMasonWorkPermitDocumentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblMenuItem
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMenuItem`;

CREATE TABLE `tblMenuItem` (
  `MenuItemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ParentMenuItemID` int(11) NOT NULL DEFAULT '0',
  `MenuName` varchar(100) NOT NULL DEFAULT '',
  `Url` varchar(200) NOT NULL DEFAULT '',
  `SecurityOption` varchar(200) NOT NULL DEFAULT '',
  `ParentMenuItemIDs` varchar(200) NOT NULL DEFAULT '',
  `CssClassName` varchar(40) NOT NULL DEFAULT '',
  `Position` int(11) NOT NULL DEFAULT '0',
  `Icon` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`MenuItemID`),
  KEY `ParentMenuItemID` (`ParentMenuItemID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblMenuItem` WRITE;
/*!40000 ALTER TABLE `tblMenuItem` DISABLE KEYS */;

INSERT INTO `tblMenuItem` (`MenuItemID`, `ParentMenuItemID`, `MenuName`, `Url`, `SecurityOption`, `ParentMenuItemIDs`, `CssClassName`, `Position`, `Icon`)
VALUES
	(1,0,'Dashboard','/landing/','','','',0,'dashboard'),
	(2,0,'<i class=\"fa fa-calendar\"></i> <span>Schedule</span>','/diary/','Diary','','',0,'calendar'),
	(3,0,'Records','/records/','Records','','',0,'folder'),
	(4,0,'Contracts','/contracts/','Contracts','','',0,'cost'),
	(5,0,'Contacts','/contacts/','Contacts','','',0,'users'),
	(6,0,'Deeds','/plot-purchases/','Deeds','','',0,'users'),
	(7,0,'Memorials','/memorials/','Memorials','','',0,'flag'),
	(8,0,'Plots','/plots/','Plots','','',0,'pin'),
	(9,0,'Permits','/events/work-permits/','Diary/WorkPermits','','',0,'notebook'),
	(10,0,'Finance','/finance/','Finance','','',0,'cost'),
	(11,0,'Reporting','/reporting/','Reports','','',0,'barchart'),
	(12,0,'Docs','/books/','Books','','',0,'book'),
	(13,0,'CRM','/crm/','CRM','','',0,'crm'),
	(14,0,'Work','/work/','Work','','',0,'notebook');

/*!40000 ALTER TABLE `tblMenuItem` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblMergedContactHistories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMergedContactHistories`;

CREATE TABLE `tblMergedContactHistories` (
  `MergedContactHistoriesID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SourceContactID` int(11) DEFAULT NULL,
  `TagetContactID` int(11) DEFAULT NULL,
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `MergedBy` varchar(100) NOT NULL DEFAULT '',
  `MergedDate` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`MergedContactHistoriesID`),
  KEY `SourceContactID` (`SourceContactID`),
  KEY `TagetContactID` (`TagetContactID`),
  KEY `MergedBy` (`MergedBy`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblNominalCode
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblNominalCode`;

CREATE TABLE `tblNominalCode` (
  `NominalCodeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NominalCode` varchar(255) NOT NULL DEFAULT '',
  `NominalCodeDescription` varchar(255) NOT NULL DEFAULT '',
  `AccountType` enum('Asset','Liability','Revenue','Cost of Sales','Equity','Expense') NOT NULL DEFAULT 'Revenue',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`NominalCodeID`),
  KEY `FacilityID` (`FacilityID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblNominalCode` WRITE;
/*!40000 ALTER TABLE `tblNominalCode` DISABLE KEYS */;

INSERT INTO `tblNominalCode` (`NominalCodeID`, `NominalCode`, `NominalCodeDescription`, `AccountType`, `IsDeleted`, `FacilityID`)
VALUES
	(1,'10900-000','DEFAULT ACCOUNTS RECEIVABLE','Asset',0,0),
	(2,'10200-000','DEFAULT CASH ACCOUNT','Asset',0,0),
	(3,'11300-000','DEFAULT OVERPAYMENT ACCOUNT','Liability',0,0),
	(4,'20500-000','DEFAULT SALES TAX','Liability',0,0),
	(5,'99900-000','DEFAULT SUSPENSE ACCOUNT','Liability',0,0),
	(6,'30100-000','DEFAULT SALES REVENUE','Revenue',0,0);

/*!40000 ALTER TABLE `tblNominalCode` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblNominatedDecedant
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblNominatedDecedant`;

CREATE TABLE `tblNominatedDecedant` (
  `NominatedDecedantID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`NominatedDecedantID`),
  KEY `ContactID` (`ContactID`),
  KEY `PlotID` (`PlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblNote
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblNote`;

CREATE TABLE `tblNote` (
  `NoteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NoteModelID` int(11) DEFAULT NULL,
  `NoteType` enum('Contract','Deed','Plot') NOT NULL DEFAULT 'Contract',
  `NoteTitle` varchar(70) NOT NULL DEFAULT '',
  `NoteText` mediumtext NOT NULL,
  `DocumentNote` tinyint(1) NOT NULL DEFAULT '0',
  `InternalNote` tinyint(1) NOT NULL DEFAULT '0',
  `MigrationNote` tinyint(1) NOT NULL DEFAULT '0',
  `Imported` tinyint(1) NOT NULL DEFAULT '0',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `DeletedBy` int(11) unsigned NOT NULL DEFAULT '0',
  `DeletedDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`NoteID`),
  KEY `DeletedBy` (`DeletedBy`),
  KEY `NoteType` (`NoteType`),
  KEY `Deleted` (`Deleted`),
  KEY `DeletedDate` (`DeletedDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblNotification
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblNotification`;

CREATE TABLE `tblNotification` (
  `NotificationID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NotificationTitle` varchar(250) NOT NULL DEFAULT 'Plotbox Notification',
  `NotificationBody` mediumtext NOT NULL,
  `Recipient` varchar(50) NOT NULL DEFAULT '',
  `Status` enum('Pending','In Progress','Sent','Failed') NOT NULL DEFAULT 'Pending',
  `NotificationType` enum('Email','SMS') NOT NULL DEFAULT 'Email',
  `Source` varchar(50) NOT NULL DEFAULT '',
  `SourceID` int(11) NOT NULL DEFAULT '0',
  `NotificationDate` date NOT NULL DEFAULT '0000-00-00',
  `NotificationCategory` enum('System','Protection Plan','Weekly Report','Monthly Report') NOT NULL DEFAULT 'System',
  `SourceAction` enum('None','Assign','Due','Complete') NOT NULL DEFAULT 'None',
  `ToContactID` int(11) DEFAULT NULL,
  `ToUserID` int(11) DEFAULT NULL,
  `Viewed` tinyint(1) NOT NULL DEFAULT '0',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`NotificationID`),
  KEY `Source` (`Source`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblPayment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblPayment`;

CREATE TABLE `tblPayment` (
  `PaymentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `AmountOutstanding` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Reference` varchar(100) NOT NULL DEFAULT '',
  `Notes` text NOT NULL,
  `Type` enum('Cash','Card','Cheque','Bank Transfer','Online Payment','Voiding Transaction','Refund','Adjustment','Credit','Returned Payment') NOT NULL DEFAULT 'Cash',
  `ContractPaymentType` enum('Non-contract','Down payment','One time','Regular','Voiding Transaction','Refund','Adjustment','Credit','Returned Payment') NOT NULL DEFAULT 'Non-contract',
  `ContractOneOffPaymentStyle` enum('N/A','Reduce Term','Reduce Instalment Cost','Payment Holiday') NOT NULL DEFAULT 'N/A',
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `PaymentDate` date NOT NULL DEFAULT '0000-00-00',
  `FuneralDirectorID` int(11) unsigned NOT NULL DEFAULT '0',
  `MemorialMasonID` int(11) unsigned NOT NULL DEFAULT '0',
  `InvoiceID` int(11) unsigned NOT NULL DEFAULT '0',
  `StripeChargeID` varchar(50) NOT NULL DEFAULT '',
  `Reconciled` tinyint(1) NOT NULL DEFAULT '0',
  `ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `IsVoid` tinyint(1) NOT NULL DEFAULT '0',
  `LinkedPaymentID` int(11) unsigned NOT NULL DEFAULT '0',
  `TransactionID` int(11) unsigned NOT NULL DEFAULT '0',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `PaymentBatchID` int(11) unsigned NOT NULL DEFAULT '0',
  `CreditContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `PayerNameText` varchar(200) NOT NULL DEFAULT '',
  `IsReturned` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PaymentID`),
  KEY `ContractID` (`ContractID`),
  KEY `FuneralDirectorID` (`FuneralDirectorID`),
  KEY `MemorialMasonID` (`MemorialMasonID`),
  KEY `InvoiceID` (`InvoiceID`),
  KEY `ContactID` (`ContactID`),
  KEY `LinkedPaymentID` (`LinkedPaymentID`),
  KEY `TransactionID` (`TransactionID`),
  KEY `PaymentBatchID` (`PaymentBatchID`),
  KEY `CreditContractID` (`CreditContractID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblPaymentBatch
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblPaymentBatch`;

CREATE TABLE `tblPaymentBatch` (
  `PaymentBatchID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `BatchNumber` varchar(50) NOT NULL DEFAULT '',
  `FacilityID` int(11) DEFAULT NULL,
  `StartDate` date NOT NULL DEFAULT '0000-00-00',
  `EndDate` date NOT NULL DEFAULT '0000-00-00',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`PaymentBatchID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblPaymentExport
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblPaymentExport`;

CREATE TABLE `tblPaymentExport` (
  `PaymentExportID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ExportDate` date NOT NULL DEFAULT '0000-00-00',
  `ExportFormat` enum('','AGRES','QUICK','CSV') NOT NULL DEFAULT '',
  `PaymentID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`PaymentExportID`),
  KEY `PaymentID` (`PaymentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblPaymentSchedule
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblPaymentSchedule`;

CREATE TABLE `tblPaymentSchedule` (
  `PaymentScheduleID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `TotalCashPrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  `DownPayment` decimal(8,2) NOT NULL DEFAULT '0.00',
  `DownPaymentDueDate` date NOT NULL DEFAULT '0000-00-00',
  `AnnualPercentageRate` decimal(8,2) NOT NULL DEFAULT '0.00',
  `InstalmentCalculator` enum('NumberOfInstallments','InstallmentAmount') NOT NULL DEFAULT 'NumberOfInstallments',
  `NumberOfInstallments` int(11) DEFAULT NULL,
  `InstallmentAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `AmountFinanced` decimal(8,2) NOT NULL DEFAULT '0.00',
  `TotalInterest` decimal(8,2) NOT NULL DEFAULT '0.00',
  `TotalFinanced` decimal(8,2) NOT NULL DEFAULT '0.00',
  `TotalSalePrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  `FirstPaymentDate` date NOT NULL DEFAULT '0000-00-00',
  `InstallmentAutoPay` tinyint(1) NOT NULL DEFAULT '0',
  `RemainingInstallments` decimal(8,1) NOT NULL DEFAULT '0.0',
  `NextExpectedPaymentDate` date NOT NULL DEFAULT '0000-00-00',
  `TotalPayments` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `GenerationMethod` enum('Current Balance','Original Price') NOT NULL DEFAULT 'Current Balance',
  `InterestGraceType` enum('None','Interest Free','Interest Forgiveness') NOT NULL DEFAULT 'None',
  `InterestFreeMonths` int(11) DEFAULT NULL,
  `InterestForgivenessMonths` int(11) DEFAULT NULL,
  PRIMARY KEY (`PaymentScheduleID`),
  KEY `ContractID` (`ContractID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblPaymentScheduleDetail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblPaymentScheduleDetail`;

CREATE TABLE `tblPaymentScheduleDetail` (
  `PaymentScheduleDetailID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PaymentScheduleID` int(11) unsigned NOT NULL DEFAULT '0',
  `PaymentType` enum('Down payment','Regular') NOT NULL DEFAULT 'Regular',
  `DueDate` date NOT NULL DEFAULT '0000-00-00',
  `AmountDue` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Principal` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Interest` decimal(8,2) NOT NULL DEFAULT '0.00',
  `AmountRemaining` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Overpayment` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CompletedDate` date NOT NULL DEFAULT '0000-00-00',
  `Notes` mediumtext NOT NULL,
  PRIMARY KEY (`PaymentScheduleDetailID`),
  KEY `PaymentScheduleID` (`PaymentScheduleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblPaymentScheduleDetailPayment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblPaymentScheduleDetailPayment`;

CREATE TABLE `tblPaymentScheduleDetailPayment` (
  `PaymentScheduleDetailPaymentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PaymentScheduleDetailID` int(11) unsigned NOT NULL DEFAULT '0',
  `PaymentID` int(11) unsigned NOT NULL DEFAULT '0',
  `Amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PaymentScheduleDetailPaymentID`),
  KEY `PaymentScheduleDetailID` (`PaymentScheduleDetailID`),
  KEY `PaymentID` (`PaymentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblPickListGroup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblPickListGroup`;

CREATE TABLE `tblPickListGroup` (
  `PickListGroupID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PickListGroupName` varchar(100) NOT NULL DEFAULT '',
  `Description` varchar(200) NOT NULL DEFAULT '',
  `IsUserConfigurable` tinyint(1) NOT NULL DEFAULT '1',
  `DisplayIcon` varchar(50) NOT NULL DEFAULT '',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `Module` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`PickListGroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tblPickListGroup` WRITE;
/*!40000 ALTER TABLE `tblPickListGroup` DISABLE KEYS */;

INSERT INTO `tblPickListGroup` (`PickListGroupID`, `PickListGroupName`, `Description`, `IsUserConfigurable`, `DisplayIcon`, `IsDeleted`, `Module`)
VALUES
	(1,'Relationships','Maintained list of possible relationships between contacts',1,'fa-group',0,''),
	(2,'Casket Type','Maintained list of casket types used at burial',1,'fa-group',0,''),
	(3,'Burial Type','Maintained list of burial types',1,'fa-group',0,''),
	(4,'Marital Status','Maintained list of marital statuses',1,'fa-group',0,''),
	(5,'Urn Delivered By','Maintained list of persons who deliver an urn for burial',1,'fa-group',0,''),
	(6,'Plot Type','Maintained list of plot types',1,'fa-group',0,''),
	(7,'Branch of Service','Maintained list of military branches of service',1,'fa-group',0,''),
	(8,'Military Rank','Maintained list of military ranks',1,'fa-group',0,''),
	(9,'Tours of Duty (War/Conflict)','Maintained list of tours of duty (war/conflict)',1,'fa-group',0,''),
	(10,'Work Order Type of Work','Maintained list of the types of work used in a work order',1,'fa-group',0,''),
	(11,'Work Order Type of Work','Maintained list of the types of work used in a work order',1,'fa-group',0,''),
	(12,'Lead Detail','Maintained list of Lead Detail',1,'fa-group',0,'crm'),
	(13,'Lead Source','Maintained list of Lead Source',1,'fa-group',0,'crm'),
	(14,'Lead Status','Maintained list of Lead Status',1,'fa-group',0,'crm'),
	(15,'LifeCycle Stage','Maintained list of Life Cycle Stage',1,'fa-group',0,'crm'),
	(16,'Urn Type','Maintained list of urn types used for cremated remains',1,'fa-group',0,''),
	(17,'Vault Type','Maintained list of vault types',1,'fa-group',0,''),
	(18,'Memorial Type','Maintained list of memorial types',1,'fa-group',0,''),
	(19,'Music Type','Maintained list of music types',1,'fa-group',0,'');

/*!40000 ALTER TABLE `tblPickListGroup` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblPickListItem
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblPickListItem`;

CREATE TABLE `tblPickListItem` (
  `PickListItemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PickListGroupName` varchar(100) NOT NULL DEFAULT '',
  `PickListItemValue` varchar(50) NOT NULL DEFAULT '',
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `IsSystemBaseValue` tinyint(1) NOT NULL DEFAULT '0',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PickListItemID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tblPickListItem` WRITE;
/*!40000 ALTER TABLE `tblPickListItem` DISABLE KEYS */;

INSERT INTO `tblPickListItem` (`PickListItemID`, `PickListGroupName`, `PickListItemValue`, `SortOrder`, `IsSystemBaseValue`, `IsDeleted`)
VALUES
	(1,'Relationships','Spouse',10,1,0),
	(2,'Relationships','Child',8,1,0),
	(3,'Relationships','Parent',7,1,0),
	(4,'Relationships','Partner',3,1,0),
	(5,'Relationships','Sister',6,1,0),
	(6,'Relationships','Brother',5,1,0),
	(7,'Relationships','Friend',4,1,0),
	(8,'Relationships','NSR/EXEC',2,1,0),
	(9,'Relationships','POAH',1,1,0),
	(10,'Relationships','Other',9,1,0),
	(11,'Casket Type','N/A',0,1,0),
	(12,'Casket Type','English',0,1,0),
	(13,'Casket Type','American',0,1,0),
	(14,'Casket Type','Shroud',0,1,0),
	(15,'Casket Type','Casket',0,1,0),
	(16,'Casket Type','Coffin',0,1,0),
	(17,'Casket Type','Metal',0,1,0),
	(18,'Casket Type','Wood',0,1,0),
	(19,'Casket Type','Cloth Covered Wood',0,1,0),
	(20,'Burial Type','Adult',0,1,1),
	(21,'Burial Type','Child',0,1,1),
	(22,'Burial Type','Casket',0,1,1),
	(23,'Burial Type','Full Body Casket',1,1,0),
	(24,'Burial Type','Cremated remains',2,1,0),
	(25,'Burial Type','Retained organs',3,1,0),
	(26,'Marital Status','Single',0,1,0),
	(27,'Marital Status','Never Married',0,1,0),
	(28,'Marital Status','Married',0,1,0),
	(29,'Marital Status','Separated',0,1,0),
	(30,'Marital Status','Divorced',0,1,0),
	(31,'Marital Status','Widowed',0,1,0),
	(32,'Marital Status','N/A',0,1,0),
	(33,'Urn Delivered By','Funeral Director',0,1,0),
	(34,'Urn Delivered By','Family',0,1,0),
	(35,'Burial Type','Adult',0,1,1),
	(36,'Burial Type','Child',0,1,1),
	(37,'Burial Type','Casket',0,1,1),
	(38,'Burial Type','Adult',0,1,1),
	(39,'Burial Type','Child',0,1,1),
	(40,'Burial Type','Casket',0,1,1),
	(41,'Plot Type','Child Grave',0,0,0),
	(42,'Plot Type','Cremated Remains',0,0,0),
	(43,'Plot Type','Crypt Plot',0,0,0),
	(44,'Plot Type','Dual Grave',0,0,0),
	(45,'Plot Type','Memorial Plot',0,0,0),
	(46,'Plot Type','Multiple Space Plot',0,0,1),
	(47,'Plot Type','Niche Plot',0,0,0),
	(48,'Plot Type','Traditional Grave',0,0,0),
	(49,'Branch of Service','Air Force',0,1,0),
	(50,'Branch of Service','Air Force Reserve',0,1,0),
	(51,'Branch of Service','Air National Guard',0,1,0),
	(52,'Branch of Service','Army',0,1,0),
	(53,'Branch of Service','Army Reserve',0,1,0),
	(54,'Branch of Service','Army National Guard',0,1,0),
	(55,'Branch of Service','Coast Guard',0,1,0),
	(56,'Branch of Service','Coast Guard Reserve',0,1,0),
	(57,'Branch of Service','Marine Corps',0,1,0),
	(58,'Branch of Service','Marine Corps Reserve',0,1,0),
	(59,'Branch of Service','Navy',0,1,0),
	(60,'Branch of Service','Navy Reserve',0,1,0),
	(61,'Military Rank','Admiral',0,1,0),
	(62,'Military Rank','Airman',0,1,0),
	(63,'Military Rank','Captain',0,1,0),
	(64,'Military Rank','Colonel',0,1,0),
	(65,'Military Rank','Corporal',0,1,0),
	(66,'Military Rank','First Lieutenant',0,1,0),
	(67,'Military Rank','Lieutenant',0,1,0),
	(68,'Military Rank','Major',0,1,0),
	(69,'Military Rank','Private',0,1,0),
	(70,'Military Rank','Recruit',0,1,0),
	(71,'Military Rank','Seaman',0,1,0),
	(72,'Military Rank','Sergeant',0,1,0),
	(73,'Military Rank','Staff Sergeant',0,1,0),
	(74,'Tours of Duty (War/Conflict)','American Revolution',0,1,0),
	(75,'Tours of Duty (War/Conflict)','War of 1812',0,1,0),
	(76,'Tours of Duty (War/Conflict)','Mexican War',0,1,0),
	(77,'Tours of Duty (War/Conflict)','Civil War',0,1,0),
	(78,'Tours of Duty (War/Conflict)','Indian Wars',0,1,0),
	(79,'Tours of Duty (War/Conflict)','Spanish American War',0,1,0),
	(80,'Tours of Duty (War/Conflict)','World War I',0,1,0),
	(81,'Tours of Duty (War/Conflict)','World War II',0,1,0),
	(82,'Tours of Duty (War/Conflict)','Korean War',0,1,0),
	(83,'Tours of Duty (War/Conflict)','Vietnam War',0,1,0),
	(84,'Tours of Duty (War/Conflict)','Gulf War',0,1,0),
	(85,'Tours of Duty (War/Conflict)','Somalia',0,1,0),
	(86,'Tours of Duty (War/Conflict)','Bosnia',0,1,0),
	(87,'Tours of Duty (War/Conflict)','Kosovo',0,1,0),
	(88,'Tours of Duty (War/Conflict)','Afghanistan',0,1,0),
	(89,'Tours of Duty (War/Conflict)','Iraq War',0,1,0),
	(90,'Tours of Duty (War/Conflict)','War Against the Islamic State (ISIS)',0,1,0),
	(91,'Burial Type','Adult',0,1,1),
	(92,'Burial Type','Child',0,1,1),
	(93,'Burial Type','Casket',0,1,1),
	(94,'Burial Type','Adult',0,1,1),
	(95,'Burial Type','Adult',0,1,1),
	(96,'Burial Type','Child',0,1,1),
	(97,'Burial Type','Child',0,1,1),
	(98,'Burial Type','Casket',0,1,1),
	(99,'Burial Type','Casket',0,1,1),
	(100,'Work Order Type of Work','Clean Memorial',0,1,0),
	(101,'Work Order Type of Work','Remove Shrubs',0,1,0),
	(102,'Work Order Type of Work','Trim Shrubs',0,1,0),
	(103,'LifeCycle Stage','Lead',0,1,0),
	(104,'LifeCycle Stage','Qualified Lead',0,1,0),
	(105,'LifeCycle Stage','Opportunity',0,1,0),
	(106,'LifeCycle Stage','Customer',0,1,0),
	(107,'LifeCycle Stage','Evangelist',0,1,0),
	(108,'Lead Detail','Free Dinner',0,1,0),
	(109,'Lead Detail','Rotary',0,1,0),
	(110,'Lead Detail','Church Presentation',0,1,0),
	(111,'Lead Detail','TV Ad',0,1,0),
	(112,'Lead Detail','Radio Ad',0,1,0),
	(113,'Lead Source','Outreach Event',0,1,0),
	(114,'Lead Source','Reload',0,1,0),
	(115,'Lead Source','At-Need Family',0,1,0),
	(116,'Lead Source','Website Request',0,1,0),
	(117,'Lead Source','Reply Card',0,1,0),
	(118,'Lead Source','Calendar',0,1,0),
	(119,'Lead Status','Contacted',0,1,0),
	(120,'Lead Status','In Progress',0,1,0),
	(121,'Lead Status','Unqualified',0,1,0),
	(122,'Burial Type','Adult',0,1,1),
	(123,'Burial Type','Child',0,1,1),
	(124,'Burial Type','Casket',0,1,1),
	(125,'Burial Type','Adult',0,1,1),
	(126,'Burial Type','Child',0,1,1),
	(127,'Burial Type','Casket',0,1,1),
	(128,'Burial Type','Adult',0,1,1),
	(129,'Burial Type','Child',0,1,1),
	(130,'Burial Type','Casket',0,1,1),
	(131,'Burial Type','Adult',0,1,1),
	(132,'Burial Type','Child',0,1,1),
	(133,'Burial Type','Casket',0,1,1),
	(134,'Urn Type','Plastic',0,1,0),
	(135,'Urn Type','Wood',0,1,0),
	(136,'Urn Type','Ceramic',0,1,0),
	(137,'Urn Type','Metal',0,1,0),
	(138,'Vault Type','Pre-Installed',0,1,0),
	(139,'Vault Type','Single',0,1,0),
	(140,'Vault Type','Double',0,1,0),
	(141,'Burial Type','Adult',0,1,1),
	(142,'Burial Type','Child',0,1,1),
	(143,'Burial Type','Casket',0,1,1),
	(144,'Burial Type','Adult',0,1,1),
	(145,'Burial Type','Child',0,1,1),
	(146,'Burial Type','Casket',0,1,1),
	(147,'Burial Type','Adult',0,1,1),
	(148,'Burial Type','Child',0,1,1),
	(149,'Burial Type','Casket',0,1,1),
	(150,'Burial Type','Adult',0,1,1),
	(151,'Burial Type','Child',0,1,1),
	(152,'Burial Type','Casket',0,1,1),
	(153,'Burial Type','Adult',0,1,1),
	(154,'Burial Type','Child',0,1,1),
	(155,'Burial Type','Casket',0,1,1),
	(156,'Burial Type','Adult',0,1,1),
	(157,'Burial Type','Child',0,1,1),
	(158,'Burial Type','Casket',0,1,1),
	(159,'Burial Type','Adult',0,1,1),
	(160,'Burial Type','Child',0,1,1),
	(161,'Burial Type','Casket',0,1,1),
	(162,'Burial Type','Adult',0,1,1),
	(163,'Burial Type','Child',0,1,1),
	(164,'Burial Type','Casket',0,1,1),
	(165,'Memorial Type','Stone Works',0,1,0),
	(166,'Memorial Type','Garden Memorials',0,1,0),
	(167,'Memorial Type','Books/Scrolls',0,1,0),
	(168,'Burial Type','Adult',0,1,1),
	(169,'Burial Type','Child',0,1,1),
	(170,'Burial Type','Casket',0,1,1),
	(171,'Burial Type','Adult',0,1,1),
	(172,'Burial Type','Child',0,1,1),
	(173,'Burial Type','Casket',0,1,1),
	(174,'Music Type','Organ',0,1,0),
	(175,'Music Type','CD',0,1,0),
	(176,'Music Type','Music Media',0,1,0),
	(177,'Relationships','Wife',0,0,0),
	(178,'Burial Type','Adult',0,1,1),
	(179,'Burial Type','Child',0,1,1),
	(180,'Burial Type','Casket',0,1,1);

/*!40000 ALTER TABLE `tblPickListItem` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblPlot
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblPlot`;

CREATE TABLE `tblPlot` (
  `PlotID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `FacilitySectionID` int(11) unsigned NOT NULL DEFAULT '0',
  `Row` varchar(50) NOT NULL DEFAULT '',
  `PlotNumber` varchar(50) NOT NULL DEFAULT '',
  `Section` varchar(50) NOT NULL DEFAULT '',
  `SectionShortCode` varchar(10) NOT NULL DEFAULT '',
  `ImportedNorthing` varchar(15) NOT NULL DEFAULT '',
  `ImportedEasting` varchar(15) NOT NULL DEFAULT '',
  `Latitude` decimal(11,8) NOT NULL DEFAULT '0.00000000',
  `Longitude` decimal(11,8) NOT NULL DEFAULT '0.00000000',
  `PrimaryImageID` int(11) NOT NULL DEFAULT '0',
  `CurrentDeedID` int(11) unsigned NOT NULL DEFAULT '0',
  `Purchased` tinyint(1) NOT NULL DEFAULT '0',
  `DepthRemainingFeet` int(11) DEFAULT NULL,
  `DepthRemainingInches` int(11) DEFAULT NULL,
  `DepthRemainingMeters` int(11) DEFAULT NULL,
  `DepthRemainingCentimeters` int(11) DEFAULT NULL,
  `PlotCost` decimal(8,2) NOT NULL DEFAULT '0.00',
  `DepthRemainingString` varchar(50) NOT NULL DEFAULT '',
  `ImportPlotIdentifier` varchar(100) NOT NULL DEFAULT '',
  `Notes` mediumtext NOT NULL,
  `PlotHeadstone` enum('Headstone','No-Headstone','Other') NOT NULL DEFAULT 'Other',
  `PlotStatus` enum('Available','Full','Partial','Empty','Unassigned but occupied','On Hold') NOT NULL DEFAULT 'Available',
  `RiskAssessmentStatus` varchar(50) NOT NULL DEFAULT 'Open',
  `DecedantId` int(11) DEFAULT NULL,
  `ImportSearchablePlotReference` varchar(150) NOT NULL DEFAULT '',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `PlotType` varchar(50) NOT NULL DEFAULT '',
  `IsMultipleSpace` tinyint(1) NOT NULL DEFAULT '0',
  `PlotInventoryCost` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Source` enum('PlotBox','External') NOT NULL DEFAULT 'PlotBox',
  `ProblematicPlotCategory` varchar(50) NOT NULL DEFAULT 'None',
  `LocationCreatedBy` int(11) DEFAULT NULL,
  `LocationDateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LocationLastUpdatedBy` int(11) DEFAULT NULL,
  `LocationLastUpdatedDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TotalIntermentCapacity` int(11) NOT NULL DEFAULT '0',
  `RemainingIntermentCapacity` int(11) NOT NULL DEFAULT '0',
  `IntermentOccupancy` int(11) NOT NULL DEFAULT '0',
  `TotalInurnmentCapacity` int(11) NOT NULL DEFAULT '0',
  `RemainingInurnmentCapacity` int(11) NOT NULL DEFAULT '0',
  `InurnmentOccupancy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PlotID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `FacilitySectionID` (`FacilitySectionID`),
  KEY `CurrentDeedID` (`CurrentDeedID`),
  KEY `ImportPlotIdentifier_idx` (`ImportPlotIdentifier`),
  KEY `ImportsearchIdentifier_idx` (`ImportSearchablePlotReference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblPlotBoxCustomer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblPlotBoxCustomer`;

CREATE TABLE `tblPlotBoxCustomer` (
  `CustomerID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CompanyDomain` varchar(200) NOT NULL DEFAULT '',
  `CompanyName` varchar(200) NOT NULL DEFAULT '',
  `Package` enum('Lite','Basic','Pro','Enterprise') NOT NULL DEFAULT 'Lite',
  `Licenses` int(11) DEFAULT NULL,
  PRIMARY KEY (`CustomerID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblPlotImage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblPlotImage`;

CREATE TABLE `tblPlotImage` (
  `PlotImageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `UploadedDate` date NOT NULL DEFAULT '0000-00-00',
  `FileType` varchar(100) NOT NULL DEFAULT '',
  `Type` enum('Other','Headstone') NOT NULL DEFAULT 'Other',
  `ImageQuality` enum('Good Photo','Bad Photo','Bad Headstone','Not a Headstone','No information to transcribe','Inappropriate Image','Other') NOT NULL DEFAULT 'Good Photo',
  `Order` int(11) DEFAULT NULL,
  PRIMARY KEY (`PlotImageID`),
  KEY `PlotID` (`PlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblPlotPurchase
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblPlotPurchase`;

CREATE TABLE `tblPlotPurchase` (
  `PlotPurchaseID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DeedID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `Disabled` tinyint(1) NOT NULL DEFAULT '0',
  `DisabledReason` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`PlotPurchaseID`),
  KEY `DeedID` (`DeedID`),
  KEY `PlotID` (`PlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblPlotSpace
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblPlotSpace`;

CREATE TABLE `tblPlotSpace` (
  `PlotSpaceID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `Position` varchar(50) NOT NULL DEFAULT '',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`PlotSpaceID`),
  KEY `PlotID` (`PlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblPlotType
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblPlotType`;

CREATE TABLE `tblPlotType` (
  `PlotTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PlotTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblPlotType` WRITE;
/*!40000 ALTER TABLE `tblPlotType` DISABLE KEYS */;

INSERT INTO `tblPlotType` (`PlotTypeID`, `Name`, `Deleted`)
VALUES
	(1,'Traditional Grave',0),
	(2,'Child Grave',0),
	(3,'Cremated Remains',0),
	(4,'Memorial Plot',0),
	(5,'Niche Plot',0),
	(6,'Crypt Plot',0),
	(7,'Dual Grave',0),
	(8,'Multiple Space Plot',0);

/*!40000 ALTER TABLE `tblPlotType` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblPlotTypeFee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblPlotTypeFee`;

CREATE TABLE `tblPlotTypeFee` (
  `PlotTypeFeeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PickListItemID` int(11) unsigned NOT NULL DEFAULT '0',
  `FeeID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`PlotTypeFeeID`),
  KEY `PickListItemID` (`PickListItemID`),
  KEY `FeeID` (`FeeID`),
  KEY `PlotTypeFee_PickListItemID_Index` (`PickListItemID`),
  KEY `PlotTypeFee_FeeID_Index` (`FeeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblRegistrationDistrict
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblRegistrationDistrict`;

CREATE TABLE `tblRegistrationDistrict` (
  `RegistrationDistrictID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(150) NOT NULL DEFAULT '',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`RegistrationDistrictID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblReligionDenomination
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblReligionDenomination`;

CREATE TABLE `tblReligionDenomination` (
  `ReligionDenominationID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `Historic` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ReligionDenominationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblReligionDenomination` WRITE;
/*!40000 ALTER TABLE `tblReligionDenomination` DISABLE KEYS */;

INSERT INTO `tblReligionDenomination` (`ReligionDenominationID`, `Name`, `Deleted`, `Historic`)
VALUES
	(1,'Church of Ireland',1,1),
	(2,'Methodist',1,1),
	(3,'Roman Catholic',0,0),
	(4,'Presbyterian',1,1),
	(5,'Baptist',1,1),
	(6,'N/A',0,0);

/*!40000 ALTER TABLE `tblReligionDenomination` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblRiskAssessment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblRiskAssessment`;

CREATE TABLE `tblRiskAssessment` (
  `RiskAssessmentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `MemorialMasonID` int(11) DEFAULT NULL,
  `AssessmentDate` date NOT NULL DEFAULT '0000-00-00',
  `MemorialType` varchar(100) NOT NULL DEFAULT 'Kerb',
  `Height` varchar(100) NOT NULL DEFAULT 'Over 2 metres',
  `Condition` varchar(100) NOT NULL DEFAULT 'Good',
  `Result` varchar(100) NOT NULL DEFAULT 'Pass',
  `Risk` varchar(100) NOT NULL DEFAULT 'Red',
  `ReasonForRisk` varchar(100) NOT NULL DEFAULT 'Found',
  `Notes` mediumtext NOT NULL,
  `ActionRequired` tinyint(1) NOT NULL DEFAULT '1',
  `NextAssessmentDate` date NOT NULL DEFAULT '0000-00-00',
  `AssessmentInterval` varchar(100) NOT NULL DEFAULT '',
  `MemorialMasonRequired` tinyint(1) NOT NULL DEFAULT '1',
  `Action` varchar(100) NOT NULL DEFAULT '',
  `ReinstateCategory` varchar(100) NOT NULL DEFAULT '1 Year',
  `Status` varchar(100) NOT NULL DEFAULT 'Open',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`RiskAssessmentID`),
  KEY `PlotID` (`PlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblRiskAssessmentImage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblRiskAssessmentImage`;

CREATE TABLE `tblRiskAssessmentImage` (
  `RiskAssessmentImageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `RiskAssessmentID` int(11) unsigned NOT NULL DEFAULT '0',
  `UploadedDate` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`RiskAssessmentImageID`),
  KEY `RiskAssessmentID` (`RiskAssessmentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblSetting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblSetting`;

CREATE TABLE `tblSetting` (
  `SettingID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL DEFAULT '',
  `Value` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`SettingID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tblSetting` WRITE;
/*!40000 ALTER TABLE `tblSetting` DISABLE KEYS */;

INSERT INTO `tblSetting` (`SettingID`, `Name`, `Value`)
VALUES
	(1,'SubDomain','dlrcc'),
	(2,'CurrencySymbol','$'),
	(3,'CurrencySymbolHtml','&dollar;'),
	(4,'DeveloperMode','1'),
	(5,'ValidateCoffinDimensions','1'),
	(6,'DateFormat','m/d/Y'),
	(7,'DateTimeFormat','m/d/Y H:i'),
	(8,'DateInputFormat','m/d/Y'),
	(9,'ProblematicPlotCategory','None|Typo|Missing Plot between consecutive numbers|Plot reference wrong in database|Missing Plot between consecutive numbers|Missing Record|Missing headstone|One Plot for two headstones|Tree in Plot area|Damaged headstone|Waterlogged|Wrong photo taken|Created plot reference|Point picking off site|Memorial with pre-need inscription');

/*!40000 ALTER TABLE `tblSetting` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblSite
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblSite`;

CREATE TABLE `tblSite` (
  `SiteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DivisionID` int(11) unsigned NOT NULL DEFAULT '0',
  `SiteName` varchar(50) NOT NULL DEFAULT '',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SiteID`),
  KEY `DivisionID` (`DivisionID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tblSite` WRITE;
/*!40000 ALTER TABLE `tblSite` DISABLE KEYS */;

INSERT INTO `tblSite` (`SiteID`, `DivisionID`, `SiteName`, `IsDeleted`)
VALUES
	(1,1,'Calvary Cemetery',0);

/*!40000 ALTER TABLE `tblSite` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblStatement
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblStatement`;

CREATE TABLE `tblStatement` (
  `StatementID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PrimaryContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `ContractID` int(11) unsigned NOT NULL DEFAULT '0',
  `StatementNumber` varchar(60) NOT NULL DEFAULT '',
  `ContractTotalSalePrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  `StatementDate` date NOT NULL DEFAULT '0000-00-00',
  `DueDate` date NOT NULL DEFAULT '0000-00-00',
  `OpeningBalance` decimal(8,2) NOT NULL DEFAULT '0.00',
  `ClosingBalance` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CurrentDueAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `DueAmount1To30Days` decimal(8,2) NOT NULL DEFAULT '0.00',
  `DueAmount31To60Days` decimal(8,2) NOT NULL DEFAULT '0.00',
  `DueAmount61To90Days` decimal(8,2) NOT NULL DEFAULT '0.00',
  `DueAmount90PlusDays` decimal(8,2) NOT NULL DEFAULT '0.00',
  `TotalDueAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Month` int(11) DEFAULT NULL,
  `Year` int(11) DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`StatementID`),
  KEY `PrimaryContactID` (`PrimaryContactID`),
  KEY `ContractID` (`ContractID`),
  KEY `DateCreated` (`DateCreated`),
  KEY `StatementDate` (`StatementDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblStockHistory
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblStockHistory`;

CREATE TABLE `tblStockHistory` (
  `StockHistoryID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Action` enum('Manual Override','Delivery','Fulfillment') NOT NULL DEFAULT 'Manual Override',
  `FeeID` int(11) unsigned NOT NULL DEFAULT '0',
  `Result` int(11) NOT NULL DEFAULT '0',
  `StockLevelBefore` int(11) NOT NULL DEFAULT '0',
  `StockLevelAfter` int(11) NOT NULL DEFAULT '0',
  `DeliveryDate` date NOT NULL DEFAULT '0000-00-00',
  `ReceivedBy` varchar(1) NOT NULL DEFAULT '',
  `ReasonForOverride` varchar(1000) NOT NULL DEFAULT '',
  `SupplierID` int(11) unsigned NOT NULL DEFAULT '0',
  `DeliveryNumber` int(11) NOT NULL DEFAULT '0',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `IsArchived` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`StockHistoryID`),
  KEY `FeeID` (`FeeID`),
  KEY `SupplierID` (`SupplierID`),
  KEY `DeliveryDate` (`DeliveryDate`),
  KEY `Action` (`Action`),
  KEY `ReceivedBy` (`ReceivedBy`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblSupplier
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblSupplier`;

CREATE TABLE `tblSupplier` (
  `SupplierID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SupplierName` varchar(100) NOT NULL DEFAULT '',
  `Order` int(11) NOT NULL DEFAULT '0',
  `EmailAddress` varchar(50) NOT NULL DEFAULT '',
  `PhoneNumber` varchar(50) NOT NULL DEFAULT '',
  `AddressLine1` varchar(50) NOT NULL DEFAULT '',
  `AddressLine2` varchar(50) NOT NULL DEFAULT '',
  `Town` varchar(50) NOT NULL DEFAULT '',
  `State` varchar(50) NOT NULL DEFAULT '',
  `Active` tinyint(1) NOT NULL DEFAULT '1',
  `Postcode` varchar(20) NOT NULL DEFAULT '',
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SupplierID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblSyncRequest
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblSyncRequest`;

CREATE TABLE `tblSyncRequest` (
  `SyncRequestID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SyncType` enum('Plots','Contacts','UserRequestedPlot') NOT NULL DEFAULT 'Plots',
  `SyncStatus` enum('Requested','InProcess','Complete') NOT NULL DEFAULT 'Requested',
  `RowCount` int(11) DEFAULT NULL,
  `SuccessCount` int(11) DEFAULT NULL,
  `FailureCount` int(11) DEFAULT NULL,
  `RequestedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CompletedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `RequestJson` mediumtext NOT NULL,
  PRIMARY KEY (`SyncRequestID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblSyncRequestDetail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblSyncRequestDetail`;

CREATE TABLE `tblSyncRequestDetail` (
  `SyncRequestDetailID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SyncRequestID` int(11) unsigned NOT NULL DEFAULT '0',
  `PlotID` int(11) unsigned NOT NULL DEFAULT '0',
  `ExtPlotDescription` varchar(200) NOT NULL DEFAULT '',
  `SyncStatus` enum('Not Synced','Created','Updated','Error') NOT NULL DEFAULT 'Not Synced',
  `StatusMessage` text NOT NULL,
  `RequestJson` mediumtext NOT NULL,
  PRIMARY KEY (`SyncRequestDetailID`),
  KEY `SyncRequestID` (`SyncRequestID`),
  KEY `PlotID` (`PlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblSystemType
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblSystemType`;

CREATE TABLE `tblSystemType` (
  `SystemTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SystemTypeName` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`SystemTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tblSystemType` WRITE;
/*!40000 ALTER TABLE `tblSystemType` DISABLE KEYS */;

INSERT INTO `tblSystemType` (`SystemTypeID`, `SystemTypeName`)
VALUES
	(1,'Cemetery'),
	(2,'Real Estate');

/*!40000 ALTER TABLE `tblSystemType` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblTag
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblTag`;

CREATE TABLE `tblTag` (
  `TagID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `TagName` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`TagID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblTagLink
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblTagLink`;

CREATE TABLE `tblTagLink` (
  `TagLinkID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `TagID` int(11) DEFAULT NULL,
  `ModelID` int(11) DEFAULT NULL,
  `ModelType` enum('Contract','Contact','Deed','Payment','Plot') NOT NULL DEFAULT 'Contract',
  PRIMARY KEY (`TagLinkID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblTaxRate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblTaxRate`;

CREATE TABLE `tblTaxRate` (
  `TaxRateID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `TaxRate` decimal(6,3) NOT NULL DEFAULT '0.000',
  `DateAdded` date NOT NULL DEFAULT '0000-00-00',
  `TaxRateName` varchar(150) NOT NULL DEFAULT '',
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `NominalCodeID` int(11) unsigned NOT NULL DEFAULT '0',
  `IsHistoric` varchar(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`TaxRateID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `NominalCodeID` (`NominalCodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblTaxRate` WRITE;
/*!40000 ALTER TABLE `tblTaxRate` DISABLE KEYS */;

INSERT INTO `tblTaxRate` (`TaxRateID`, `TaxRate`, `DateAdded`, `TaxRateName`, `FacilityID`, `NominalCodeID`, `IsHistoric`)
VALUES
	(21,0.000,'2017-04-26','',0,0,'0'),
	(22,20.000,'2017-04-26','',0,0,'0'),
	(23,23.000,'2017-04-26','',0,0,'0'),
	(24,13.000,'2017-04-26','',0,0,'0'),
	(25,9.000,'2017-04-26','',0,0,'0');

/*!40000 ALTER TABLE `tblTaxRate` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblTaxRateHistory
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblTaxRateHistory`;

CREATE TABLE `tblTaxRateHistory` (
  `TaxRateHistoryID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DateTimeChanged` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TaxRateID` int(11) unsigned NOT NULL DEFAULT '0',
  `ChangesBefore` mediumtext NOT NULL,
  `ChangesAfter` mediumtext NOT NULL,
  PRIMARY KEY (`TaxRateHistoryID`),
  KEY `TaxRateID` (`TaxRateID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblTaxRateHistory` WRITE;
/*!40000 ALTER TABLE `tblTaxRateHistory` DISABLE KEYS */;

INSERT INTO `tblTaxRateHistory` (`TaxRateHistoryID`, `DateTimeChanged`, `TaxRateID`, `ChangesBefore`, `ChangesAfter`)
VALUES
	(1,'2017-04-26 10:38:37',21,'[]','{\"DateAdded\":\"2017-04-26T00:00:00+0000\"}'),
	(2,'2017-04-26 10:38:37',22,'[]','{\"TaxRate\":20,\"DateAdded\":\"2017-04-26T00:00:00+0000\"}'),
	(3,'2017-04-26 10:38:37',23,'[]','{\"TaxRate\":23,\"DateAdded\":\"2017-04-26T00:00:00+0000\"}'),
	(4,'2017-04-26 10:38:37',24,'[]','{\"TaxRate\":13,\"DateAdded\":\"2017-04-26T00:00:00+0000\"}'),
	(5,'2017-04-26 10:38:37',25,'[]','{\"TaxRate\":9,\"DateAdded\":\"2017-04-26T00:00:00+0000\"}');

/*!40000 ALTER TABLE `tblTaxRateHistory` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tblTransaction
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblTransaction`;

CREATE TABLE `tblTransaction` (
  `TransactionID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `TransactionModelID` int(11) unsigned NOT NULL DEFAULT '0',
  `TransactionType` enum('Invoice','Payment') NOT NULL DEFAULT 'Invoice',
  `ContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `Amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `AmountOutstanding` decimal(8,2) NOT NULL DEFAULT '0.00',
  `CreatedDate` date NOT NULL DEFAULT '0000-00-00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`TransactionID`),
  KEY `TransactionModelID` (`TransactionModelID`),
  KEY `ContactID` (`ContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblTransactionAllocation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblTransactionAllocation`;

CREATE TABLE `tblTransactionAllocation` (
  `TransactionAllocationID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `InvoiceTransactionID` int(11) unsigned NOT NULL DEFAULT '0',
  `PaymentTransactionID` int(11) unsigned NOT NULL DEFAULT '0',
  `AllocationAmount` decimal(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`TransactionAllocationID`),
  KEY `InvoiceTransactionID` (`InvoiceTransactionID`),
  KEY `PaymentTransactionID` (`PaymentTransactionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblTrustType
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblTrustType`;

CREATE TABLE `tblTrustType` (
  `TrustTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(150) NOT NULL DEFAULT '',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`TrustTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tblWorkOrder
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblWorkOrder`;

CREATE TABLE `tblWorkOrder` (
  `WorkOrderID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `WorkOrderNumber` varchar(50) NOT NULL DEFAULT '',
  `FacilityID` int(11) unsigned NOT NULL DEFAULT '0',
  `Status` enum('Draft','Open','In Progress','Complete') NOT NULL DEFAULT 'Draft',
  `StatusSetDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `StartDate` date NOT NULL DEFAULT '0000-00-00',
  `StartTime` time NOT NULL DEFAULT '00:00:00',
  `DueDate` date NOT NULL DEFAULT '0000-00-00',
  `DueTime` time NOT NULL DEFAULT '00:00:00',
  `CompletedByUserID` int(11) unsigned NOT NULL DEFAULT '0',
  `CompletedDate` date NOT NULL DEFAULT '0000-00-00',
  `CompletedTime` time NOT NULL DEFAULT '00:00:00',
  `TimeToCompleteUnix` int(11) DEFAULT NULL,
  `RequestedByContactID` int(11) unsigned NOT NULL DEFAULT '0',
  `Notes` mediumtext NOT NULL,
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedBy` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastUpdatedBy` int(11) DEFAULT NULL,
  `LastUpdatedDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`WorkOrderID`),
  KEY `FacilityID` (`FacilityID`),
  KEY `CompletedByUserID` (`CompletedByUserID`),
  KEY `RequestedByContactID` (`RequestedByContactID`),
  KEY `WorkOrderNumber` (`WorkOrderNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblWorkOrderAllocation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblWorkOrderAllocation`;

CREATE TABLE `tblWorkOrderAllocation` (
  `WorkOrderAllocationID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `WorkOrderID` int(11) unsigned NOT NULL DEFAULT '0',
  `ModelType` enum('PlotBox User','Contact') NOT NULL DEFAULT 'PlotBox User',
  `ModelID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`WorkOrderAllocationID`),
  KEY `WorkOrderID` (`WorkOrderID`),
  KEY `ModelID` (`ModelID`),
  KEY `ModelType` (`ModelType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblWorkOrderDocument
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblWorkOrderDocument`;

CREATE TABLE `tblWorkOrderDocument` (
  `WorkOrderDocumentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `WorkOrderID` int(11) unsigned NOT NULL DEFAULT '0',
  `DocumentName` varchar(255) NOT NULL DEFAULT '',
  `FormTypeID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`WorkOrderDocumentID`),
  KEY `WorkOrderID` (`WorkOrderID`),
  KEY `FormTypeID` (`FormTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblWorkOrderLink
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblWorkOrderLink`;

CREATE TABLE `tblWorkOrderLink` (
  `WorkOrderLinkID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `WorkOrderID` int(11) unsigned NOT NULL DEFAULT '0',
  `ModelType` enum('Contract','Memorial','Plot','Work Permit') NOT NULL DEFAULT 'Plot',
  `ModelID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`WorkOrderLinkID`),
  KEY `WorkOrderID` (`WorkOrderID`),
  KEY `ModelID` (`ModelID`),
  KEY `WorkOrderLinkID` (`WorkOrderLinkID`),
  KEY `ModelType` (`ModelType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tblWorkOrderWorkType
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblWorkOrderWorkType`;

CREATE TABLE `tblWorkOrderWorkType` (
  `WorkOrderWorkTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `WorkOrderID` int(11) unsigned NOT NULL DEFAULT '0',
  `WorkType` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`WorkOrderWorkTypeID`),
  KEY `WorkOrderID` (`WorkOrderID`),
  KEY `WorkType` (`WorkType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
