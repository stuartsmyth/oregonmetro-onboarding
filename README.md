# Calvary Data Migration Template #

### Setup ###

To start a new PlotBox customer data migration project simply fork this repository.

### Dependencies ###

- Install pip: https://pip.pypa.io/en/stable/installing/
- Install CsvKit: https://github.com/wireservice/csvkit
- Local MySQL with root user and no password
- PlotBox CLI

### Customisation ###



### How to run ###

From the root directory of the project run:

>. run.sh
